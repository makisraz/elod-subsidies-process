package actions;

import java.util.ArrayList;
import java.util.List;

import ontology.OntologyInitialization;

import objects.Municipality;
import objects.Subsidy;

import com.hp.hpl.jena.rdf.model.Model;

import utils.Configuration;
import utils.HelperMethods;

import virtuoso.jena.driver.VirtGraph;

/**
 * @author G. Razis
 */
public class Main {
	
	public static VirtGraph graphOrgs = null;
	public static List<Municipality> municipalitiesList = new ArrayList<Municipality>();
	
	/**
     * Method to initiate the process
     */
	public static void main(String[] args) {
		
		boolean createConcepts = true;
		
		HelperMethods hm = new HelperMethods();
		CsvActions csvMethods = new CsvActions();
		RdfActions rdfActions = new RdfActions();
		ConceptCreator concepts = new ConceptCreator();
		MunicipalityNames municNames = new MunicipalityNames();
		OntologyInitialization ontInit = new OntologyInitialization();
		
		/* Organizations Graph */
		graphOrgs = new VirtGraph(Configuration.queryGraphOrganizations, Configuration.connectionString, 
								  Configuration.username, Configuration.password);
		System.out.println("Connected to Organizations Graph!");
		
		Model model = rdfActions.createModel();
		
		//Perform the basic initialization on the model
		ontInit.setPrefixes(model);
		ontInit.createHierarchies(model);
		
		//add the Concepts
		if (createConcepts) {
			concepts.addProjectStatusToModel(model);
			concepts.addCountriesToModel(model);
			concepts.addCurrenciesToModel(model);
		}
		
		//load the Municipalities
		municipalitiesList = municNames.getMunicipalitiesCSVList();
		
		//find the CSV files
		List<String> csvFileNameList = hm.getCsvFileNames(Configuration.filePathCsv);
		
		for (String csvFileName : csvFileNameList) {
			//create the subsidy Objects list
			List<Subsidy> sybsidiesList = csvMethods.readSubsidyCsv(Configuration.filePathCsv + csvFileName);
			//List<Subsidy> sybsidiesList = csvMethods.readSubsidyCsv(Configuration.filePathCsv + "/" + "");
			for (Subsidy subsidy : sybsidiesList) {
				if (subsidy.getDistrict().length() > 1) {
					if ( (subsidy.getAsBuyerUri() == null) && (subsidy.getAsSellerUri() == null) ) {
						System.out.println("RDFizing line with ID: " + subsidy.getId());
						rdfActions.createRdfFromCsv(subsidy, model);
					} else if ( (subsidy.getAsBuyerUri() != null) && (subsidy.getAsSellerUri() == null) ) {
						System.out.println("RDFizing line with ID: " + subsidy.getId());
						rdfActions.createRdfFromCsv(subsidy, model);
					} else if ( (subsidy.getAsBuyerUri() == null) && (subsidy.getAsSellerUri() != null) ) {
						System.out.println("RDFizing line with ID: " + subsidy.getId());
						rdfActions.createRdfFromCsv(subsidy, model);
					} else if ( (subsidy.getAsBuyerUri() != null) && (subsidy.getAsSellerUri() != null) && 
								hm.checkValidityForProcess(subsidy.getAsBuyerUri().getURI(), subsidy.getAsSellerUri().getURI()) ) {
						System.out.println("RDFizing line with ID: " + subsidy.getId());
						rdfActions.createRdfFromCsv(subsidy, model);
					} else {
						System.out.println("Will not RDFize line with ID: " + subsidy.getId());
						hm.writeDataToFile("buyerAndSellerId", subsidy.getId());
					}
				} else {
					System.out.println("Will not process line with ID: " + subsidy.getId());
					hm.writeDataToFile("noDistrict", subsidy.getId());
				}
			}
			
			/* store the model at the end of each file */
			rdfActions.writeModel(model);
			
			hm.writeDataToFile("filesOK", csvFileName);
			//break;
		}
		
		model.close();
		
		System.out.println("\nFinished!");
	}

}