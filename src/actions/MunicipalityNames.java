package actions;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import java.util.ArrayList;
import java.util.List;

import au.com.bytecode.opencsv.CSVReader;

import objects.Municipality;

import utils.Configuration;

/**
 * @author G. Razis
 */
public class MunicipalityNames {
	
	/**
     * Read the list of municipalities from the 
     * CSV file and store them in a list.
     * 
     * @param List<Municipality> the list of municipalities
     */
	public List<Municipality> getMunicipalitiesCSVList() {
		
		List<Municipality> buyersList = new ArrayList<Municipality>();
		
		//CSV details
		String fileName = null;
		String uri = null;
		String graphName = null;
		
		String[] fields;
		Municipality municObj = null;
		
		try {
			CSVReader reader = new CSVReader(new FileReader(Configuration.municipalitiesMapping + "municipalities.csv"), ';');
			String[] nextLine;
			try {
				while ((nextLine = reader.readNext()) != null) {
					fields = new String[nextLine.length];
					
					for (int i = 0; i < nextLine.length; i++) {
						fields[i] = nextLine[i];
					}
					
					fileName = fields[0];
					uri = fields[1];
					graphName = fields[2];
					
					municObj = new Municipality(fileName, uri, graphName);
					buyersList.add(municObj);
				}
				reader.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		
		return buyersList;
	}
	
	/**
     * Find the URI of the municipality by searching 
     * the list of municipalities from the CSV.
     * 
     * @param String the name of the municipality
     * @return String the URI of the municipality
     */
	public String findBuyerURIFromFile(String municipalityName) {
		
		String uri = null;
		
		for (Municipality municObj : Main.municipalitiesList) {
			if (municObj.getFileName().equalsIgnoreCase(municipalityName)) {
				uri =  municObj.getUri();
				break;
			}
		}
		
		//for some curious reason this cannot be found...
		if ( (municipalityName.contains("Άκτιου") && municipalityName.contains("Βόνιτσας")) ) {
			uri =  "http://linkedeconomy.org/resource/Municipality/9124";
		}
		
		return uri;
	}

}