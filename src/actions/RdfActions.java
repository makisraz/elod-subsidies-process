package actions;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;

import objects.Subsidy;

import ontology.Ontology;

import utils.Configuration;
import utils.HelperMethods;

import com.hp.hpl.jena.datatypes.xsd.XSDDatatype;
import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.ModelFactory;
import com.hp.hpl.jena.rdf.model.Resource;

/**
 * @author G. Razis
 */
public class RdfActions {
	
	/**
     * Fetch all the details from the provided Subsidy object, 
     * create the RDF triples and add them to the existing model.
     * 
     * @param Subsidy a Subsidy object
     * @param Model the model to add the triples
     */
	public void createRdfFromCsv(Subsidy subsidyObject, Model model) {
		
		HelperMethods hm = new HelperMethods();
		MunicipalityNames municNames = new MunicipalityNames();
		
		Resource subsidyResource = null;
		Resource budgetItemResource = null;
		Resource contractResource = null;
		Resource spendingItemResource = null;
		Resource beneficiaryResource = null;
		Resource upsBudgetResource = null;
		Resource upsContractResource = null;
		Resource upsSpendingResource = null;
		
		String municipalityUri = municNames.findBuyerURIFromFile(subsidyObject.getDistrict());
		String minicipalityId = municipalityUri.split("/Municipality/")[1];
		
		/** Beneficiary **/
		//1st case: data from buyer or seller id -> query the resource
		if (subsidyObject.getAsBuyerUri() != null) {
			beneficiaryResource = subsidyObject.getAsBuyerUri();
		} else if (subsidyObject.getAsSellerUri() != null) {
			beneficiaryResource = subsidyObject.getAsSellerUri();
		} else { //2n case: no matching at all -> create the resource
			beneficiaryResource = model.createResource(Ontology.instancePrefix + "Organization/" + "tempId_" + subsidyObject.getId(), Ontology.organizationResource);
			model.createResource(Ontology.instancePrefix + "Organization/" + "tempId_" + subsidyObject.getId(), Ontology.businessEntityResource);
		    model.createResource(Ontology.instancePrefix + "Organization/" + "tempId_" + subsidyObject.getId(), Ontology.orgOrganizationResource);
		    model.createResource(Ontology.instancePrefix + "Organization/" + "tempId_" + subsidyObject.getId(), Ontology.registeredOrganizationResource);
		    beneficiaryResource.addLiteral(Ontology.name, subsidyObject.getBeneficiaryName());
		}
		
		/** Budget Item **/
		budgetItemResource = model.createResource(Ontology.instancePrefix + "Subsidy/BudgetItem/" + minicipalityId + "/" + subsidyObject.getId(), Ontology.budgetItemResource);
		
		/** Budget Item - Organization **/
		budgetItemResource.addProperty(Ontology.beneficiary, beneficiaryResource);
		
		/** Unit Price Specification (Budget) **/
		upsBudgetResource = model.createResource(Ontology.instancePrefix + "UnitPriceSpecification/" + minicipalityId + "/" + subsidyObject.getId() + "/BudgetItem", Ontology.unitPriceSpecificationResource);
		upsBudgetResource.addLiteral(Ontology.hasCurrencyValue, model.createTypedLiteral(subsidyObject.getBudgetAmount(), XSDDatatype.XSDfloat));
		upsBudgetResource.addProperty(Ontology.hasCurrency, model.getResource(Ontology.instancePrefix + "Currency/" + "EUR"));
		upsBudgetResource.addLiteral(Ontology.valueAddedTaxIncluded, false);
		
		/** Budget Item - Unit Price Specification (Budget Item) **/
		budgetItemResource.addProperty(Ontology.price, upsBudgetResource);
		
		/** Contract **/
		contractResource = model.createResource(Ontology.instancePrefix + "Subsidy/Contract/" + minicipalityId + "/" + subsidyObject.getId(), Ontology.contractResource);
		
		/** Contract - Organization **/
		contractResource.addProperty(Ontology.beneficiary, beneficiaryResource);
		
		/** Unit Price Specification (Contract) **/
		upsContractResource = model.createResource(Ontology.instancePrefix + "UnitPriceSpecification/" + minicipalityId + "/" + subsidyObject.getId() + "/Contract", Ontology.unitPriceSpecificationResource);
		upsContractResource.addLiteral(Ontology.hasCurrencyValue, model.createTypedLiteral(subsidyObject.getContractAmount(), XSDDatatype.XSDfloat));
		upsContractResource.addProperty(Ontology.hasCurrency, model.getResource(Ontology.instancePrefix + "Currency/" + "EUR"));
		upsContractResource.addLiteral(Ontology.valueAddedTaxIncluded, false);
		
		/** Contract - Unit Price Specification (Contract) **/
		contractResource.addProperty(Ontology.agreedPrice, upsContractResource);
		
		/** Spending Item **/
		spendingItemResource = model.createResource(Ontology.instancePrefix + "Subsidy/SpendingItem/" + minicipalityId + "/" + subsidyObject.getId(), Ontology.spendingItemResource);
		
		/** Spending Item - Organization **/
		spendingItemResource.addProperty(Ontology.beneficiary, beneficiaryResource);
		
		/** Unit Price Specification (Spending Item) **/
		upsSpendingResource = model.createResource(Ontology.instancePrefix + "UnitPriceSpecification/" + minicipalityId + "/" + subsidyObject.getId() + "/SpendingItem", Ontology.unitPriceSpecificationResource);
		upsSpendingResource.addLiteral(Ontology.hasCurrencyValue, model.createTypedLiteral(subsidyObject.getPaymentAmount(), XSDDatatype.XSDfloat));
		upsSpendingResource.addProperty(Ontology.hasCurrency, model.getResource(Ontology.instancePrefix + "Currency/" + "EUR"));
		upsSpendingResource.addLiteral(Ontology.valueAddedTaxIncluded, false);
		
		/** Spending Item - Unit Price Specification (Budget) **/
		spendingItemResource.addProperty(Ontology.amount, upsSpendingResource);
		
		/** Subsidy **/
		subsidyResource = model.createResource(Ontology.instancePrefix + "Subsidy/" + minicipalityId + "/" + subsidyObject.getId(), Ontology.subsidyResource);
		
		subsidyResource.addProperty(Ontology.subject, subsidyObject.getTitle(), "el");
		
		String[] statusIndividual = hm.findProjectStatusIndividual(subsidyObject.getProjectStatus());
		subsidyResource.addProperty(Ontology.projectStatus, model.getResource(statusIndividual[0]));
		
		/** Subsidy - Budget Item **/
		subsidyResource.addProperty(Ontology.hasRelatedBudgetItem, budgetItemResource);
		
		/** Subsidy - Contract **/
		subsidyResource.addProperty(Ontology.hasRelatedContract, contractResource);
		
		/** Subsidy - Spending Item **/
		subsidyResource.addProperty(Ontology.hasRelatedSpendingItem, spendingItemResource);
		
		/** Subsidy - Organization **/
		subsidyResource.addProperty(Ontology.beneficiary, beneficiaryResource);
		
		/** Subsidy - Municipality **/
		subsidyResource.addProperty(Ontology.subsidyMunicipality, model.createResource(municipalityUri));
	}
	
	/**
     * Fetch the local existing model.
     * 
     * @return Model the new model
     */
	public Model createModel () {
		
		Model remoteModel = ModelFactory.createDefaultModel();
		
		try {
			InputStream is = new FileInputStream(Configuration.filePathOutput + "/" + Configuration.rdfName);
			remoteModel.read(is,null);
			is.close();
		} catch (Exception e) { //empty file
		}
		
		return remoteModel;
		
	}
	
	/**
     * Store the Model.
     * 
     * @param Model the model
     */
	public void writeModel(Model model) {
		
		try {
			System.out.println("\nSaving Model...");
			FileOutputStream fos = new FileOutputStream(Configuration.filePathOutput + "/" + Configuration.rdfName);
			model.write(fos, "RDF/XML-ABBREV", Configuration.filePathOutput + "/" + Configuration.rdfName);
			fos.close();
		} catch(Exception e) {
			e.printStackTrace();
		}
		
	}
	
}