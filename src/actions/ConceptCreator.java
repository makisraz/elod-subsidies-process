package actions;

import java.util.Arrays;
import java.util.List;

import ontology.Ontology;

import utils.CountryOriented;
import utils.HelperMethods;

import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.Resource;

/**
 * @author G. Razis
 */
public class ConceptCreator {
	
	private HelperMethods hm = new HelperMethods();
	private CountryOriented co = new CountryOriented();
	
	/**
     * Add to the model the project statuses.
     * 
     * @param Model the model we are currently working with
     */
	public void addProjectStatusToModel(Model model) {
		
		List<String> statusList = Arrays.asList("Συμβασιοποιημένο", "Ολοκληρωμένο");
		
		for (String status : statusList) {
			String[] statusDtls = hm.findProjectStatusIndividual(status);
			/** statusResource **/
			Resource statusResource = model.createResource(statusDtls[0], Ontology.projectStatusResource);
			model.createResource(statusDtls[0], Ontology.conceptResource);
			/** configure prefLabel **/
			statusResource.addProperty(Ontology.prefLabel, statusDtls[1], "el");
			statusResource.addProperty(Ontology.prefLabel, statusDtls[2], "en");
        }
		
	}
	
	/**
     * Add to the model the currencies.
     * 
     * @param Model the model we are currently working with
     */
	public void addCountriesToModel(Model model) {
		
		List<String> nameList = Arrays.asList("Ντιρχάμ Ενωμένων Αραβικών Εμιράτων", "Αφγάνι", "Λεκ της Αλβανίας", "Νραμ της Αρμενίας", "Γκίλντερ των Ολλανδικών Ανντιλών", 
											  "Κουάνζα Ανγκόλας", "Πέσο της Αργεντινής", "Δολάριο Αυστραλίας", "Φλορίνι της Αρούμπας", "Μανάτ του Αζερμπαϊτζάν", 
											  "Μετατρέψιμο Μάρκο Βοσνίας και Ερζεγοβίνης", "Δολάριο Μπαρμπάντος", "Τάκα του Μπανγκλαντές", "Λεβ Βουλγαρίας", 
											  "Bulgarian Lev", "Δηνάριο Μπαχρέιν", "Φράγκο Μπουρούντι", "Δολάριο Βερμούδων", "Δολάριο Μπρουνέι", "Μπολιβιάνο", 
											  "Ρεάλ Βραζιλίας", "Δολάριο Μπαχαμών", "Νγκούλτρουμ του Μπουτάν", "Πούλα Μποτσουάνα", "Ρούβλι Λευκορωσίας", "Δολάριο Μπελίζε", 
											  "Δολάριο Καναδά", "Φράγκο Κονγκό / Κινσάσα", "Φράγκο Ελβετίας", "Ελβετικό Φράγκο", "Πέσο Χιλής", "Κινεζικό Γουάν (Ρενμίνμπι)", 
											  "Πέσο Κολομβίας", "Κολόν Κόστα Ρίκα", "Μετατρέψιμο Πέσο της Κούβας", "Πέσο Κούβας", "Εσκούδο του Πράσινου Ακρωτηρίου", 
											  "Κορώνα Τσέχικης Δημοκρατίας", "Φράγκο του Τζιμπουτί", "Κορώνα Δανίας", "Δανική Κορώνα", "Πέσο Δομινικανής Δημοκρατίας", 
											  "Δηνάριο Αλγερίας", "Κορώνα Εσθονίας ", "Αίγυπτος Λίρα", "Νάκφα Ερυθραίας", "Μπιρ Αιθιοπίας", "ΕΥΡΩ", "Ευρώ", "Δολάριο Φίτζι", 
											  "Λίρα των Νησιών Φώκλαντ (Μαλβίνες)", "Λίρα Στερλίνα", "Λάρι Γεωργίας", "Λίρα Γκέρνσεϊ", "Σέντι της Γκάνας", "Λίρα Γιβραλτάρ", 
											  "Νταλάζι Γκάμπια", "Φράγκο Γουινέας", "Κετσάλ Γουατεμάλας", "Δολάριο Γουιάνας", "Δολάριο Χονγκ Κονγκ", "Λεμπίρα Ονδούρας", "Κούνα Κροατίας", 
											  "Γκουρντ Αϊτής", "Φόριντ Ουγγαρίας", "Ρουπία Ινδονησίας", "Νεο Σεκέλ Ισραήλ", "Λίρα Νήσου του Μαν", "Ρουπία Ινδίας", "Δηνάριο Ιράκ", 
											  "Ριάλι Ιράν", "Κορώνα Ισλανδίας", "Λίρα Τζέρσεϊ", "Δολάριο Τζαμάικας", "Δηνάριο Ιορδανίας", "Γιεν Ιαπωνίας", "Σελίνι Κένυας", "Σομ της Κιργιζίας", 
											  "Ριέλ Καμπότζης", "Φράγκο Κομορών", "Ουόν Βόρειας Κορέας", "Ουόν Νότια Κορέας", "Δηνάριο Κουβέιτ", "Δολάριο Νησιών Κέιμαν", "Τένγκε Καζακστάν", 
											  "Κιπ Λάος", "Λίρα του Λιβάνου", "Ρουπία Σρι Λάνκα", "Δολάριο Λιβερίας", "Λότι Λεσότο", "Λίτας Λιθουανίας", "Λατς Λετονίας", "Δηνάριο Λιβύης", 
											  "Ντιρχάμ Μαρόκου", "Λέου Μολδαβία", "Αριάρι Μαδαγασκάρης", "Δηνάριο της ΠΓΔΜ", "Κιάτ Μιανμάρ", "Τουγκρίκ Μογγολίας", "Πατάκα Μακάο", 
											  "Ουγκίγια Μαυριτανίας", "Λιρέτα Μάλτας", "Ρουπία του Μαυρίκιου", "Ρουφίγια Μαλδίβων", "Κουάτσα του Μαλάουι", "Πέσο Μεξικού", "Ρινγκίτ Μαλαισίας", 
											  "Μετικάλ Μοζαμβίκης", "Δολάριο Ναμίμπιας", "Νάιρα Νιγηρίας", "Κόρδοβα Νικαράγουας", "Κορώνα Νορβηγίας", "Ρουπία Νεπάλ", "Δολάριο Νέας Ζηλανδίας", 
											  "Ριάλ του Ομάν", "Μπαλμπόα Παναμά", "Νέο Σολ Περού", "Κίνα Παπούα-Νέας Γουινέας", "Πέσο Φιλιππίνων", "Ρουπία Πακιστάν", "Ζλότι Πολωνίας", 
											  "Γκουαρανί Παραγουάης", "Ριγιάλ του Κατάρ", "Λέου Ρουμανίας", "Ρουμανία Νέο Λέου", "Δηνάριο Σερβίας", "Ρούβλι Ρωσίας", "Φράγκο της Ρουάντα", 
											  "Ριάλ Σαουδικής Αραβίας", "Δολάριο Νήσων Σολομώντα", "Ρουπία Σεϋχελλών", "Λίρα του Σουδάν", "Κορώνα Σουηδίας", "Δολάριο Σιγκαπούρης", 
											  "Λίρα Αγίας Ελένης", "Κορώνα Σλοβακίας", "Λεόνε της Σιέρα Λεόνε", "Σελίνι Σομαλίας", "Δολάριο Σουρινάμ", "Ντόμπρα του Σάο Τομέ και Πρίνσιπε", 
											  "Ελ Σαλβαδόρ Κολόν", "Λίρα Συρίας", "Λιλανγκένι Σουαζιλάνδης", "Μπατ Ταϊλάνδης", "Σομόνι Τατζικιστάν", "Μανάτ του Τουρκμενιστάν", 
											  "Δηνάριο Τυνησίας", "Παάνγκα της Τόνγκα", "Λίρα Τουρκίας", "Δολάριο Τρινιδάδ και Τομπάγκο", "Δολάριο Τουβαλού", "Νέο Δολάριο Ταϊβάν", 
											  "Σελίνι Τανζανίας", "Γρίβνα Ουκρανίας", "Σελίνι της Ουγκάντας", "Δολάριο ΗΠΑ", "Δολάρια", "Πέσο Ουρουγουάης", "Σομ του Ουζμπεκιστάν", 
											  "Μπολίβαρ Βενεζουέλας", "Ντονγκ του Βιετνάμ", "Βάτου του Βανουάτου", "Τάλα του Σαμόα", "Φράγκο CFA Κεντρικής Αφρικής", "Δολάριο Ανατολικής Καραϊβικής", 
											  "Διεθνές Νομισματικό Ταμείο (ΔΝΤ)", "Φράγκο CFA Δυτικής Αφρικής", "Φράγκο CFP", "Ριάλ Υεμένης", "Ραντ Νοτίου Αφρικής", "Κουάτσα της Ζάμπιας", "Δολάριο Ζιμπάμπουε");
		
		for (String currencyName : nameList) {
			String isoCode = co.currencyToIso(currencyName);
			/** currencyResource **/
			Resource currencyResource = model.createResource(Ontology.instancePrefix + "Currency/" + isoCode, Ontology.currencyResource);
			model.createResource(Ontology.instancePrefix + "Currency/" + isoCode, Ontology.conceptResource);
			/** configure prefLabel **/
			currencyResource.addProperty(Ontology.prefLabel, currencyName, "el");
			currencyResource.addProperty(Ontology.currencyIsoCode, isoCode);
		}
		
	}
	
	/**
     * Add to the model the countries.
     * 
     * @param Model the model we are currently working with
     */
	public void addCurrenciesToModel(Model model) {
		
		List<String> isoCodeList = Arrays.asList("GR", "EL", "DE", "AT", "AUT", "USA", "US", "FR", "IT", "CA", "CN", "IR", "GB", "TR", "JP", "ER", 
												 "CH", "GH", "BZ", "AE", "AN", "CY", "ES", "GE", "BE", "AD", "ID", "FI", "DK", "EU", "AF", "AG", 
												 "AI", "AL", "AM", "AO", "AQ", "AR", "AS", "AU", "AW", "AZ", "BA", "BB", "BD", "BF", "BG", "BH", 
												 "BI", "BJ", "BM", "BN", "BO", "BR", "BS", "BT", "BV", "BW", "BY", "CC", "CD", "CF", "CG", "CI", 
												 "CK", "CL", "CM", "CO", "CR", "CU", "CV", "CX", "CZ", "DJ", "DM", "DO", "DZ", "EC", "EE", "EG", 
												 "ET", "FJ", "FK", "FM", "FO", "GA", "GD", "GI", "GL", "GM", "GN", "GQ", "GS", "GT", "GU", "GW", 
												 "GY", "HK", "HM", "HN", "HR", "HT", "HU", "IE", "IL", "IN", "IO", "IQ", "IS", "JM", "JO", "KE", 
												 "KG", "KH", "KI", "KM", "KN", "KP", "KR", "KW", "KY", "KZ", "LA", "LB", "LC", "LI", "LK", "LR", 
												 "LS", "LT", "LU", "LV", "LY", "MA", "LD", "ME", "MG", "MH", "MK", "ML", "MM", "MN", "MO", "MP", 
												 "MR", "MS", "MT", "MU", "MV", "MW", "MX", "MY", "MZ", "NA", "NC", "NE", "NF", "NG", "NI", "NL", 
												 "NO", "NP", "NR", "NU", "NZ", "OM", "PA", "PE", "PF", "PG", "PH", "PK", "PL", "PM", "PN", "PS", 
												 "PT", "PW", "PY", "QA", "QQ", "QR", "QS", "QU", "QV", "QW", "QX", "QY", "QZ", "RO", "RU", "RW", 
												 "SA", "SB", "SC", "SD", "SE", "SG", "SH", "SI", "SK", "SL", "SM", "SN", "SO", "SR", "ST", "SV", 
												 "SY", "SZ", "TC", "TD", "TF", "TG", "TH", "TJ", "TK", "TL", "TM", "TN", "TO", "TT", "TV", "TW", 
												 "TZ", "UA", "UG", "UM", "UY", "UZ", "VA", "VC", "VE", "VG", "VI", "VN", "VU", "WF", "WS", "XC", 
												 "XK", "XL", "XS", "YE", "YT", "ZA", "ZM", "ZW");
		
		for (String isoCode : isoCodeList) {
			String[] countryDtls = co.findCountryFromAbbreviation(isoCode);
			/** countryResource **/
			Resource countryResource = model.createResource(Ontology.instancePrefix + "Country/" + isoCode, Ontology.countryResource);
			model.createResource(Ontology.instancePrefix + "Country/" + isoCode, Ontology.conceptResource);
			/** configure prefLabel **/
			countryResource.addProperty(Ontology.prefLabel, countryDtls[0], "el");
			countryResource.addProperty(Ontology.prefLabel, countryDtls[1], "en");
			countryResource.addProperty(Ontology.countryIsoCode, isoCode);
		}
		
	}
	
}