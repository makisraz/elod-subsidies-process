package actions;

import java.io.FileReader;

import java.util.ArrayList;
import java.util.List;

import com.hp.hpl.jena.rdf.model.Resource;

import objects.Subsidy;

import utils.HelperMethods;

import au.com.bytecode.opencsv.CSVReader;

/**
 * @author G. Razis
 */
public class CsvActions {
	
	/* 
	 * [^\\d+(\\.\\d+)?]*
	 * \d equals to 0-9. ^ defines that the patter must start at beginning of a new line.
	 * \d+ matches one or several digits. The ? makes the statement in brackets optional. 
	 * \. matches ".", parentheses are used for grouping. 
	 * Matches for example "5", "1.5" and "2.21".
	 * 
	 * [^0-9]* equivalent to [^\\d]*
	 * \d equals to 0-9. ^ defines that the patter must start at beginning of a new line.
	 * \d+ matches one or several digits.
	 */
	public List<Subsidy> readSubsidyCsv(String csvFilePath) {
		
		Queries qs = new Queries();
		HelperMethods hm = new HelperMethods();
		
		String id = null;
		String beneficiaryName = null;
		String title = null;
		float budgetAmount = -1;
		float contractAmount = -1;
		float paymentAmount = -1;
		String projectStatus = null;
		Resource asSellerUri = null;
		Resource asBuyerUri = null;
		String district = null;
		
		List<Subsidy> subsidiesList = new ArrayList<Subsidy>();
		
		try {
			CSVReader reader = new CSVReader(new FileReader(csvFilePath), ',', '"', '\0');
			String[] nextLine = null;
			System.out.println("\n" + csvFilePath);
			
			reader.readNext(); //skip the headers
			
			while ((nextLine = reader.readNext()) != null) {

				for (int i = 0; i < 10; i++) {
					if (i == 0) {
						id = hm.cleanCsvData(nextLine[i]);
						System.out.println("Reading line with ID: " + id);
					} else if (i == 1) {
						title = hm.cleanCsvData(nextLine[i]);
						if ( !(title.length() > 1) ) { //title is not provided
							title = "-";
						}
					} else if (i == 2) {
						beneficiaryName = hm.cleanCsvData(nextLine[i]);
					} else if (i == 3) {
						budgetAmount = Float.parseFloat(hm.cleanCsvData(nextLine[i]).replaceAll("[^0-9]*", "")); 
					} else if (i == 4) {
						contractAmount = Float.parseFloat(hm.cleanCsvData(nextLine[i]).replaceAll("[^0-9]*", ""));
					} else if (i == 5) {
						paymentAmount = Float.parseFloat(hm.cleanCsvData(nextLine[i]).replaceAll("[^0-9]*", ""));
					} else if (i == 6) {
						projectStatus = hm.cleanCsvData(nextLine[i]);
					} else if (i == 7) {
						asSellerUri = qs.getAgentUri( hm.findAgentsIdFromUri(hm.cleanCsvData(nextLine[i])) );
					} else if (i == 8) {
						asBuyerUri = qs.getAgentUri( hm.findAgentsIdFromUri(hm.cleanCsvData(nextLine[i])) );
					} else {
						district = hm.cleanCsvData(nextLine[i].split("\\.")[0]);
					}
				}
				
				Subsidy subsidyObject = new Subsidy(id, beneficiaryName, title, budgetAmount, contractAmount, 
													paymentAmount, projectStatus, asSellerUri, asBuyerUri, district);
				subsidiesList.add(subsidyObject);
				
				//re-initialize the variables
				id = null;
				beneficiaryName = null;
				title = null;
				budgetAmount = -1;
				contractAmount = -1;
				paymentAmount = -1;
				projectStatus = null;
				asSellerUri = null;
				asBuyerUri = null;
				district = null;
			}

			reader.close();

		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return subsidiesList;
	}
	
}