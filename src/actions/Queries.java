package actions;

import java.util.Arrays;
import java.util.List;

import utils.Configuration;

import virtuoso.jena.driver.VirtuosoQueryExecution;
import virtuoso.jena.driver.VirtuosoQueryExecutionFactory;

import com.hp.hpl.jena.query.QuerySolution;
import com.hp.hpl.jena.query.ResultSet;
import com.hp.hpl.jena.rdf.model.Resource;

public class Queries {
	
	public Resource getAgentUri(String id) {
		
		Resource agentUri = null;
		
		if (id != null) {
			List<String> uriPartsList = Arrays.asList("http://linkedeconomy.org/resource/Organization/", 
													  "http://linkedeconomy.org/resource/OrganizationalUnit/", 
													  "http://linkedeconomy.org/resource/Person/");
			
			for (String uriPart : uriPartsList) {
				String queryUri = "SELECT DISTINCT ?s " +
						"FROM <" + Configuration.queryGraphOrganizations + "> " +
						"WHERE { " +
						"?s ?p ?o . " +
						"FILTER (STR(?s) = \"" + uriPart + id + "\") . " +
						"}";
	
				VirtuosoQueryExecution vqeUri = VirtuosoQueryExecutionFactory.create(queryUri, Main.graphOrgs);		
				ResultSet resultsUri = vqeUri.execSelect();
				
				if (resultsUri.hasNext()) {
					QuerySolution result = resultsUri.nextSolution();
					agentUri = result.getResource("s");
					vqeUri.close();
					break;
				}
				
				vqeUri.close();
			}
		}
		
		return agentUri;
	}

}