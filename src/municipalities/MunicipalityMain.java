package municipalities;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

import java.util.List;

import com.hp.hpl.jena.query.QuerySolution;
import com.hp.hpl.jena.query.ResultSet;

import utils.Configuration;
import utils.HelperMethods;

import virtuoso.jena.driver.VirtGraph;
import virtuoso.jena.driver.VirtuosoQueryExecution;
import virtuoso.jena.driver.VirtuosoQueryExecutionFactory;

/**
 * @author G. Razis
 */
public class MunicipalityMain {
	
	public static void main(String[] args) {
		
		HelperMethods hm = new HelperMethods();
		
		/* Organizations Graph */
		VirtGraph graphGeo = new VirtGraph(Configuration.queryGraphGeoData, Configuration.connectionString, 
								 Configuration.username, Configuration.password);
		System.out.println("Connected to GeoData Graph!");
		
		//find the CSV files
		List<String> csvFileNameList = hm.getCsvFileNames("C:/Users/Makis/Dropbox/Espa (1)/2015_05_06");
		
		for (String csvFileName : csvFileNameList) {
			System.out.println("Querying: " + csvFileName);
			findMunicipalityUri(graphGeo, csvFileName.split("\\.")[0]);
		}
		
		System.out.println("\nFinished!");
	}
	
	private static void findMunicipalityUri(VirtGraph graphGeo, String municipalityName) {
		
		String uri = null;
		String name = null;
		
		String queryUri = "PREFIX elodGeo: <http://linkedeconomy.org/geoOntology#> " +
				"SELECT ?uri ?name " +
				"FROM <" + Configuration.queryGraphGeoData + "> " +
				"WHERE { " +
				"?uri elodGeo:name ?name . " +
				"FILTER regex (?name, \"Δήμος " + municipalityName + "\", \"i\") . " +
				"FILTER regex (?uri, \"Municipality\") . " +
				"}";

		VirtuosoQueryExecution vqeUri = VirtuosoQueryExecutionFactory.create(queryUri, graphGeo);		
		ResultSet resultsUri = vqeUri.execSelect();
		
		while (resultsUri.hasNext()) {
			QuerySolution result = resultsUri.nextSolution();
			uri = result.getResource("uri").getURI();
			name = result.getLiteral("name").getString();
		}
		
		vqeUri.close();
		
		writeMetadata("municipalities", municipalityName, uri, name);
	}
	
	private static void writeMetadata(String fileName, String municipalityName, String uri, String name) {
		
		try {
		    PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(fileName + ".txt", true)));
		    out.println(municipalityName + "	" + uri + "	" + name);
		    out.close();
		} catch (IOException e) {
		    e.printStackTrace();
		}
		
	}

}