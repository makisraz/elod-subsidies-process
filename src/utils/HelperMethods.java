package utils;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import ontology.Ontology;

/**
 * @author G. Razis
 */
public class HelperMethods {
	
	/**
     * Find the the filenames of the CSV of the provided directory.
     * 
     * @param String the path of the directory where the files exist
     * @return List<String> the filenames of the CSV
     */
	public List<String> getCsvFileNames(String filePath) {
		
		List<String> namesList = new ArrayList<String>();
		File[] files = new File(filePath).listFiles();

		for (File file : files) {
		    if (file.isFile()) {
		        namesList.add(file.getName());
		    }
		}
		
		return namesList;
	}
	
	/**
     * Check if a filed retrieved from API contains non-printable data
     * and return its proper format.
     * 
     * @param String a field which may need cleansing from non-printable data
     * @return String the cleaned data of the field.
     */
	public String cleanCsvData(String field) {
		
		try {
    	    //Pattern regex = Pattern.compile("[\\x00\\x08\\x0B\\x0C\\x0E-\\x1F]"); //non-printable data
			Pattern regex = Pattern.compile("[\\p{Cc}\\p{Cf}\\p{Co}\\p{Cn}]"); //non-printable data
    	    Matcher regexMatcher = regex.matcher(field);
    	    if (regexMatcher.find()) {
    	    	field = field.replaceAll("[\\p{Cntrl}]", "");
    	    	System.out.print("Cleaned field: " + field + "\n");
    	    }
    	    if (field.contains("\"")) {
    	    	field = field.replace("\"", "'");
    	    }
    	    if (field.contains("\\")) {
    	    	field = field.replace("\\", "");
    	    }
    	} catch (Exception e) {
    	    e.printStackTrace();
    	}
		
		return field;
	}
	
	/**
     * Export to a file the needed data.
     * 
     * @param String the output filename
     * @param String the data
     */
	public void writeDataToFile(String fileName, String metadata) {
		
		try {
		    PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(fileName + ".txt", true)));
		    out.println(metadata);
		    out.close();
		} catch (IOException e) {
		    e.printStackTrace();
		}
		
	}
	
	/**
     * Find the corresponding SKOS Concept, its Greek and English name of the provided status of the project.
     * 
     * @param String the status of the project
     * @return String the corresponding URI of the SKOS Concept, its Greek and English name 
     * of the provided status of the project
     */
	public String[] findProjectStatusIndividual(String status) {
		
		String[] statusDtls = null;
		
		if (status.equalsIgnoreCase("Συμβασιοποιημένο")) {
			statusDtls = new String[]{Ontology.instancePrefix + "ProjectStatus/" + "Contracted", "Συμβασιοποιημένο", "Contracted"};
		} else {
			statusDtls = new String[]{Ontology.instancePrefix + "ProjectStatus/" + "Completed", "Ολοκληρωμένο", "Completed"};
		}
		
		return statusDtls;
	}
	
	/**
     * Handle the invalid characters of provided string.
     * 
     * @param String a string to be cleaned
     * @return String the cleaned representation of the input string
     */
	public String cleanInvalidCharsJsonData(String aString) {
		
		String cleanedString = aString.replace("\"", "'");
		cleanedString = cleanedString.replace("\\", "");
		
		return cleanedString;
	}
	
	/**
     * Find the Agent's Id from the provided Uri.
     * 
     * @param String the provided Uri
     * @return String the Agent's Id
     */
	public String findAgentsIdFromUri(String uri) {
		
		String id = null;
		
		if ( !uri.equalsIgnoreCase("") && uri.contains("=afm=") ) {
			id = uri.split("=afm=")[1].replace("_", "/");
		}
		
		return id;
	}
	
	/**
     * Find whether to process the Subsidy or not depending on 
     * the Agent's provided URIs as Buyer and as Seller.
     * 
     * @param String the Uri of the Agent as Buyer
     * @param String Uri of the Agent as Seller
     * @return boolean whether to process the Subsidy or not
     */
	public boolean checkValidityForProcess(String asBuyerUri, String asSellerUri) {
		
		boolean processFlag = false;
		
		String partB = null;
		String partS = null;
		
		if ( (asSellerUri.contains("Organization/") && asBuyerUri.contains("Organization/")) ) {
			partS = asSellerUri.split("Organization/")[1];
			partB = asBuyerUri.split("Organization/")[1];
			if ( partB.contains(partS) || partS.contains(partB) || partB.equalsIgnoreCase(partS) ) {
				processFlag = true;
			}
		} else if ( (asSellerUri.contains("OrganizationalUnit/") && asBuyerUri.contains("OrganizationalUnit/")) ) {
			partS = asSellerUri.split("OrganizationalUnit/")[1];
			partB = asBuyerUri.split("OrganizationalUnit/")[1];
			if ( partB.contains(partS) || partS.contains(partB) || partB.equalsIgnoreCase(partS) ) {
				processFlag = true;
			}
		} else if ( (asSellerUri.contains("Person/") && asBuyerUri.contains("Person/")) ) {
			partS = asSellerUri.split("Person/")[1];
			partB = asBuyerUri.split("Person/")[1];
			if ( partB.contains(partS) || partS.contains(partB) || partB.equalsIgnoreCase(partS) ) {
				processFlag = true;
			}
		}
		
		return processFlag;
	}
	
}