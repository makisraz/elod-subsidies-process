package utils;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * @author G. Razis
 */
public class CountryOriented {
	
	/**
     * Find the the ISO Format of the currency.
     * 
     * @param String the currency from the CSV file
     * @return String the ISO Format of the currency
     */
	public String currencyToIso(String currency) {
		
		String isoFormat = "";
		
		if (currency.equalsIgnoreCase("Ντιρχάμ Ενωμένων Αραβικών Εμιράτων")) {
			isoFormat = "AED";
		} else if (currency.equalsIgnoreCase("Αφγάνι")) {
			isoFormat = "AFN";
		} else if (currency.equalsIgnoreCase("Λεκ της Αλβανίας")) {
			isoFormat = "ALL";
		} else if (currency.equalsIgnoreCase("Νραμ της Αρμενίας")) {	         
			isoFormat = "AMD";
		} else if (currency.equalsIgnoreCase("Γκίλντερ των Ολλανδικών Ανντιλών")) {	         
			isoFormat = "ANG";
		} else if (currency.equalsIgnoreCase("Κουάνζα Ανγκόλας")) {	         
			isoFormat = "AOA";
		} else if (currency.equalsIgnoreCase("Πέσο της Αργεντινής")) {
			isoFormat = "ARS";
		} else if (currency.equalsIgnoreCase("Δολάριο Αυστραλίας")) {
			isoFormat = "AUD";
		} else if (currency.equalsIgnoreCase("Φλορίνι της Αρούμπας")) {	         
			isoFormat = "AWG";
		} else if (currency.equalsIgnoreCase("Μανάτ του Αζερμπαϊτζάν")) {	         
			isoFormat = "AZN";
		} else if (currency.equalsIgnoreCase("Μετατρέψιμο Μάρκο Βοσνίας και Ερζεγοβίνης")) {	         
			isoFormat = "BAM";
		} else if (currency.equalsIgnoreCase("Δολάριο Μπαρμπάντος")) {
			isoFormat = "BBD";
		} else if (currency.equalsIgnoreCase("Τάκα του Μπανγκλαντές")) {
			isoFormat = "BDT";
		} else if (currency.equalsIgnoreCase("Λεβ Βουλγαρίας") || currency.equalsIgnoreCase("Bulgarian Lev")) {	         
			isoFormat = "BGN";
		} else if (currency.equalsIgnoreCase("Δηνάριο Μπαχρέιν")) {	         
			isoFormat = "BHD";
		} else if (currency.equalsIgnoreCase("Φράγκο Μπουρούντι")) {	         
			isoFormat = "BIF";
		} else if (currency.equalsIgnoreCase("Δολάριο Βερμούδων")) {
			isoFormat = "BMD";
		} else if (currency.equalsIgnoreCase("Δολάριο Μπρουνέι")) {
			isoFormat = "BND";
		} else if (currency.equalsIgnoreCase("Μπολιβιάνο")) {	         
			isoFormat = "BOB";
		} else if (currency.equalsIgnoreCase("Ρεάλ Βραζιλίας")) {	         
			isoFormat = "BRL";
		} else if (currency.equalsIgnoreCase("Δολάριο Μπαχαμών")) {	         
			isoFormat = "BSD";
		} else if (currency.equalsIgnoreCase("Νγκούλτρουμ του Μπουτάν")) {
			isoFormat = "BTN";
		} else if (currency.equalsIgnoreCase("Πούλα Μποτσουάνα")) {
			isoFormat = "BWP";
		} else if (currency.equalsIgnoreCase("Ρούβλι Λευκορωσίας")) {	         
			isoFormat = "BYR";
		} else if (currency.equalsIgnoreCase("Δολάριο Μπελίζε")) {	         
			isoFormat = "BZD";
		} else if (currency.equalsIgnoreCase("Δολάριο Καναδά")) {	         
			isoFormat = "CAD";
		} else if (currency.equalsIgnoreCase("Φράγκο Κονγκό / Κινσάσα")) {
			isoFormat = "CDF";
		} else if ( currency.equalsIgnoreCase("Φράγκο Ελβετίας") || currency.equalsIgnoreCase("Ελβετικό Φράγκο") ) {
			isoFormat = "CHF";
		} else if (currency.equalsIgnoreCase("Πέσο Χιλής")) {	         
			isoFormat = "CLP";
		} else if (currency.equalsIgnoreCase("Κινεζικό Γουάν (Ρενμίνμπι)")) {	         
			isoFormat = "CNY";
		} else if (currency.equalsIgnoreCase("Πέσο Κολομβίας")) {	         
			isoFormat = "COP";
		} else if (currency.equalsIgnoreCase("Κολόν Κόστα Ρίκα")) {
			isoFormat = "CRC";
		} else if (currency.equalsIgnoreCase("Μετατρέψιμο Πέσο της Κούβας")) {
			isoFormat = "CUC";
		} else if (currency.equalsIgnoreCase("Πέσο Κούβας")) {	         
			isoFormat = "CUP";
		} else if (currency.equalsIgnoreCase("Εσκούδο του Πράσινου Ακρωτηρίου")) {	         
			isoFormat = "CVE";
		} else if (currency.equalsIgnoreCase("Κορώνα Τσέχικης Δημοκρατίας")) {	         
			isoFormat = "CZK";
		} else if (currency.equalsIgnoreCase("Φράγκο του Τζιμπουτί")) {
			isoFormat = "DJF";
		} else if (currency.equalsIgnoreCase("Κορώνα Δανίας") || currency.equalsIgnoreCase("Δανική Κορώνα")) {
			isoFormat = "DKK";
		} else if (currency.equalsIgnoreCase("Πέσο Δομινικανής Δημοκρατίας")) {	         
			isoFormat = "DOP";
		} else if (currency.equalsIgnoreCase("Δηνάριο Αλγερίας")) {	         
			isoFormat = "DZD";
		} else if (currency.equalsIgnoreCase("Κορώνα Εσθονίας ")) {	         
			isoFormat = "EEK";
		} else if (currency.equalsIgnoreCase("Αίγυπτος Λίρα")) {
			isoFormat = "EGP";
		} else if (currency.equalsIgnoreCase("Νάκφα Ερυθραίας")) {
			isoFormat = "ERN";
		} else if (currency.equalsIgnoreCase("Μπιρ Αιθιοπίας")) {	         
			isoFormat = "ETB";
		} else if (currency.equalsIgnoreCase("ΕΥΡΩ") || currency.equalsIgnoreCase("Ευρώ")) {	         
			isoFormat = "EUR";
		} else if (currency.equalsIgnoreCase("Δολάριο Φίτζι")) {	         
			isoFormat = "FJD";
		} else if (currency.equalsIgnoreCase("Λίρα των Νησιών Φώκλαντ (Μαλβίνες)")) {
			isoFormat = "FKP";
		} else if (currency.equalsIgnoreCase("Λίρα Στερλίνα")) {
			isoFormat = "GBP";
		} else if (currency.equalsIgnoreCase("Λάρι Γεωργίας")) {	         
			isoFormat = "GEL";
		} else if (currency.equalsIgnoreCase("Λίρα Γκέρνσεϊ")) {	         
			isoFormat = "GGP";
		} else if (currency.equalsIgnoreCase("Σέντι της Γκάνας")) {	         
			isoFormat = "GHS";
		} else if (currency.equalsIgnoreCase("Λίρα Γιβραλτάρ")) {
			isoFormat = "GIP";
		} else if (currency.equalsIgnoreCase("Νταλάζι Γκάμπια")) {
			isoFormat = "GMD";
		} else if (currency.equalsIgnoreCase("Φράγκο Γουινέας")) {	         
			isoFormat = "GNF";
		} else if (currency.equalsIgnoreCase("Κετσάλ Γουατεμάλας")) {	         
			isoFormat = "GTQ";
		} else if (currency.equalsIgnoreCase("Δολάριο Γουιάνας")) {	         
			isoFormat = "GYD";
		} else if (currency.equalsIgnoreCase("Δολάριο Χονγκ Κονγκ")) {
			isoFormat = "HKD";
		} else if (currency.equalsIgnoreCase("Λεμπίρα Ονδούρας")) {
			isoFormat = "HNL";
		} else if (currency.equalsIgnoreCase("Κούνα Κροατίας")) {	         
			isoFormat = "HRK";
		} else if (currency.equalsIgnoreCase("Γκουρντ Αϊτής")) {	         
			isoFormat = "HTG";
		} else if (currency.equalsIgnoreCase("Φόριντ Ουγγαρίας")) {	         
			isoFormat = "HUF";
		} else if (currency.equalsIgnoreCase("Ρουπία Ινδονησίας")) {
			isoFormat = "IDR";
		} else if (currency.equalsIgnoreCase("Νεο Σεκέλ Ισραήλ")) {
			isoFormat = "ILS";
		} else if (currency.equalsIgnoreCase("Λίρα Νήσου του Μαν")) {	         
			isoFormat = "IMP";
		} else if (currency.equalsIgnoreCase("Ρουπία Ινδίας")) {	         
			isoFormat = "INR";
		} else if (currency.equalsIgnoreCase("Δηνάριο Ιράκ")) {	         
			isoFormat = "IQD";
		} else if (currency.equalsIgnoreCase("Ριάλι Ιράν")) {
			isoFormat = "IRR";
		} else if (currency.equalsIgnoreCase("Κορώνα Ισλανδίας")) {
			isoFormat = "ISK";
		} else if (currency.equalsIgnoreCase("Λίρα Τζέρσεϊ")) {	         
			isoFormat = "JEP";
		} else if (currency.equalsIgnoreCase("Δολάριο Τζαμάικας")) {	         
			isoFormat = "JMD";
		} else if (currency.equalsIgnoreCase("Δηνάριο Ιορδανίας")) {	         
			isoFormat = "JOD";
		} else if (currency.equalsIgnoreCase("Γιεν Ιαπωνίας")) {
			isoFormat = "JPY";
		} else if (currency.equalsIgnoreCase("Σελίνι Κένυας")) {
			isoFormat = "KES";
		} else if (currency.equalsIgnoreCase("Σομ της Κιργιζίας")) {	         
			isoFormat = "KGS";
		} else if (currency.equalsIgnoreCase("Ριέλ Καμπότζης")) {	         
			isoFormat = "KHR";
		} else if (currency.equalsIgnoreCase("Φράγκο Κομορών")) {	         
			isoFormat = "KMF";
		} else if (currency.equalsIgnoreCase("Ουόν Βόρειας Κορέας")) {
			isoFormat = "KPW";
		} else if (currency.equalsIgnoreCase("Ουόν Νότια Κορέας")) {
			isoFormat = "KRW";
		} else if (currency.equalsIgnoreCase("Δηνάριο Κουβέιτ")) {	         
			isoFormat = "KWD";
		} else if (currency.equalsIgnoreCase("Δολάριο Νησιών Κέιμαν")) {	         
			isoFormat = "KYD";
		} else if (currency.equalsIgnoreCase("Τένγκε Καζακστάν")) {	         
			isoFormat = "KZT";
		} else if (currency.equalsIgnoreCase("Κιπ Λάος")) {
			isoFormat = "LAK";
		} else if (currency.equalsIgnoreCase("Λίρα του Λιβάνου")) {
			isoFormat = "LBP";
		} else if (currency.equalsIgnoreCase("Ρουπία Σρι Λάνκα")) {	         
			isoFormat = "LKR";
		} else if (currency.equalsIgnoreCase("Δολάριο Λιβερίας")) {	         
			isoFormat = "LRD";
		} else if (currency.equalsIgnoreCase("Λότι Λεσότο")) {	         
			isoFormat = "LSL";
		} else if ( (currency.equalsIgnoreCase("Λίτας Λιθουανίας")) || (currency.equalsIgnoreCase("Λιθουανικό Λίτας")) ){	         
			isoFormat = "LTL";
		} else if ( (currency.equalsIgnoreCase("Λατς Λετονίας")) || (currency.equalsIgnoreCase("Λεττονικό Λατς")) ) {	         
			isoFormat = "LVL";
		} else if (currency.equalsIgnoreCase("Δηνάριο Λιβύης")) {
			isoFormat = "LYD";
		} else if (currency.equalsIgnoreCase("Ντιρχάμ Μαρόκου")) {
			isoFormat = "MAD";
		} else if (currency.equalsIgnoreCase("Λέου Μολδαβία")) {	         
			isoFormat = "MDL";
		} else if (currency.equalsIgnoreCase("Αριάρι Μαδαγασκάρης")) {	         
			isoFormat = "MGA";
		} else if (currency.equalsIgnoreCase("Δηνάριο της ΠΓΔΜ")) {	         
			isoFormat = "MKD";
		} else if (currency.equalsIgnoreCase("Κιάτ Μιανμάρ")) {	         
			isoFormat = "MMK";
		} else if (currency.equalsIgnoreCase("Τουγκρίκ Μογγολίας")) {	         
			isoFormat = "MNT";
		} else if (currency.equalsIgnoreCase("Πατάκα Μακάο")) {
			isoFormat = "MOP";
		} else if (currency.equalsIgnoreCase("Ουγκίγια Μαυριτανίας")) {
			isoFormat = "MRO";
		} else if (currency.equalsIgnoreCase("Λιρέτα Μάλτας")) {	         
			isoFormat = "MTL";
		} else if (currency.equalsIgnoreCase("Ρουπία του Μαυρίκιου")) {	         
			isoFormat = "MUR";
		} else if (currency.equalsIgnoreCase("Ρουφίγια Μαλδίβων")) {	         
			isoFormat = "MVR";
		} else if (currency.equalsIgnoreCase("Κουάτσα του Μαλάουι")) {	         
			isoFormat = "MWK";
		} else if (currency.equalsIgnoreCase("Πέσο Μεξικού")) {	         
			isoFormat = "MXN";
		} else if (currency.equalsIgnoreCase("Ρινγκίτ Μαλαισίας")) {
			isoFormat = "MYR";
		} else if (currency.equalsIgnoreCase("Μετικάλ Μοζαμβίκης")) {
			isoFormat = "MZN";
		} else if (currency.equalsIgnoreCase("Δολάριο Ναμίμπιας")) {	         
			isoFormat = "NAD";
		} else if (currency.equalsIgnoreCase("Νάιρα Νιγηρίας")) {	         
			isoFormat = "NGN";
		} else if (currency.equalsIgnoreCase("Κόρδοβα Νικαράγουας")) {	         
			isoFormat = "NIO";
		} else if (currency.equalsIgnoreCase("Κορώνα Νορβηγίας")) {	         
			isoFormat = "NOK";
		} else if (currency.equalsIgnoreCase("Ρουπία Νεπάλ")) {	         
			isoFormat = "NPR";
		} else if (currency.equalsIgnoreCase("Δολάριο Νέας Ζηλανδίας")) {
			isoFormat = "NZD";
		} else if (currency.equalsIgnoreCase("Ριάλ του Ομάν")) {
			isoFormat = "OMR";
		} else if (currency.equalsIgnoreCase("Μπαλμπόα Παναμά")) {	         
			isoFormat = "PAB";
		} else if (currency.equalsIgnoreCase("Νέο Σολ Περού")) {	         
			isoFormat = "PEN";
		} else if (currency.equalsIgnoreCase("Κίνα Παπούα-Νέας Γουινέας")) {	         
			isoFormat = "PGK";
		} else if (currency.equalsIgnoreCase("Πέσο Φιλιππίνων")) {	         
			isoFormat = "PHP";
		} else if (currency.equalsIgnoreCase("Ρουπία Πακιστάν")) {	         
			isoFormat = "PKR";
		} else if (currency.equalsIgnoreCase("Ζλότι Πολωνίας")) {
			isoFormat = "PLN";
		} else if (currency.equalsIgnoreCase("Γκουαρανί Παραγουάης")) {
			isoFormat = "PYG";
		} else if (currency.equalsIgnoreCase("Ριγιάλ του Κατάρ")) {	         
			isoFormat = "QAR";
		} else if (currency.equalsIgnoreCase("Λέου Ρουμανίας")) {	         
			isoFormat = "ROL";
		} else if (currency.equalsIgnoreCase("Ρουμανία Νέο Λέου")) {	         
			isoFormat = "RON";
		} else if (currency.equalsIgnoreCase("Δηνάριο Σερβίας")) {	         
			isoFormat = "RSD";
		} else if (currency.equalsIgnoreCase("Ρούβλι Ρωσίας")) {	         
			isoFormat = "RUB";
		} else if (currency.equalsIgnoreCase("Φράγκο της Ρουάντα")) {
			isoFormat = "RWF";
		} else if (currency.equalsIgnoreCase("Ριάλ Σαουδικής Αραβίας")) {
			isoFormat = "SAR";
		} else if (currency.equalsIgnoreCase("Δολάριο Νήσων Σολομώντα")) {	         
			isoFormat = "SBD";
		} else if (currency.equalsIgnoreCase("Ρουπία Σεϋχελλών")) {	         
			isoFormat = "SCR";
		} else if (currency.equalsIgnoreCase("Λίρα του Σουδάν")) {	         
			isoFormat = "SDG";
		} else if ( (currency.equalsIgnoreCase("Κορώνα Σουηδίας")) || (currency.equalsIgnoreCase("Σουηδική Κορώνα")) ){	         
			isoFormat = "SEK";
		} else if (currency.equalsIgnoreCase("Δολάριο Σιγκαπούρης")) {	         
			isoFormat = "SGD";
		} else if (currency.equalsIgnoreCase("Λίρα Αγίας Ελένης")) {
			isoFormat = "SHP";
		} else if (currency.equalsIgnoreCase("Κορώνα Σλοβακίας")) {
			isoFormat = "SKK";
		} else if (currency.equalsIgnoreCase("Λεόνε της Σιέρα Λεόνε")) {	         
			isoFormat = "SLL";
		} else if (currency.equalsIgnoreCase("Σελίνι Σομαλίας")) {	         
			isoFormat = "SOS";
		} else if (currency.equalsIgnoreCase("Δολάριο Σουρινάμ")) {	         
			isoFormat = "SRD";
		} else if (currency.equalsIgnoreCase("Ντόμπρα του Σάο Τομέ και Πρίνσιπε")) {	         
			isoFormat = "STD";
		} else if (currency.equalsIgnoreCase("Ελ Σαλβαδόρ Κολόν")) {	         
			isoFormat = "SVC";
		} else if (currency.equalsIgnoreCase("Λίρα Συρίας")) {
			isoFormat = "SYP";
		} else if (currency.equalsIgnoreCase("Λιλανγκένι Σουαζιλάνδης")) {
			isoFormat = "SZL";
		} else if (currency.equalsIgnoreCase("Μπατ Ταϊλάνδης")) {	         
			isoFormat = "THB";
		} else if (currency.equalsIgnoreCase("Σομόνι Τατζικιστάν")) {	         
			isoFormat = "TJS";
		} else if (currency.equalsIgnoreCase("Μανάτ του Τουρκμενιστάν")) {	         
			isoFormat = "TMT";
		} else if (currency.equalsIgnoreCase("Δηνάριο Τυνησίας")) {	         
			isoFormat = "TND";
		} else if (currency.equalsIgnoreCase("Παάνγκα της Τόνγκα")) {	         
			isoFormat = "TOP";
		} else if (currency.equalsIgnoreCase("Λίρα Τουρκίας")) {
			isoFormat = "TRY";
		} else if (currency.equalsIgnoreCase("Δολάριο Τρινιδάδ και Τομπάγκο")) {
			isoFormat = "TTD";
		} else if (currency.equalsIgnoreCase("Δολάριο Τουβαλού")) {	         
			isoFormat = "TVD";
		} else if (currency.equalsIgnoreCase("Νέο Δολάριο Ταϊβάν")) {	         
			isoFormat = "TWD";
		} else if (currency.equalsIgnoreCase("Σελίνι Τανζανίας")) {	         
			isoFormat = "TZS";
		} else if (currency.equalsIgnoreCase("Γρίβνα Ουκρανίας")) {	         
			isoFormat = "UAH";
		} else if (currency.equalsIgnoreCase("Σελίνι της Ουγκάντας")) {	         
			isoFormat = "UGX";
		} else if (currency.equalsIgnoreCase("Δολάριο ΗΠΑ") || currency.equalsIgnoreCase("Δολάρια")) {
			isoFormat = "USD";
		} else if (currency.equalsIgnoreCase("Πέσο Ουρουγουάης")) {
			isoFormat = "UYU";
		} else if (currency.equalsIgnoreCase("Σομ του Ουζμπεκιστάν")) {	         
			isoFormat = "UZS";
		} else if (currency.equalsIgnoreCase("Μπολίβαρ Βενεζουέλας")) {	         
			isoFormat = "VEF";
		} else if (currency.equalsIgnoreCase("Ντονγκ του Βιετνάμ")) {	         
			isoFormat = "VND";
		} else if (currency.equalsIgnoreCase("Βάτου του Βανουάτου")) {	         
			isoFormat = "VUV";
		} else if (currency.equalsIgnoreCase("Τάλα του Σαμόα")) {	         
			isoFormat = "WST";
		} else if (currency.equalsIgnoreCase("Φράγκο CFA Κεντρικής Αφρικής")) {
			isoFormat = "XAF";
		} else if (currency.equalsIgnoreCase("Δολάριο Ανατολικής Καραϊβικής")) {
			isoFormat = "XCD";
		} else if (currency.equalsIgnoreCase("Διεθνές Νομισματικό Ταμείο (ΔΝΤ)")) {	         
			isoFormat = "XDR";
		} else if (currency.equalsIgnoreCase("Φράγκο CFA Δυτικής Αφρικής")) {	         
			isoFormat = "XOF";
		} else if (currency.equalsIgnoreCase("Φράγκο CFP")) {	         
			isoFormat = "XPF";
		} else if (currency.equalsIgnoreCase("Ριάλ Υεμένης")) {	         
			isoFormat = "YER";
		} else if (currency.equalsIgnoreCase("Ραντ Νοτίου Αφρικής")) {	         
			isoFormat = "ZAR";
		} else if (currency.equalsIgnoreCase("Κουάτσα της Ζάμπιας")) {
			isoFormat = "ZMW";
		} else if (currency.equalsIgnoreCase("Δολάριο Ζιμπάμπουε")) {
			isoFormat = "ZWD";
		} else {
			writeUnknownCurrency(currency);
		}
		
		return isoFormat;
	}
	
	/**
     * Find the the official abbreviation of the country.
     * 
     * @param String the country from the CSV file
     * @return String the official abbreviation of the country
     */
	public String findCountryAbbreviation(String country) {
		
		String abbreviation = null;
		
		if ( country.contains("Ελλάδα") || country.contains("ΕΛΛΑ") ) {
			abbreviation = "EL";
		} else if (country.contains("Γερμανία")) {
			abbreviation = "DE";
		} else if (country.contains("Αυστρία")) {
			abbreviation = "AT";
		} else if ( country.contains("ΗΠΑ") || country.contains("Ηνωμένες Πολ. της Αμερικής") || country.contains("Ηνωμένες Πολιτείες Αμερικής") ) {
			abbreviation = "US";
		} else if (country.contains("Γαλλία")) {
			abbreviation = "FR";
		} else if (country.contains("Ιταλία")) {
			abbreviation = "IT";
		} else if (country.contains("Καναδάς")) {
			abbreviation = "CA";
		} else if (country.contains("Κίνα")) {
			abbreviation = "CN";
		} else if (country.contains("Ιράν")) {
			abbreviation = "IR";
		} else if (country.contains("Ηνωμένο Βασίλειο")) {
			abbreviation = "GB";
		} else if (country.contains("Τουρκία")) {
			abbreviation = "TR";
		} else if (country.contains("Ιαπωνία")) {
			abbreviation = "JP";
		} else if (country.contains("Ερυθραία")) {
			abbreviation = "ER";
		} else if (country.contains("Ελβετία")) {
			abbreviation = "CH";
		} else if (country.contains("Γκάνα")) {
			abbreviation = "GH";
		} else if (country.contains("Μπελίζ")) {
			abbreviation = "BZ";
		} else if (country.contains("Ενωμένα Αραβικά Εμιράτα")) {
			abbreviation = "AE";
		} else if (country.contains("Ολλανδικές Αντίλλες")) {
			abbreviation = "AN";
		} else if (country.contains("Κύπρος")) {
			abbreviation = "CY";
		} else if (country.contains("Ισπανία")) {
			abbreviation = "ES";
		} else if (country.contains("Γεωργία")) {
			abbreviation = "GE";
		} else if (country.contains("Βέλγιο")) {
			abbreviation = "BE";
		} else if (country.contains("Ανδόρρα")) {
			abbreviation = "AD";
		} else if (country.contains("Ινδονησία")) {
			abbreviation = "ID";
		} else if (country.contains("Φινλανδία")) {
			abbreviation = "FI";
		} else if (country.contains("Δανία")) {
			abbreviation = "DK";
		} else if (country.contains("European Community") || country.contains("Ευραπαϊκή Κοινότητα")) {
			abbreviation = "EU";
		} else if (country.contains("Αφγανιστάν")) {
			abbreviation = "AF";
		} else if (country.contains("Αντίγκουα και Μπαρμπούντα")) {
			abbreviation = "AG";
		} else if (country.contains("Ανγκουίλα")) {
			abbreviation = "AI";
		} else if (country.contains("Αλβανία")) {
			abbreviation = "AL";
		} else if (country.contains("Αρμενία")) {
			abbreviation = "AM";
		} else if (country.contains("Αγκόλα")) {
			abbreviation = "AO";
		} else if (country.contains("Ανταρκτική")) {
			abbreviation = "AQ";
		} else if (country.contains("Αργεντινή")) {
			abbreviation = "AR";
		} else if (country.contains("Αμερικανικές Σαμόα")) {
			abbreviation = "AS";
		} else if (country.contains("Αυστραλία")) {
			abbreviation = "AU";
		} else if (country.contains("Αρούμπα")) {
			abbreviation = "AW";
		} else if (country.contains("Αζερμπαϊτζάν")) {
			abbreviation = "AZ";
		} else if (country.contains("Βοσνία-Ερζεγοβίνη")) {
			abbreviation = "BA";
		} else if (country.contains("Μπαρμπάντος")) {
			abbreviation = "BB";
		} else if (country.contains("Μπαγκλαντές")) {
			abbreviation = "BD";
		} else if (country.contains("Μπουρκίνα Φάσο")) {
			abbreviation = "BF";
		} else if (country.contains("Βουλγαρία")) {
			abbreviation = "BG";
		} else if (country.contains("Μπαχρέιν")) {
			abbreviation = "BH";
		} else if (country.contains("Μπουρούντι")) {
			abbreviation = "BI";
		} else if (country.contains("Μπενίν")) {
			abbreviation = "BJ";
		} else if (country.contains("Βερμούδες")) {
			abbreviation = "BM";
		} else if (country.contains("Μπρουνέι")) {
			abbreviation = "BN";
		} else if (country.contains("Βολιβία")) {
			abbreviation = "BO";
		} else if (country.contains("Βραζιλία")) {
			abbreviation = "BR";
		} else if (country.contains("Μπαχάμες")) {
			abbreviation = "BS";
		} else if (country.contains("Μπουτάν")) {
			abbreviation = "BT";
		} else if (country.contains("Μπουβέ")) {
			abbreviation = "BV";
		} else if (country.contains("Μποτσουάνα")) {
			abbreviation = "BW";
		} else if (country.contains("Λευκορωσία")) {
			abbreviation = "BY";
		} else if (country.contains("Κόκος") || country.contains("Κήλινγκ")) {
			abbreviation = "CC";
		} else if (country.contains("Δημοκρατία του Κονγκό")) {
			abbreviation = "CD";
		} else if (country.contains("Δημοκρατία Κεντρικής Αφρικής")) {
			abbreviation = "CF";
		} else if (country.contains("Κονγκό")) {
			abbreviation = "CG";
		} else if (country.contains("Ακτή Ελεφαντοστού")) {
			abbreviation = "CI";
		} else if (country.contains("Κουκ")) {
			abbreviation = "CK";
		} else if (country.contains("Χιλή")) {
			abbreviation = "CL";
		} else if (country.contains("Καμερούν")) {
			abbreviation = "CM";
		} else if (country.contains("Κολομβία")) {
			abbreviation = "CO";
		} else if (country.contains("Κόστα Ρίκα")) {
			abbreviation = "CR";
		} else if (country.contains("Κούβα")) {
			abbreviation = "CU";
		} else if (country.contains("Πράσινο Ακρωτήρι")) {
			abbreviation = "CV";
		} else if (country.contains("Χριστουγέννων")) {
			abbreviation = "CX";
		} else if (country.contains("Δημοκρατία της Τσεχίας") || country.contains("Τσεχία")) {
			abbreviation = "CZ";
		} else if (country.contains("Τζιμπουτί")) {
			abbreviation = "DJ";
		} else if (country.contains("Ντομίνικα")) {
			abbreviation = "DM";
		} else if (country.contains("Δομινικανή Δημοκρατία")) {
			abbreviation = "DO";
		} else if (country.contains("Αλγερία")) {
			abbreviation = "DZ";
		} else if (country.contains("Ισημερινός")) {
			abbreviation = "EC";
		} else if (country.contains("Εσθονία")) {
			abbreviation = "EE";
		} else if (country.contains("Αίγυπτος")) {
			abbreviation = "EG";
		} else if (country.contains("Αιθιοποία")) {
			abbreviation = "ET";
		} else if (country.contains("Φίτζι")) {
			abbreviation = "FJ";
		} else if (country.contains("Νήσοι Φώκλαντ")) {
			abbreviation = "FK";
		} else if (country.contains("Μικρονησία")) {
			abbreviation = "FM";
		} else if (country.contains("Νήσοι Φερόε")) {
			abbreviation = "FO";
		} else if (country.contains("Γκαμπόν")) {
			abbreviation = "GA";
		} else if (country.contains("Γρενάδα")) {
			abbreviation = "GD";
		} else if (country.contains("Γιβραλτάρ")) {
			abbreviation = "GI";
		} else if (country.contains("Γροιλανδία")) {
			abbreviation = "GL";
		} else if (country.contains("Γκάμπια")) {
			abbreviation = "GM";
		} else if (country.contains("Γουινέα")) {
			abbreviation = "GN";
		} else if (country.contains("Ισημερινή Γουινέα")) {
			abbreviation = "GQ";
		} else if (country.contains("Νότια Γεωργία και νήσοι Νότιες Σάντουιτς")) {
			abbreviation = "GS";
		} else if (country.contains("Γουατεμάλα")) {
			abbreviation = "GT";
		} else if (country.contains("Γκουάμ")) {
			abbreviation = "GU";
		} else if (country.contains("Γουινέα Μπισσάου")) {
			abbreviation = "GW";
		} else if (country.contains("Γουιάνα")) {
			abbreviation = "GY";
		} else if (country.contains("Χονγκ Κονγκ")) {
			abbreviation = "HK";
		} else if (country.contains("Χέρντ και Μακντόναλντ")) {
			abbreviation = "HM";
		} else if (country.contains("Ονδούρα")) {
			abbreviation = "HN";
		} else if (country.contains("Κροατία")) {
			abbreviation = "HR";
		} else if (country.contains("Αϊτή")) {
			abbreviation = "HT";
		} else if (country.contains("Ουγγαρία")) {
			abbreviation = "HU";
		} else if (country.contains("Ιρλανδία")) {
			abbreviation = "IE";
		} else if (country.contains("Ισραήλ")) {
			abbreviation = "IL";
		} else if (country.contains("Ινδία")) {
			abbreviation = "IN";
		} else if (country.contains("Βρετανικό Έδαφος Ινδ. Ωκεανού")) {
			abbreviation = "IO";
		} else if (country.contains("Ιράκ")) {
			abbreviation = "IQ";
		} else if (country.contains("Ισλανδία")) {
			abbreviation = "IS";
		} else if (country.contains("Ιαμαϊκή") || country.contains("Τζαμάικα")) {
			abbreviation = "JM";
		} else if (country.contains("Ιορδανία ")) {
			abbreviation = "JO";
		} else if (country.contains("Κένυα")) {
			abbreviation = "KE";
		} else if (country.contains("Κιργιζία")) {
			abbreviation = "KG";
		} else if (country.contains("Καμπότζη")) {
			abbreviation = "KH";
		} else if (country.contains("Κιριμπάτι")) {
			abbreviation = "KI";
		} else if (country.contains("Κομόρες (δεν συμπεριλ. η Μαγιότ)")) {
			abbreviation = "KM";
		} else if (country.contains("Άγιος Χριστόφορος και Νέβις")) {
			abbreviation = "KN";
		} else if (country.contains("Βόρεια Κορέα")) {
			abbreviation = "KP";
		} else if (country.contains("Νότια Κορέα")) {
			abbreviation = "KR";
		} else if (country.contains("Κουβέιτ")) {
			abbreviation = "KW";
		} else if (country.contains("Κάυμαν") || country.contains("Κέιμαν")) {
			abbreviation = "KY";
		} else if (country.contains("Καζακστάν")) {
			abbreviation = "KZ";
		} else if (country.contains("Λάος")) {
			abbreviation = "LA";
		} else if (country.contains("Λίβανος")) {
			abbreviation = "LB";
		} else if (country.contains("Αγία Λουκία")) {
			abbreviation = "LC";
		} else if (country.contains("Λιχτενστάϊν")) {
			abbreviation = "LI";
		} else if (country.contains("Σρι Λάνκα")) {
			abbreviation = "LK";
		} else if (country.contains("Λιβερία")) {
			abbreviation = "LR";
		} else if (country.contains("Λεσόθο")) {
			abbreviation = "LS";
		} else if (country.contains("Λιθουανία")) {
			abbreviation = "LT";
		} else if (country.contains("Λουξεμβούργο")) {
			abbreviation = "LU";
		} else if (country.contains("Λετονία")) {
			abbreviation = "LV";
		} else if (country.contains("Λιβύη")) {
			abbreviation = "LY";
		} else if (country.contains("Μαρόκo")) {
			abbreviation = "MA";
		} else if (country.contains("Μολδαβία")) {
			abbreviation = "MD";
		} else if (country.contains("Μαυροβούνιο")) {
			abbreviation = "ME";
		} else if (country.contains("Μαδαγασκάρη")) {
			abbreviation = "MG";
		} else if (country.contains("Νήσοι Μάρσαλ")) {
			abbreviation = "MH";
		} else if (country.contains("Πρώην Γιουγκοσλαβική Δημοκρατία της Μακεδονίας") || country.contains("FYROM") || country.contains("Σκόπια")) {
			abbreviation = "MK";
		} else if (country.contains("Μάλι")) {
			abbreviation = "ML";
		} else if (country.contains("Μιανμάρ")) {
			abbreviation = "MM";
		} else if (country.contains("Μογγολία")) {
			abbreviation = "MN";
		} else if (country.contains("Μακάο")) {
			abbreviation = "MO";
		} else if (country.contains("Βόρειες Μαριάνες")) {
			abbreviation = "MP";
		} else if (country.contains("Μαυριτανία")) {
			abbreviation = "MR";
		} else if (country.contains("Μοντσεράτ")) {
			abbreviation = "MS";
		} else if (country.contains("Μάλτα")) {
			abbreviation = "MT";
		} else if (country.contains("Μαυρίκιος")) {
			abbreviation = "MU";
		} else if (country.contains("Μαλδίβες")) {
			abbreviation = "MV";
		} else if (country.contains("Μαλάουι")) {
			abbreviation = "MW";
		} else if (country.contains("Μεξικό")) {
			abbreviation = "MX";
		} else if (country.contains("Μαλαισία")) {
			abbreviation = "MY";
		} else if (country.contains("Μοζαμβίκη")) {
			abbreviation = "MZ";
		} else if (country.contains("Ναμίμπια")) {
			abbreviation = "NA";
		} else if (country.contains("Νέα Καληδονία")) {
			abbreviation = "NC";
		} else if (country.contains("Νίγηρ")) {
			abbreviation = "NE";
		} else if (country.contains("Νόρφολκ")) {
			abbreviation = "NF";
		} else if (country.contains("Νιγηρία")) {
			abbreviation = "NG";
		} else if (country.contains("Νικαράγουα")) {
			abbreviation = "NI";
		} else if (country.contains("Κάτω Χώρες")  || country.contains("Ολλανδία")) {
			abbreviation = "NL";
		} else if (country.contains("Νορβηγία")) {
			abbreviation = "NO";
		} else if (country.contains("Νεπάλ")) {
			abbreviation = "NP";
		} else if (country.contains("Ναούρου")) {
			abbreviation = "NR";
		} else if (country.contains("Νίουε")) {
			abbreviation = "NU";
		} else if (country.contains("Νέα Ζηλανδία")) {
			abbreviation = "NZ";
		} else if (country.contains("Ομάν")) {
			abbreviation = "OM";
		} else if (country.contains("Παναμάς")) {
			abbreviation = "PA";
		} else if (country.contains("Περού")) {
			abbreviation = "PE";
		} else if (country.contains("Γαλλική Πολυνησία")) {
			abbreviation = "PF";
		} else if (country.contains("Πάπουα Νέα Γουινέα")) {
			abbreviation = "PG";
		} else if (country.contains("Φιλιππίνες")) {
			abbreviation = "PH";
		} else if (country.contains("Πακιστάν")) {
			abbreviation = "PK";
		} else if (country.contains("Πολωνία")) {
			abbreviation = "PL";
		} else if (country.contains("Άγιος Πέτρος και Μικελόν")) {
			abbreviation = "PM";
		} else if (country.contains("Πιτκαίρν")) {
			abbreviation = "PN";
		} else if (country.contains("Κατεχόμενο παλαιστινιακό έδαφος")) {
			abbreviation = "PS";
		} else if (country.contains("Πορτογαλία")) {
			abbreviation = "PT";
		} else if (country.contains("Παλάου")) {
			abbreviation = "PW";
		} else if (country.contains("Παραγουάη")) {
			abbreviation = "PY";
		} else if (country.contains("Κατάρ")) {
			abbreviation = "QA";
		} else if (country.contains("Εφοδιασμός σκαφών")) {
			abbreviation = "QQ";
		} else if (country.contains("Εφοδιασμός σκαφών στο πλαίσιο των ενδοκοινοτικών συναλλαγών")) {
			abbreviation = "QR";
		} else if (country.contains("Εφοδιασμός σκαφών στο πλαίσιο των συναλλαγών με τρίτες χώρες")) {
			abbreviation = "QS";
		} else if (country.contains("Μη καθοριζόμενες χώρες και εδάφη")) {
			abbreviation = "QU";
		} else if (country.contains("Χώρες και εδάφη που δεν καθορίζονται στο πλαίσιο των ενδοκοινοτικών συναλλαγών")) {
			abbreviation = "QV";
		} else if (country.contains("Χώρες και εδάφη που δεν καθορίζονται στο πλαίσιο των συναλλαγών με τρίτες χώρες")) {
			abbreviation = "QW";
		} else if (country.contains("Χώρες και εδάφη μη κατονομαζόμενα για εμπορικούς ή στρατιωτικούς λόγους")) {
			abbreviation = "QX";
		} else if (country.contains("QX στο πλαίσιο των ενδοκοινοτικών συναλλαγών")) {
			abbreviation = "QY";
		} else if (country.contains("QX στο πλαίσιο των συναλλαγών με τρίτες χώρες")) {
			abbreviation = "QZ";
		} else if (country.contains("Ρουμανία")) {
			abbreviation = "RO";
		} else if (country.contains("Ρωσία")) {
			abbreviation = "RU";
		} else if (country.contains("Ρουάντα")) {
			abbreviation = "RW";
		} else if (country.contains("Σαουδική Αραβία")) {
			abbreviation = "SA";
		} else if (country.contains("Νήσοι Σολομώντος")) {
			abbreviation = "SB";
		} else if (country.contains("Σεϋχέλλες")) {
			abbreviation = "SC";
		} else if (country.contains("Σουδάν")) {
			abbreviation = "SD";
		} else if (country.contains("Σουηδία")) {
			abbreviation = "SE";
		} else if (country.contains("Σιγκαπούρη")) {
			abbreviation = "SG";
		} else if (country.contains("Αγία Ελένη")) {
			abbreviation = "SH";
		} else if (country.contains("Σλοβενία")) {
			abbreviation = "SI";
		} else if (country.contains("Σλοβακία")) {
			abbreviation = "SK";
		} else if (country.contains("Σιέρρα Λεόνε")) {
			abbreviation = "SL";
		} else if (country.contains("Άγιος Μαρίνος")) {
			abbreviation = "SM";
		} else if (country.contains("Σενεγάλη")) {
			abbreviation = "SN";
		} else if (country.contains("Σομαλία")) {
			abbreviation = "SO";
		} else if (country.contains("Σουρινάμ")) {
			abbreviation = "SR";
		} else if (country.contains("Σάο Τομέ και Πρίνσιπε")) {
			abbreviation = "ST";
		} else if (country.contains("Ελ Σαλβαδόρ")) {
			abbreviation = "SV";
		} else if (country.contains("Συρία")) {
			abbreviation = "SY";
		} else if (country.contains("Σουαζιλάνδη")) {
			abbreviation = "SZ";
		} else if (country.contains("Νήσοι Τερκς και Κάικος")) {
			abbreviation = "TC";
		} else if (country.contains("Τσαντ")) {
			abbreviation = "TD";
		} else if (country.contains("Γαλλικά νότια εδάφη")) {
			abbreviation = "TF";
		} else if (country.contains("Τόγκο")) {
			abbreviation = "TG";
		} else if (country.contains("Ταϊλάνδη")) {
			abbreviation = "TH";
		} else if (country.contains("Τατζικιστάν")) {
			abbreviation = "TJ";
		} else if (country.contains("Τουκελάου")) {
			abbreviation = "TK";
		} else if (country.contains("Ανατολικό Τιμόρ")) {
			abbreviation = "TL";
		} else if (country.contains("Τουρκμενιστάν")) {
			abbreviation = "TM";
		} else if (country.contains("Τυνησία")) {
			abbreviation = "TN";
		} else if (country.contains("Τόνγκα")) {
			abbreviation = "TO";
		} else if (country.contains("Τρινιδάδ και Τομπάγκο") || country.contains("Τρινιντάντ και Τομπάγκο")) {
			abbreviation = "TT";
		} else if (country.contains("Τουβαλού")) {
			abbreviation = "TV";
		} else if (country.contains("Ταϊβάν")) {
			abbreviation = "TW";
		} else if (country.contains("Τανζανία")) {
			abbreviation = "TZ";
		} else if (country.contains("Ουκρανία")) {
			abbreviation = "UA";
		} else if (country.contains("Ουγκάντα")) {
			abbreviation = "UG";
		} else if (country.contains("Μικρές νήσοι απομεμακρυσμένες από τις Ηνωμένες Πολιτείες")) {
			abbreviation = "UM";
		} else if (country.contains("Ουραγουάη")) {
			abbreviation = "UY";
		} else if (country.contains("Ουζμπεκιστάν")) {
			abbreviation = "UZ";
		} else if (country.contains("Πόλη του Βατικανού")) {
			abbreviation = "VA";
		} else if (country.contains("Άγιος Βικέντιος και Γρεναδίνες")) {
			abbreviation = "VC";
		} else if (country.contains("Βενεζουέλα")) {
			abbreviation = "VE";
		} else if (country.contains("Βρετανικοί Παρθένοι Νήσοι")) {
			abbreviation = "VG";
		} else if (country.contains("Παρθένοι Νήσοι των Ηνωμ. Πολιτειών")) {
			abbreviation = "VI";
		} else if (country.contains("Βιετνάμ")) {
			abbreviation = "VN";
		} else if (country.contains("Βανουάτου")) {
			abbreviation = "VU";
		} else if (country.contains("Νήσοι Ουώλις και Φουτούνα")) {
			abbreviation = "WF";
		} else if (country.contains("Σαμόα")) {
			abbreviation = "WS";
		} else if (country.contains("Θέουτα")) {
			abbreviation = "XC";
		} else if (country.contains("Κόσοβο")) {
			abbreviation = "XK";
		} else if (country.contains("Μελίλια")) {
			abbreviation = "XL";
		} else if (country.contains("Σερβία")) {
			abbreviation = "XS";
		} else if (country.contains("Υεμένη")) {
			abbreviation = "YE";
		} else if (country.contains("Μαγιότ")) {
			abbreviation = "YT";
		} else if (country.contains("Νότια Αφρική")) {
			abbreviation = "ZA";
		} else if (country.contains("Ζάμπια")) {
			abbreviation = "ZM";
		} else if (country.contains("Ζιμπάμπουε")) {
			abbreviation = "ZW";
		} else {
			writeUnknownCountry(abbreviation);
		}
		
		return abbreviation;
	}
	
	/*public String findCountryLatinName(String country) {
		
		String latinName = null;
		
		if (country.contains("Ελλάδα")) {
			latinName = "Greece";
		} else if (country.contains("Γερμανία")) {
			latinName = "Germany";
		} else if (country.contains("Αυστρία")) {
			latinName = "Austria";
		} else if ( country.contains("ΗΠΑ") || country.contains("Ηνωμένες Πολ. της Αμερικής") || country.contains("Ηνωμένες Πολιτείες Αμερικής") ) {
			latinName = "USA";
		} else if (country.contains("Γαλλία")) {
			latinName = "France";
		}  else if (country.contains("Ιταλία")) {
			latinName = "Italy";
		} else if (country.contains("Καναδάς")) {
			latinName = "Canada";
		} else if (country.contains("Κίνα")) {
			latinName = "China";
		} else if (country.contains("Ιράν")) {
			latinName = "Iran";
		} else if (country.contains("Ηνωμένο Βασίλειο") || country.contains("Μεγάλη Βρετανία")) {
			latinName = "Great Britain";
		} else if (country.contains("Τουρκία")) {
			latinName = "Turkey";
		} else if (country.contains("Ιαπωνία")) {
			latinName = "Japan";
		} else if (country.contains("Ερυθραία")) {
			latinName = "Eritrea";
		} else if (country.contains("Ελβετία")) {
			latinName = "Switzerland";
		} else if (country.contains("Γκάνα")) {
			latinName = "Ghana";
		} else if (country.contains("Μπελίζ")) {
			latinName = "Beliz";
		} else if (country.contains("Ενωμένα Αραβικά Εμιράτα")) {
			latinName = "UAE";
		} else if (country.contains("Ολλανδικές Αντίλλες")) {
			latinName = "Netherlands Antilles";
		} else if (country.contains("Κύπρος")) {
			latinName = "Cyprus";
		} else if (country.contains("Ισπανία")) {
			latinName = "Spain (not incl XC, XL)";
		} else if (country.contains("Γεωργία")) {
			latinName = "Georgia";
		} else if (country.contains("Βέλγιο")) {
			latinName = "Belgium";
		} else if (country.contains("Ανδόρρα")) {
			latinName = "Andorra";
		} else if (country.contains("Ινδονησία")) {
			latinName = "Indonesia";
		} else if (country.contains("Φινλανδία")) {
			latinName = "Finland";
		} else if (country.contains("Δανία")) {
			latinName = "Denmark";
		} else if (country.contains("European Community") || country.contains("Ευραπαϊκή Κοινότητα")) {
			latinName = "European Community";
		} else if (country.contains("Αφγανιστάν")) {
			latinName = "Afghanistan";
		} else if (country.contains("Αντίγκουα και Μπαρμπούντα")) {
			latinName = "Antigua and Barbuda";
		} else if (country.contains("Ανγκουίλα")) {
			latinName = "Anguilla";
		} else if (country.contains("Αλβανία")) {
			latinName = "Albania";
		} else if (country.contains("Αρμενία")) {
			latinName = "Armenia";
		} else if (country.contains("Αγκόλα")) {
			latinName = "Angola";
		} else if (country.contains("Ανταρκτική")) {
			latinName = "Antarctic";
		} else if (country.contains("Αργεντινή")) {
			latinName = "Argentina";
		} else if (country.contains("Αμερικανικές Σαμόα")) {
			latinName = "American Samoa";
		} else if (country.contains("Αυστραλία")) {
			latinName = "Australia";
		} else if (country.contains("Αρούμπα")) {
			latinName = "Aruba";
		} else if (country.contains("Αζερμπαϊτζάν")) {
			latinName = "Azerbaijan";
		} else if (country.contains("Βοσνία-Ερζεγοβίνη")) {
			latinName = "Bosnia-Herzegovina";
		} else if (country.contains("Μπαρμπάντος")) {
			latinName = "Barbados";
		} else if (country.contains("Μπαγκλαντές")) {
			latinName = "Bangladesh";
		} else if (country.contains("Μπουρκίνα Φάσο")) {
			latinName = "Burkina Faso";
		} else if (country.contains("Βουλγαρία")) {
			latinName = "Bulgaria";
		} else if (country.contains("Μπαχρέιν")) {
			latinName = "Bahrain";
		} else if (country.contains("Μπουρούντι")) {
			latinName = "Burundi";
		} else if (country.contains("Μπενίν")) {
			latinName = "Benin";
		} else if (country.contains("Βερμούδες")) {
			latinName = "Bermuda";
		} else if (country.contains("Μπρουνέι")) {
			latinName = "Brunei";
		} else if (country.contains("Βολιβία")) {
			latinName = "Bolivia";
		} else if (country.contains("Βραζιλία")) {
			latinName = "Brazil";
		} else if (country.contains("Μπαχάμες")) {
			latinName = "Bahamas";
		} else if (country.contains("Μπουτάν")) {
			latinName = "Bhutan";
		} else if (country.contains("Μπουβέ")) {
			latinName = "Bouvet";
		} else if (country.contains("Μποτσουάνα")) {
			latinName = "Botswana";
		} else if (country.contains("Λευκορωσία")) {
			latinName = "Belarus";
		} else if (country.contains("Κόκος") || country.contains("Κήλινγκ")) {
			latinName = "Cocos (Keeling) Islands";
		} else if (country.contains("Δημοκρατία του Κονγκό")) {
			latinName = "Republic of Congo";
		} else if (country.contains("Δημοκρατία Κεντρικής Αφρικής")) {
			latinName = "Central African Republic";
		} else if (country.contains("Κονγκό")) {
			latinName = "Republic of Congo";
		} else if (country.contains("Ακτή Ελεφαντοστού")) {
			latinName = "IvoryCoast";
		} else if (country.contains("Κουκ")) {
			latinName = "Cook";
		} else if (country.contains("Χιλή")) {
			latinName = "Chile";
		} else if (country.contains("Καμερούν")) {
			latinName = "Cameroon";
		} else if (country.contains("Κολομβία")) {
			latinName = "Colombia";
		} else if (country.contains("Κόστα Ρίκα")) {
			latinName = "CostaRica";
		} else if (country.contains("Κούβα")) {
			latinName = "Cuba";
		} else if (country.contains("Πράσινο Ακρωτήρι")) {
			latinName = "Cape Verde";
		} else if (country.contains("Χριστουγέννων")) {
			latinName = "Christmas Islands";
		} else if (country.contains("Δημοκρατία της Τσεχίας") || country.contains("Τσεχία")) {
			latinName = "Czech Republic";
		} else if (country.contains("Τζιμπουτί")) {
			latinName = "Djibouti";
		} else if (country.contains("Ντομίνικα")) {
			latinName = "Dominica";
		} else if (country.contains("Δομινικανή Δημοκρατία")) {
			latinName = "Dominican Republic";
		} else if (country.contains("Αλγερία")) {
			latinName = "Algeria";
		} else if (country.contains("Ισημερινός")) {
			latinName = "Equator";
		} else if (country.contains("Εσθονία")) {
			latinName = "Estonia";
		} else if (country.contains("Αίγυπτος")) {
			latinName = "Egypt";
		} else if (country.contains("Αιθιοποία")) {
			latinName = "Ethiopia";
		} else if (country.contains("Φίτζι")) {
			latinName = "Fiji";
		} else if (country.contains("Νήσοι Φώκλαντ")) {
			latinName = "Falkland Islands";
		} else if (country.contains("Μικρονησία")) {
			latinName = "Micronesia";
		} else if (country.contains("Νήσοι Φερόε")) {
			latinName = "FaroeIslands";
		} else if (country.contains("Γκαμπόν")) {
			latinName = "Gabon";
		} else if (country.contains("Γρενάδα")) {
			latinName = "Grenada";
		} else if (country.contains("Γιβραλτάρ")) {
			latinName = "Gibraltar";
		} else if (country.contains("Γροιλανδία")) {
			latinName = "Greenland";
		} else if (country.contains("Γκάμπια")) {
			latinName = "Gambia";
		} else if (country.contains("Γουινέα")) {
			latinName = "Guinea";
		} else if (country.contains("Ισημερινή Γουινέα")) {
			latinName = "Equatorial Guinea";
		} else if (country.contains("Νότια Γεωργία και νήσοι Νότιες Σάντουιτς")) {
			latinName = "South Georgia and South Sandwich Islands";
		} else if (country.contains("Γουατεμάλα")) {
			latinName = "Guatemala";
		} else if (country.contains("Γκουάμ")) {
			latinName = "Guam";
		} else if (country.contains("Γουινέα Μπισσάου")) {
			latinName = "Guinea Bissau";
		} else if (country.contains("Γουιάνα")) {
			latinName = "Guyana";
		} else if (country.contains("Χονγκ Κονγκ")) {
			latinName = "Hong Kong";
		} else if (country.contains("Χέρντ και Μακντόναλντ")) {
			latinName = "Heard Island and McDonald Islands";
		} else if (country.contains("Ονδούρα")) {
			latinName = "Honduras";
		} else if (country.contains("Κροατία")) {
			latinName = "Croatian";
		} else if (country.contains("Αϊτή")) {
			latinName = "Haiti";
		} else if (country.contains("Ουγγαρία")) {
			latinName = "Hungary";
		} else if (country.contains("Ιρλανδία")) {
			latinName = "Ireland";
		} else if (country.contains("Ισραήλ")) {
			latinName = "Israel";
		} else if (country.contains("Ινδία")) {
			latinName = "India";
		} else if (country.contains("Βρετανικό Έδαφος Ινδ. Ωκεανού")) {
			latinName = "British Territory Indian Ocean";
		} else if (country.contains("Ιράκ")) {
			latinName = "Iraq";
		} else if (country.contains("Ισλανδία")) {
			latinName = "Iceland";
		} else if (country.contains("Ιαμαϊκή") || country.contains("Τζαμάικα")) {
			latinName = "Jamaica";
		} else if (country.contains("Ιορδανία")) {
			latinName = "Jordan";
		} else if (country.contains("Κένυα")) {
			latinName = "Kenya";
		} else if (country.contains("Κιργιζία")) {
			latinName = "Kyrgyzstan";
		} else if (country.contains("Καμπότζη")) {
			latinName = "Cambodia";
		} else if (country.contains("Κιριμπάτι")) {
			latinName = "Kiribati";
		} else if (country.contains("Κομόρες (δεν συμπεριλ. η Μαγιότ)")) {
			latinName = "Comoros (not incl. Mayotte)";
		} else if (country.contains("Άγιος Χριστόφορος και Νέβις")) {
			latinName = "Saint Kitts and Nevis";
		} else if (country.contains("Βόρεια Κορέα")) {
			latinName = "North Korea";
		} else if (country.contains("Νότια Κορέα")) {
			latinName = "South Korea";
		} else if (country.contains("Κουβέιτ")) {
			latinName = "Kuwait";
		} else if (country.contains("Κάυμαν") || country.contains("Κέιμαν")) {
			latinName = "Cayman Islands";
		} else if (country.contains("Καζακστάν")) {
			latinName = "Kazakhstan";
		} else if (country.contains("Λάος")) {
			latinName = "Laos";
		} else if (country.contains("Λίβανος")) {
			latinName = "Lebanon";
		} else if (country.contains("Αγία Λουκία")) {
			latinName = "SaintLucia";
		} else if (country.contains("Λιχτενστάϊν")) {
			latinName = "Liechtenstein";
		} else if (country.contains("Σρι Λάνκα")) {
			latinName = "Sri Lanka";
		} else if (country.contains("Λιβερία")) {
			latinName = "Liberia";
		} else if (country.contains("Λεσόθο")) {
			latinName = "Lesotho";
		} else if (country.contains("Λιθουανία")) {
			latinName = "Lithuania";
		} else if (country.contains("Λουξεμβούργο")) {
			latinName = "Luxembourg";
		} else if (country.contains("Λετονία")) {
			latinName = "Latvia";
		} else if (country.contains("Λιβύη")) {
			latinName = "Libya";
		} else if (country.contains("Μαρόκo")) {
			latinName = "Maroko";
		} else if (country.contains("Μολδαβία")) {
			latinName = "Moldova";
		} else if (country.contains("Μαυροβούνιο")) {
			latinName = "Montenegro";
		} else if (country.contains("Μαδαγασκάρη")) {
			latinName = "Madagascar";
		} else if (country.contains("Νήσοι Μάρσαλ")) {
			latinName = "Marshall Islands";
		} else if (country.contains("Πρώην Γιουγκοσλαβική Δημοκρατία της Μακεδονίας") || country.contains("FYROM") || country.contains("Σκόπια")) {
			latinName = "FYROM";
		} else if (country.contains("Μάλι")) {
			latinName = "Mali";
		} else if (country.contains("Μιανμάρ")) {
			latinName = "Myanmar";
		} else if (country.contains("Μογγολία")) {
			latinName = "Mongolia";
		} else if (country.contains("Μακάο")) {
			latinName = "Macau";
		} else if (country.contains("Βόρειες Μαριάνες")) {
			latinName = "Northern Mariana Islands";
		} else if (country.contains("Μαυριτανία")) {
			latinName = "Mauritania";
		} else if (country.contains("Μοντσεράτ")) {
			latinName = "Montserrat";
		} else if (country.contains("Μάλτα")) {
			latinName = "Malta";
		} else if (country.contains("Μαυρίκιος")) {
			latinName = "Maurice";
		} else if (country.contains("Μαλδίβες")) {
			latinName = "Maldives";
		} else if (country.contains("Μαλάουι")) {
			latinName = "Malawi";
		} else if (country.contains("Μεξικό")) {
			latinName = "Mexico";
		} else if (country.contains("Μαλαισία")) {
			latinName = "Malaysia";
		} else if (country.contains("Μοζαμβίκη")) {
			latinName = "Mozambique";
		} else if (country.contains("Ναμίμπια")) {
			latinName = "Namibia";
		} else if (country.contains("Νέα Καληδονία")) {
			latinName = "New Caledonia and dependencies";
		} else if (country.contains("Νίγηρ")) {
			latinName = "Niger";
		} else if (country.contains("Νόρφολκ")) {
			latinName = "Norfolk (Island)";
		} else if (country.contains("Νιγηρία")) {
			latinName = "Nigeria";
		} else if (country.contains("Νικαράγουα")) {
			latinName = "Nicaragua";
		} else if (country.contains("Κάτω Χώρες") || country.contains("Ολλανδία")) {
			latinName = "Netherlands";
		} else if (country.contains("Νορβηγία")) {
			latinName = "Norway";
		} else if (country.contains("Νεπάλ")) {
			latinName = "Nepal";
		} else if (country.contains("Ναούρου")) {
			latinName = "Nauru";
		} else if (country.contains("Νίουε")) {
			latinName = "Niue";
		} else if (country.contains("Νέα Ζηλανδία")) {
			latinName = "New Zealand";
		} else if (country.contains("Ομάν")) {
			latinName = "Oman";
		} else if (country.contains("Παναμάς")) {
			latinName = "Panama";
		} else if (country.contains("Περού")) {
			latinName = "Peru";
		} else if (country.contains("Γαλλική Πολυνησία")) {
			latinName = "French Polynesia";
		} else if (country.contains("Πάπουα Νέα Γουινέα")) {
			latinName = "Papua New Guinea";
		} else if (country.contains("Φιλιππίνες")) {
			latinName = "Philippines";
		} else if (country.contains("Πακιστάν")) {
			latinName = "Pakistan";
		} else if (country.contains("Πολωνία")) {
			latinName = "Poland";
		} else if (country.contains("Άγιος Πέτρος και Μικελόν")) {
			latinName = "Saint Pierre and Miquelon";
		} else if (country.contains("Πιτκαίρν")) {
			latinName = "Pitcairn";
		} else if (country.contains("Κατεχόμενο παλαιστινιακό έδαφος")) {
			latinName = "Occupied Palestinian Territory";
		} else if (country.contains("Πορτογαλία")) {
			latinName = "Portugal";
		} else if (country.contains("Παλάου")) {
			latinName = "Palau";
		} else if (country.contains("Παραγουάη")) {
			latinName = "Paraguay";
		} else if (country.contains("Κατάρ")) {
			latinName = "Qatar";
		} else if (country.contains("Εφοδιασμός σκαφών")) {
			latinName = "Supply of ships";
		} else if (country.contains("Εφοδιασμός σκαφών στο πλαίσιο των ενδοκοινοτικών συναλλαγών")) {
			latinName = "Supply of shipsin intra-Community trade";
		} else if (country.contains("Εφοδιασμός σκαφών στο πλαίσιο των συναλλαγών με τρίτες χώρες")) {
			latinName = "Supply of ships within the framework of trade with third countries";
		} else if (country.contains("Μη καθοριζόμενες χώρες και εδάφη")) {
			latinName = "Not specified countries and territories";
		} else if (country.contains("Χώρες και εδάφη που δεν καθορίζονται στο πλαίσιο των ενδοκοινοτικών συναλλαγών")) {
			latinName = "Countries and territories not specified within the framework of intra-Community trade";
		} else if (country.contains("Χώρες και εδάφη που δεν καθορίζονται στο πλαίσιο των συναλλαγών με τρίτες χώρες")) {
			latinName = "Countries and territories not specified within the framework of trade with third countries";
		} else if (country.contains("Χώρες και εδάφη μη κατονομαζόμενα για εμπορικούς ή στρατιωτικούς λόγους")) {
			latinName = "Countries and territories not specified for commercial or military reasons";
		} else if (country.contains("QX στο πλαίσιο των ενδοκοινοτικών συναλλαγών")) {
			latinName = "QX in intra-Community trade";
		} else if (country.contains("QX στο πλαίσιο των συναλλαγών με τρίτες χώρες")) {
			latinName = "QX in trade with third countries";
		} else if (country.contains("Ρουμανία")) {
			latinName = "Romania";
		} else if (country.contains("Ρωσία")) {
			latinName = "Russia";
		} else if (country.contains("Ρουάντα")) {
			latinName = "Rwanda";
		} else if (country.contains("Σαουδική Αραβία")) {
			latinName = "Saudi Arabia";
		} else if (country.contains("Νήσοι Σολομώντος")) {
			latinName = "Solomon Islands";
		} else if (country.contains("Σεϋχέλλες")) {
			latinName = "Seychelles and dependencies";
		} else if (country.contains("Σουδάν")) {
			latinName = "Sudan";
		} else if (country.contains("Σουηδία")) {
			latinName = "Sweden";
		} else if (country.contains("Σιγκαπούρη")) {
			latinName = "Singapore";
		} else if (country.contains("Αγία Ελένη")) {
			latinName = "Saint Helena and dependencies";
		} else if (country.contains("Σλοβενία")) {
			latinName = "Slovenia";
		} else if (country.contains("Σλοβακία")) {
			latinName = "Slovak Republic";
		} else if (country.contains("Σιέρρα Λεόνε")) {
			latinName = "SierraLeone";
		} else if (country.contains("Άγιος Μαρίνος")) {
			latinName = "San Marino";
		} else if (country.contains("Σενεγάλη")) {
			latinName = "Senegal";
		} else if (country.contains("Σομαλία")) {
			latinName = "Somalia";
		} else if (country.contains("Σουρινάμ")) {
			latinName = "Suriname";
		} else if (country.contains("Σάο Τομέ και Πρίνσιπε")) {
			latinName = "Sao Tome and Principe";
		} else if (country.contains("Ελ Σαλβαδόρ")) {
			latinName = "ElSalvador";
		} else if (country.contains("Συρία")) {
			latinName = "Syria";
		} else if (country.contains("Σουαζιλάνδη")) {
			latinName = "Swaziland";
		} else if (country.contains("Νήσοι Τερκς και Κάικος")) {
			latinName = "Turks and Caicos";
		} else if (country.contains("Τσαντ")) {
			latinName = "Chad";
		} else if (country.contains("Γαλλικά νότια εδάφη")) {
			latinName = "French Southern Territories";
		} else if (country.contains("Τόγκο")) {
			latinName = "Togo";
		} else if (country.contains("Ταϊλάνδη")) {
			latinName = "Thailand";
		} else if (country.contains("Τατζικιστάν")) {
			latinName = "Tajikistan";
		} else if (country.contains("Τουκελάου")) {
			latinName = "Toukelaou";
		} else if (country.contains("Ανατολικό Τιμόρ")) {
			latinName = "East Timor";
		} else if (country.contains("Τουρκμενιστάν")) {
			latinName = "Turkmenistan";
		} else if (country.contains("Τυνησία")) {
			latinName = "Tunisia";
		} else if (country.contains("Τόνγκα")) {
			latinName = "Tonga";
		} else if (country.contains("Τρινιδάδ και Τομπάγκο") || country.contains("Τρινιντάντ και Τομπάγκο")) {
			latinName = "Trinidad and Tobago";
		} else if (country.contains("Τουβαλού")) {
			latinName = "Tuvalu";
		} else if (country.contains("Ταϊβάν")) {
			latinName = "Taiwan";
		} else if (country.contains("Τανζανία")) {
			latinName = "Tanzania";
		} else if (country.contains("Ουκρανία")) {
			latinName = "Ukraine";
		} else if (country.contains("Ουγκάντα")) {
			latinName = "Uganda";
		} else if (country.contains("Μικρές νήσοι απομεμακρυσμένες από τις Ηνωμένες Πολιτείες")) {
			latinName = "Minor Outlying Islands of United States";
		} else if (country.contains("Ουραγουάη")) {
			latinName = "Uruguay";
		} else if (country.contains("Ουζμπεκιστάν")) {
			latinName = "Uzbekistan";
		} else if (country.contains("Πόλη του Βατικανού")) {
			latinName = "Vatican City";
		} else if (country.contains("Άγιος Βικέντιος και Γρεναδίνες")) {
			latinName = "Saint Vincent and Grenadines";
		} else if (country.contains("Βενεζουέλα")) {
			latinName = "Venezuela";
		} else if (country.contains("Βρετανικοί Παρθένοι Νήσοι")) {
			latinName = "Virgin Islands UK";
		} else if (country.contains("Παρθένοι Νήσοι των Ηνωμ. Πολιτειών")) {
			latinName = "Virgin Islands USA";
		} else if (country.contains("Βιετνάμ")) {
			latinName = "Vietnam";
		} else if (country.contains("Βανουάτου")) {
			latinName = "Vanuatu";
		} else if (country.contains("Νήσοι Ουώλις και Φουτούνα")) {
			latinName = "Wallis and Futuna";
		} else if (country.contains("Σαμόα")) {
			latinName = "Samoa";
		} else if (country.contains("Θέουτα")) {
			latinName = "Ceuta";
		} else if (country.contains("Κόσοβο")) {
			latinName = "Kosovo";
		} else if (country.contains("Μελίλια")) {
			latinName = "Melilla";
		} else if (country.contains("Σερβία")) {
			latinName = "Serbia";
		} else if (country.contains("Υεμένη")) {
			latinName = "Yemen";
		} else if (country.contains("Μαγιότ")) {
			latinName = "Mayotte";
		} else if (country.contains("Νότια Αφρική")) {
			latinName = "South Africa";
		} else if (country.contains("Ζάμπια")) {
			latinName = "Zambia";
		} else if (country.contains("Ζιμπάμπουε")) {
			latinName = "Zimbabwe";
		} else {
			writeUnknownCountry(country);
		}
		
		return latinName;
	}*/
	
	/**
     * Find the the official Greek and English name of the country.
     * 
     * @param String the abbreviation of the country
     * @return String[] the official Greek and English name oh the country
     */
	public String[] findCountryFromAbbreviation(String abbreviation) {
		
		String[] country = null;
		
		if (abbreviation.equalsIgnoreCase("GR") || abbreviation.equalsIgnoreCase("EL")) {
			country = new String[]{"Ελλάδα", "Greece"};
		} else if (abbreviation.equalsIgnoreCase("DE")) {
			country = new String[]{"Γερμανία", "Germany"};
		} else if (abbreviation.equalsIgnoreCase("AT") || abbreviation.equalsIgnoreCase("AUT")) {
			country = new String[]{"Αυστρία", "Austria"};
		} else if (abbreviation.equalsIgnoreCase("USA") || abbreviation.equalsIgnoreCase("US")) {
			country = new String[]{"Ηνωμένες Πολιτείες της Αμερικής", "United States of America (USA)"};
		} else if (abbreviation.equalsIgnoreCase("FR")) {
			country = new String[]{"Γαλλία", "France"};
		} else if (abbreviation.equalsIgnoreCase("IT")) {
			country = new String[]{"Ιταλία", "Italy"};
		} else if (abbreviation.equalsIgnoreCase("CA")) {
			country = new String[]{"Καναδάς", "Canada"};
		} else if (abbreviation.equalsIgnoreCase("CN")) {
			country = new String[]{"Κίνα", "China"};
		} else if (abbreviation.equalsIgnoreCase("IR")) {
			country = new String[]{"Ισλαμική Δημοκρατία του Ιράν", " Islamic Republic of Iran"};
		} else if (abbreviation.equalsIgnoreCase("GB")) {
			country = new String[]{"Ηνωμένο Βασίλειο", "Great Britain"};
		} else if (abbreviation.equalsIgnoreCase("TR")) {
			country = new String[]{"Τουρκία", "Turkey"};
		} else if (abbreviation.equalsIgnoreCase("JP")) {
			country = new String[]{"Ιαπωνία", "Japan"};
		} else if (abbreviation.equalsIgnoreCase("ER")) {
			country = new String[]{"Ερυθραία", "Eritrea"};
		} else if (abbreviation.equalsIgnoreCase("CH")) {
			country = new String[]{"Ελβετία", "Switzerland"};
		} else if (abbreviation.equalsIgnoreCase("GH")) {
			country = new String[]{"Γκάνα", "Ghana"};
		} else if (abbreviation.equalsIgnoreCase("BZ")) {
			country = new String[]{"Μπελίζ", "Belize"};
		} else if (abbreviation.equalsIgnoreCase("AE")) {
			country = new String[]{"Ενωμένα Αραβικά Εμιράτα", "United Arab Emirates (UAE)"};
		} else if (abbreviation.equalsIgnoreCase("AN")) {
			country = new String[]{"Ολλανδικές Αντίλλες", "Netherlands Antilles"};
		} else if (abbreviation.equalsIgnoreCase("CY")) {
			country = new String[]{"Κύπρος", "Cyprus"};
		} else if (abbreviation.equalsIgnoreCase("ES")) {
			country = new String[]{"Ισπανία (δεν συμπεριλ. XC, XL)", "Spain (not incl XC, XL)"};
		} else if (abbreviation.equalsIgnoreCase("GE")) {
			country = new String[]{"Γεωργία", "Georgia"};
		} else if (abbreviation.equalsIgnoreCase("BE")) {
			country = new String[]{"Βελγίο", "Belgium"};
		} else if (abbreviation.equalsIgnoreCase("AD")) {
			country = new String[]{"Ανδόρρα", "Andorra"};
		} else if (abbreviation.equalsIgnoreCase("ID")) {
			country = new String[]{"Ινδονησία", "Indonesia"};
		} else if (abbreviation.equalsIgnoreCase("FI")) {
			country = new String[]{"Φινλανδία", "Finland"};
		} else if (abbreviation.equalsIgnoreCase("DK")) {
			country = new String[]{"Δανία", "Denmark"};
		} else if (abbreviation.equalsIgnoreCase("EU")) {
			country = new String[]{"Ευρωπαϊκή Κοινότητα", "European Community"};
		} else if (abbreviation.equalsIgnoreCase("AF")) {
			country = new String[]{"Αφγανιστάν", "Afghanistan"};
		} else if (abbreviation.equalsIgnoreCase("AG")) {
			country = new String[]{"Αντίγκουα και Μπαρμπούντα", "Antigua and Barbuda"};
		} else if (abbreviation.equalsIgnoreCase("AI")) {
			country = new String[]{"Ανγκουίλα", "Anguilla"};
		} else if (abbreviation.equalsIgnoreCase("AL")) {
			country = new String[]{"Αλβανία", "Albania"};
		} else if (abbreviation.equalsIgnoreCase("AM")) {
			country = new String[]{"Αρμενία", "Armenia"};
		} else if (abbreviation.equalsIgnoreCase("AO")) {
			country = new String[]{"Αγκόλα", "Angola"};
		} else if (abbreviation.equalsIgnoreCase("AQ")) {
			country = new String[]{"Ανταρκτική", "Antarctic"};
		} else if (abbreviation.equalsIgnoreCase("AR")) {
			country = new String[]{"Αργεντινή", "Argentina"};
		} else if (abbreviation.equalsIgnoreCase("AS")) {
			country = new String[]{"Αμερικανικές Σαμόα", "American Samoa"};
		} else if (abbreviation.equalsIgnoreCase("AU")) {
			country = new String[]{"Αυστραλία", "Australia"};
		} else if (abbreviation.equalsIgnoreCase("AW")) {
			country = new String[]{"Αρούμπα", "Aruba"};
		} else if (abbreviation.equalsIgnoreCase("AZ")) {
			country = new String[]{"Αζερμπαϊτζάν", "Azerbaijan"};
		} else if (abbreviation.equalsIgnoreCase("BA")) {
			country = new String[]{"Βοσνία-Ερζεγοβίνη", "Bosnia-Herzegovina"};
		} else if (abbreviation.equalsIgnoreCase("BB")) {
			country = new String[]{"Μπαρμπάντος", "Barbados"};
		} else if (abbreviation.equalsIgnoreCase("BD")) {
			country = new String[]{"Μπαγκλαντές", "Bangladesh"};
		} else if (abbreviation.equalsIgnoreCase("BF")) {
			country = new String[]{"Μπουρκίνα Φάσο (πρώην Ανω Βόλτα)", "Burkina Faso"};
		} else if (abbreviation.equalsIgnoreCase("BG")) {
			country = new String[]{"Βουλγαρία", "Bulgaria"};
		} else if (abbreviation.equalsIgnoreCase("BH")) {
			country = new String[]{"Μπαχρέιν", "Bahrain"};
		} else if (abbreviation.equalsIgnoreCase("BI")) {
			country = new String[]{"Μπουρούντι", "Burundi"};
		} else if (abbreviation.equalsIgnoreCase("BJ")) {
			country = new String[]{"Μπενίν", "Benin"};
		} else if (abbreviation.equalsIgnoreCase("BM")) {
			country = new String[]{"Βερμούδες", "Bermuda"};
		} else if (abbreviation.equalsIgnoreCase("BN")) {
			country = new String[]{"Μπρουνέι", "Brunei Darussalam"};
		} else if (abbreviation.equalsIgnoreCase("BO")) {
			country = new String[]{"Πολυεθνικό Κράτος της Βολιβίας", "Plurinational State of Bolivia"};
		} else if (abbreviation.equalsIgnoreCase("BR")) {
			country = new String[]{"Βραζιλία", "Brazil"};
		} else if (abbreviation.equalsIgnoreCase("BS")) {
			country = new String[]{"Μπαχάμες", "Bahamas"};
		} else if (abbreviation.equalsIgnoreCase("BT")) {
			country = new String[]{"Μπουτάν", "Bhutan"};
		} else if (abbreviation.equalsIgnoreCase("BV")) {
			country = new String[]{"Μπουβέ (Νήσος)", "Bouvet"};
		} else if (abbreviation.equalsIgnoreCase("BW")) {
			country = new String[]{"Μποτσουάνα", "Botswana"};
		} else if (abbreviation.equalsIgnoreCase("BY")) {
			country = new String[]{"Λευκορωσία", "Belarus"};
		} else if (abbreviation.equalsIgnoreCase("CC")) {
			country = new String[]{"Κόκος (Νήσοι) (ή Νήσοι Κήλινγκ)", "Cocos (Keeling) Islands"};
		} else if (abbreviation.equalsIgnoreCase("CD")) {
			country = new String[]{"Δημοκρατία του Κονγκό", "The Democratic Republic of Congo"};
		} else if (abbreviation.equalsIgnoreCase("CF")) {
			country = new String[]{"Δημοκρατία Κεντρικής Αφρικής", "Central African Republic"};
		} else if (abbreviation.equalsIgnoreCase("CG")) {
			country = new String[]{"Κονγκό", "Congo"};
		} else if (abbreviation.equalsIgnoreCase("CI")) {
			country = new String[]{"Ακτή Ελεφαντοστού", "IvoryCoast"};
		} else if (abbreviation.equalsIgnoreCase("CK")) {
			country = new String[]{"Κουκ (Νήσοι)", "Cook"};
		} else if (abbreviation.equalsIgnoreCase("CL")) {
			country = new String[]{"Χιλή", "Chile"};
		} else if (abbreviation.equalsIgnoreCase("CM")) {
			country = new String[]{"Καμερούν", "Cameroon"};
		} else if (abbreviation.equalsIgnoreCase("CO")) {
			country = new String[]{"Κολομβία", "Colombia"};
		} else if (abbreviation.equalsIgnoreCase("CR")) {
			country = new String[]{"Κόστα Ρίκα", "Costa Rica"};
		} else if (abbreviation.equalsIgnoreCase("CU")) {
			country = new String[]{"Κούβα", "Cuba"};
		} else if (abbreviation.equalsIgnoreCase("CV")) {
			country = new String[]{"Πράσινο Ακρωτήριο", "CapeVerde"};
		} else if (abbreviation.equalsIgnoreCase("CX")) {
			country = new String[]{"Νήσος Χριστουγέννων", "Christmas Island"};
		} else if (abbreviation.equalsIgnoreCase("CZ")) {
			country = new String[]{"Δημοκρατία της Τσεχίας", "Czech Republic"};
		} else if (abbreviation.equalsIgnoreCase("DJ")) {
			country = new String[]{"Τζιμπουτί", "Djibouti"};
		} else if (abbreviation.equalsIgnoreCase("DM")) {
			country = new String[]{"Ντομίνικα", "Dominica"};
		} else if (abbreviation.equalsIgnoreCase("DO")) {
			country = new String[]{"Δομινικανή Δημοκρατία", "DominicanRepublic"};
		} else if (abbreviation.equalsIgnoreCase("DZ")) {
			country = new String[]{"Αλγερία", "Algeria"};
		} else if (abbreviation.equalsIgnoreCase("EC")) {
			country = new String[]{"Ισημερινός", "Equator"};
		} else if (abbreviation.equalsIgnoreCase("EE")) {
			country = new String[]{"Εσθονία", "Estonia"};
		} else if (abbreviation.equalsIgnoreCase("EG")) {
			country = new String[]{"Αίγυπτος", "Egypt"};
		} else if (abbreviation.equalsIgnoreCase("ET")) {
			country = new String[]{"Αιθιοποία", "Ethiopia"};
		} else if (abbreviation.equalsIgnoreCase("FJ")) {
			country = new String[]{"Φίτζι", "Fiji"};
		} else if (abbreviation.equalsIgnoreCase("FK")) {
			country = new String[]{"Νήσοι Φώκλαντ", "Falkland Islands (Malvinas)"};
		} else if (abbreviation.equalsIgnoreCase("FM")) {
			country = new String[]{"Μικρονησία (ομόσπονδα κράτη)", "Micronesia (Federated States of)"};
		} else if (abbreviation.equalsIgnoreCase("FO")) {
			country = new String[]{"Νήσοι Φερόε", "Faroe Islands"};
		} else if (abbreviation.equalsIgnoreCase("GA")) {
			country = new String[]{"Γκαμπόν", "Gabon"};
		} else if (abbreviation.equalsIgnoreCase("GD")) {
			country = new String[]{"Γρενάδα", "Grenada"};
		} else if (abbreviation.equalsIgnoreCase("GI")) {
			country = new String[]{"Γιβραλτάρ", "Gibraltar"};
		} else if (abbreviation.equalsIgnoreCase("GL")) {
			country = new String[]{"Γροιλανδία", "Greenland"};
		} else if (abbreviation.equalsIgnoreCase("GM")) {
			country = new String[]{"Γκάμπια", "Gambia"};
		} else if (abbreviation.equalsIgnoreCase("GN")) {
			country = new String[]{"Γουινέα", "Guinea"};
		} else if (abbreviation.equalsIgnoreCase("GQ")) {
			country = new String[]{"Ισημερινή Γουινέα ", "Equatorial Guinea"};
		} else if (abbreviation.equalsIgnoreCase("GS")) {
			country = new String[]{"Νότια Γεωργία και νήσοι Νότιες Σάντουιτς", "South Georgia and South Sandwich Islands"};
		} else if (abbreviation.equalsIgnoreCase("GT")) {
			country = new String[]{"Γουατεμάλα", "Guatemala"};
		} else if (abbreviation.equalsIgnoreCase("GU")) {
			country = new String[]{"Γκουάμ", "Guam"};
		} else if (abbreviation.equalsIgnoreCase("GW")) {
			country = new String[]{"Γουινέα Μπισσάου", "Guinea Bissau"};
		} else if (abbreviation.equalsIgnoreCase("GY")) {
			country = new String[]{"Γουιάνα", "Guyana"};
		} else if (abbreviation.equalsIgnoreCase("HK")) {
			country = new String[]{"Χονγκ Κονγκ", "HongKong"};
		} else if (abbreviation.equalsIgnoreCase("HM")) {
			country = new String[]{"Νήσος Χέρντ και Νήσοι Μακντόναλντ", "Heard Island αnd McDonald Islands"};
		} else if (abbreviation.equalsIgnoreCase("HN")) {
			country = new String[]{"Ονδούρα", "Honduras"};
		} else if (abbreviation.equalsIgnoreCase("HR")) {
			country = new String[]{"Κροατία", "Croatia"};
		} else if (abbreviation.equalsIgnoreCase("HT")) {
			country = new String[]{"Αϊτή", "Haiti"};
		} else if (abbreviation.equalsIgnoreCase("HU")) {
			country = new String[]{"Ουγγαρία", "Hungary"};
		} else if (abbreviation.equalsIgnoreCase("IE")) {
			country = new String[]{"Ιρλανδία", "Ireland"};
		} else if (abbreviation.equalsIgnoreCase("IL")) {
			country = new String[]{"Ισραήλ", "Israel"};
		} else if (abbreviation.equalsIgnoreCase("IN")) {
			country = new String[]{"Ινδία", "India"};
		} else if (abbreviation.equalsIgnoreCase("IO")) {
			country = new String[]{"Βρετανικό Έδαφος Ινδικού Ωκεανού", "British Indian Ocean Territory"};
		} else if (abbreviation.equalsIgnoreCase("IQ")) {
			country = new String[]{"Ιράκ", "Iraq"};
		} else if (abbreviation.equalsIgnoreCase("IS")) {
			country = new String[]{"Ισλανδία", "Iceland"};
		} else if (abbreviation.equalsIgnoreCase("JM")) {
			country = new String[]{"Ιαμαϊκή", "Jamaica"};
		} else if (abbreviation.equalsIgnoreCase("JO")) {
			country = new String[]{"Ιορδανία", "Jordan"};
		} else if (abbreviation.equalsIgnoreCase("KE")) {
			country = new String[]{"Κένυα", "Kenya"};
		} else if (abbreviation.equalsIgnoreCase("KG")) {
			country = new String[]{"Κιργιζία", "Kyrgyzstan"};
		} else if (abbreviation.equalsIgnoreCase("KH")) {
			country = new String[]{"Καμπότζη", "Cambodia"};
		} else if (abbreviation.equalsIgnoreCase("KI")) {
			country = new String[]{"Κιριμπάτι", "Kiribati"};
		} else if (abbreviation.equalsIgnoreCase("KM")) {
			country = new String[]{"Κομόρες (δεν συμπεριλ. η Μαγιότ)", "Comoros (not incl. Mayotte)"};
		} else if (abbreviation.equalsIgnoreCase("KN")) {
			country = new String[]{"Άγιος Χριστόφορος και Νέβις", "Saint Kitts and Nevis"};
		} else if (abbreviation.equalsIgnoreCase("KP")) {
			country = new String[]{"Λαϊκή Δημοκρατία της Κορέας (Βόρεια Κορέα)", "Democratic People's Republic of Korea (North Korea)"};
		} else if (abbreviation.equalsIgnoreCase("KR")) {
			country = new String[]{"Δημοκρατία της Κορέας (Νότια Κορέα)", "Republic of Korea (South Korea)"};
		} else if (abbreviation.equalsIgnoreCase("KW")) {
			country = new String[]{"Κουβέιτ", "Kuwait"};
		} else if (abbreviation.equalsIgnoreCase("KY")) {
			country = new String[]{"Νήσοι Κάυμαν", "Cayman Islands"};
		} else if (abbreviation.equalsIgnoreCase("KZ")) {
			country = new String[]{"Καζακστάν", "Kazakhstan"};
		} else if (abbreviation.equalsIgnoreCase("LA")) {
			country = new String[]{"Δημοκρατική Λαϊκή Δημοκρατία του Λάος", "Lao People's Democratic Republic"};
		} else if (abbreviation.equalsIgnoreCase("LB")) {
			country = new String[]{"Λίβανος", "Lebanon"};
		} else if (abbreviation.equalsIgnoreCase("LC")) {
			country = new String[]{"Αγία Λουκία", "Saint Lucia"};
		} else if (abbreviation.equalsIgnoreCase("LI")) {
			country = new String[]{"Λιχτενστάϊν", "Liechtenstein"};
		} else if (abbreviation.equalsIgnoreCase("LK")) {
			country = new String[]{"Σρι Λάνκα", "Sri Lanka"};
		} else if (abbreviation.equalsIgnoreCase("LR")) {
			country = new String[]{"Λιβερία", "Liberia"};
		} else if (abbreviation.equalsIgnoreCase("LS")) {
			country = new String[]{"Λεσόθο", "Lesotho"};
		} else if (abbreviation.equalsIgnoreCase("LT")) {
			country = new String[]{"Λιθουανία", "Lithuania"};
		} else if (abbreviation.equalsIgnoreCase("LU")) {
			country = new String[]{"Λουξεμβούργο", "Luxembourg"};
		} else if (abbreviation.equalsIgnoreCase("LV")) {
			country = new String[]{"Λετονία", "Latvia"};
		} else if (abbreviation.equalsIgnoreCase("LY")) {
			country = new String[]{"Λιβύη", "Libya"};
		} else if (abbreviation.equalsIgnoreCase("MA")) {
			country = new String[]{"Μαρόκo", "Maroko"};
		} else if (abbreviation.equalsIgnoreCase("LD")) {
			country = new String[]{"Μολδαβία", "Moldova"};
		} else if (abbreviation.equalsIgnoreCase("ME")) {
			country = new String[]{"Μαυροβούνιο", "Montenegro"};
		} else if (abbreviation.equalsIgnoreCase("MG")) {
			country = new String[]{"Μαδαγασκάρη", "Madagascar"};
		} else if (abbreviation.equalsIgnoreCase("MH")) {
			country = new String[]{"Νήσοι Μάρσαλ", "Marshall Islands"};
		} else if (abbreviation.equalsIgnoreCase("MK")) {
			country = new String[]{"Πρώην Γιουγκοσλαβική Δημοκρατία της Μακεδονίας (FYROM)", "FYROM"};
		} else if (abbreviation.equalsIgnoreCase("ML")) {
			country = new String[]{"Μάλι", "Mali"};
		} else if (abbreviation.equalsIgnoreCase("MM")) {
			country = new String[]{"Μιανμάρ", "Myanmar"};
		} else if (abbreviation.equalsIgnoreCase("MN")) {
			country = new String[]{"Μογγολία", "Mongolia"};
		} else if (abbreviation.equalsIgnoreCase("MO")) {
			country = new String[]{"Μακάο", "Macau"};
		} else if (abbreviation.equalsIgnoreCase("MP")) {
			country = new String[]{"Νήσοι Βόρειες Μαριάνες", "Northern Mariana Islands"};
		} else if (abbreviation.equalsIgnoreCase("MR")) {
			country = new String[]{"Μαυριτανία", "Mauritania"};
		} else if (abbreviation.equalsIgnoreCase("MS")) {
			country = new String[]{"Μοντσεράτ", "Montserrat"};
		} else if (abbreviation.equalsIgnoreCase("MT")) {
			country = new String[]{"Μάλτα", "Malta"};
		} else if (abbreviation.equalsIgnoreCase("MU")) {
			country = new String[]{"Μαυρίκιος", "Maurice"};
		} else if (abbreviation.equalsIgnoreCase("MV")) {
			country = new String[]{"Μαλδίβες", "Maldives"};
		} else if (abbreviation.equalsIgnoreCase("MW")) {
			country = new String[]{"Μαλάουι", "Malawi"};
		} else if (abbreviation.equalsIgnoreCase("MX")) {
			country = new String[]{"Μεξικό", "Mexico"};
		} else if (abbreviation.equalsIgnoreCase("MY")) {
			country = new String[]{"Μαλαισία", "Malaysia"};
		} else if (abbreviation.equalsIgnoreCase("MZ")) {
			country = new String[]{"Μοζαμβίκη", "Mozambique"};
		} else if (abbreviation.equalsIgnoreCase("NA")) {
			country = new String[]{"Ναμίμπια", "Namibia"};
		} else if (abbreviation.equalsIgnoreCase("NC")) {
			country = new String[]{"Νέα Καληδονία και εξαρτήσεις", "New Caledonia and dependencies"};
		} else if (abbreviation.equalsIgnoreCase("NE")) {
			country = new String[]{"Νίγηρ", "Niger"};
		} else if (abbreviation.equalsIgnoreCase("NF")) {
			country = new String[]{"Νήσος Νόρφολκ", "Norfolk Island"};
		} else if (abbreviation.equalsIgnoreCase("NG")) {
			country = new String[]{"Νιγηρία", "Nigeria"};
		} else if (abbreviation.equalsIgnoreCase("NI")) {
			country = new String[]{"Νικαράγουα", "Nicaragua"};
		} else if (abbreviation.equalsIgnoreCase("NL")) {
			country = new String[]{"Κάτω Χώρες (Ολλανδία)", "Netherlands"};
		} else if (abbreviation.equalsIgnoreCase("NO")) {
			country = new String[]{"Νορβηγία", "Norway"};
		} else if (abbreviation.equalsIgnoreCase("NP")) {
			country = new String[]{"Νεπάλ", "Nepal"};
		} else if (abbreviation.equalsIgnoreCase("NR")) {
			country = new String[]{"Ναούρου", "Nauru"};
		} else if (abbreviation.equalsIgnoreCase("NU")) {
			country = new String[]{"Νίουε (Νήσος)", "Niue"};
		} else if (abbreviation.equalsIgnoreCase("NZ")) {
			country = new String[]{"Νέα Ζηλανδία", "NewZealand"};
		} else if (abbreviation.equalsIgnoreCase("OM")) {
			country = new String[]{"Ομάν", "Oman"};
		} else if (abbreviation.equalsIgnoreCase("PA")) {
			country = new String[]{"Παναμάς", "Panama"};
		} else if (abbreviation.equalsIgnoreCase("PE")) {
			country = new String[]{"Περού", "Peru"};
		} else if (abbreviation.equalsIgnoreCase("PF")) {
			country = new String[]{"Γαλλική Πολυνησία", "French Polynesia"};
		} else if (abbreviation.equalsIgnoreCase("PG")) {
			country = new String[]{"Πάπουα-Νέα Γουινέα", "Papua New Guinea"};
		} else if (abbreviation.equalsIgnoreCase("PH")) {
			country = new String[]{"Φιλιππίνες", "Philippines"};
		} else if (abbreviation.equalsIgnoreCase("PK")) {
			country = new String[]{"Πακιστάν", "Pakistan"};
		} else if (abbreviation.equalsIgnoreCase("PL")) {
			country = new String[]{"Πολωνία", "Poland"};
		} else if (abbreviation.equalsIgnoreCase("PM")) {
			country = new String[]{"Άγιος Πέτρος και Μικελόν", "Saint Pierre and Miquelon"};
		} else if (abbreviation.equalsIgnoreCase("PN")) {
			country = new String[]{"Πιτκαίρν", "Pitcairn"};
		} else if (abbreviation.equalsIgnoreCase("PS")) {
			country = new String[]{"Κράτος της Παλαιστίνης", "State of Palestine"};
		} else if (abbreviation.equalsIgnoreCase("PT")) {
			country = new String[]{"Πορτογαλία", "Portugal"};
		} else if (abbreviation.equalsIgnoreCase("PW")) {
			country = new String[]{"Παλάου", "Palau"};
		} else if (abbreviation.equalsIgnoreCase("PY")) {
			country = new String[]{"Παραγουάη", "Paraguay"};
		} else if (abbreviation.equalsIgnoreCase("QA")) {
			country = new String[]{"Κατάρ", "Qatar"};
		} else if (abbreviation.equalsIgnoreCase("QQ")) {
			country = new String[]{"Εφοδιασμός σκαφών", "Supply of ships"};
		} else if (abbreviation.equalsIgnoreCase("QR")) {
			country = new String[]{"Εφοδιασμός σκαφών στο πλαίσιο των ενδοκοινοτικών συναλλαγών", "Supply of shipsin intra-Community trade"};
		} else if (abbreviation.equalsIgnoreCase("QS")) {
			country = new String[]{"Εφοδιασμός σκαφών στο πλαίσιο των συναλλαγών με τρίτες χώρες", "Supply of ships within the framework of trade with third countries"};
		} else if (abbreviation.equalsIgnoreCase("QU")) {
			country = new String[]{"Μη καθοριζόμενες χώρες και εδάφη ", "Not specified countries and territories"};
		} else if (abbreviation.equalsIgnoreCase("QV")) {
			country = new String[]{"Χώρες και εδάφη που δεν καθορίζονται στο πλαίσιο των ενδοκοινοτικών συναλλαγών", "Countries and territories not specified within the framework of intra-Community trade"};
		} else if (abbreviation.equalsIgnoreCase("QW")) {
			country = new String[]{"Χώρες και εδάφη που δεν καθορίζονται στο πλαίσιο των συναλλαγών με τρίτες χώρες", "Countries and territories not specified within the framework of trade with third countries"};
		} else if (abbreviation.equalsIgnoreCase("QX")) {
			country = new String[]{"Χώρες και εδάφη μη κατονομαζόμενα για εμπορικούς ή στρατιωτικούς λόγους", "Countries and territories not specified for commercial or military reasons"};
		} else if (abbreviation.equalsIgnoreCase("QY")) {
			country = new String[]{"QX στο πλαίσιο των ενδοκοινοτικών συναλλαγών", "QX in intra-Community trade"};
		} else if (abbreviation.equalsIgnoreCase("QZ")) {
			country = new String[]{"QX στο πλαίσιο των συναλλαγών με τρίτες χώρες", "QX in trade with third countries"};
		} else if (abbreviation.equalsIgnoreCase("RO")) {
			country = new String[]{"Ρουμανία", "Romania"};
		} else if (abbreviation.equalsIgnoreCase("RU")) {
			country = new String[]{"Ρωσική Ομοσπονδία", "Russian Federation"};
		} else if (abbreviation.equalsIgnoreCase("RW")) {
			country = new String[]{"Ρουάντα", "Rwanda"};
		} else if (abbreviation.equalsIgnoreCase("SA")) {
			country = new String[]{"Σαουδική Αραβία", "SaudiArabia"};
		} else if (abbreviation.equalsIgnoreCase("SB")) {
			country = new String[]{"Νήσοι Σολομώντος", "SolomonIslands"};
		} else if (abbreviation.equalsIgnoreCase("SC")) {
			country = new String[]{"Σεϋχέλλες και εξαρτήσεις", "Seychelles and dependencies"};
		} else if (abbreviation.equalsIgnoreCase("SD")) {
			country = new String[]{"Σουδάν", "Sudan"};
		} else if (abbreviation.equalsIgnoreCase("SE")) {
			country = new String[]{"Σουηδία", "Sweden"};
		} else if (abbreviation.equalsIgnoreCase("SG")) {
			country = new String[]{"Σιγκαπούρη", "Singapore"};
		} else if (abbreviation.equalsIgnoreCase("SH")) {
			country = new String[]{"Αγία Ελένη, Αναλήψεως και Τριστάν ντα Κούνια", "Saint Helena, Ascension and Tristan da Cunha"};
		} else if (abbreviation.equalsIgnoreCase("SI")) {
			country = new String[]{"Σλοβενία", "Slovenia"};
		} else if (abbreviation.equalsIgnoreCase("SK")) {
			country = new String[]{"Δημοκρατία της Σλοβακίας", "Slovak Republic"};
		} else if (abbreviation.equalsIgnoreCase("SL")) {
			country = new String[]{"Σιέρρα Λεόνε", "Sierra Leone"};
		} else if (abbreviation.equalsIgnoreCase("SM")) {
			country = new String[]{"Άγιος Μαρίνος", "San Marino"};
		} else if (abbreviation.equalsIgnoreCase("SN")) {
			country = new String[]{"Σενεγάλη", "Senegal"};
		} else if (abbreviation.equalsIgnoreCase("SO")) {
			country = new String[]{"Σομαλία", "Somalia"};
		} else if (abbreviation.equalsIgnoreCase("SR")) {
			country = new String[]{"Σουρινάμ", "Suriname"};
		} else if (abbreviation.equalsIgnoreCase("ST")) {
			country = new String[]{"Σάο Τομέ και Πρίνσιπε", "Sao Tome and Principe"};
		} else if (abbreviation.equalsIgnoreCase("SV")) {
			country = new String[]{"Ελ Σαλβαδόρ ", "El Salvador"};
		} else if (abbreviation.equalsIgnoreCase("SY")) {
			country = new String[]{"Αραβική Δημοκρατία της Συρίας", "Syrian Arab Republic"};
		} else if (abbreviation.equalsIgnoreCase("SZ")) {
			country = new String[]{"Σουαζιλάνδη", "Swaziland"};
		} else if (abbreviation.equalsIgnoreCase("TC")) {
			country = new String[]{"Νήσοι Τερκς και Κάικος", "Turks And Caicos Islands"};
		} else if (abbreviation.equalsIgnoreCase("TD")) {
			country = new String[]{"Τσαντ", "Chad"};
		} else if (abbreviation.equalsIgnoreCase("TF")) {
			country = new String[]{"Γαλλικά νότια εδάφη", "French Southern Territories"};
		} else if (abbreviation.equalsIgnoreCase("TG")) {
			country = new String[]{"Τόγκο", "Togo"};
		} else if (abbreviation.equalsIgnoreCase("TH")) {
			country = new String[]{"Ταϊλάνδη", "Thailand"};
		} else if (abbreviation.equalsIgnoreCase("TJ")) {
			country = new String[]{"Τατζικιστάν", "Tajikistan"};
		} else if (abbreviation.equalsIgnoreCase("TK")) {
			country = new String[]{"Τουκελάου", "Toukelaou"};
		} else if (abbreviation.equalsIgnoreCase("TL")) {
			country = new String[]{"Ανατολικό Τιμόρ", "East Timor"};
		} else if (abbreviation.equalsIgnoreCase("TM")) {
			country = new String[]{"Τουρκμενιστάν", "Turkmenistan"};
		} else if (abbreviation.equalsIgnoreCase("TN")) {
			country = new String[]{"Τυνησία", "Tunisia"};
		} else if (abbreviation.equalsIgnoreCase("TO")) {
			country = new String[]{"Τόνγκα", "Tonga"};
		} else if (abbreviation.equalsIgnoreCase("TT")) {
			country = new String[]{"Τρινιδάδ και τομπάγκο", "Trinidad and Tobago"};
		} else if (abbreviation.equalsIgnoreCase("TV")) {
			country = new String[]{"Τουβαλού", "Tuvalu"};
		} else if (abbreviation.equalsIgnoreCase("TW")) {
			country = new String[]{"Ταϊβάν, Επαρχία της Κίνας", "Taiwan,  Province of China"};
		} else if (abbreviation.equalsIgnoreCase("TZ")) {
			country = new String[]{"Ενωμένη Δημοκρατία της Τανζανίας", " United Republic of Tanzania"};
		} else if (abbreviation.equalsIgnoreCase("UA")) {
			country = new String[]{"Ουκρανία", "Ukraine"};
		} else if (abbreviation.equalsIgnoreCase("UG")) {
			country = new String[]{"Ουγκάντα", "Uganda"};
		} else if (abbreviation.equalsIgnoreCase("UM")) {
			country = new String[]{"Μικρές νήσοι απομεμακρυσμένες από τις Ηνωμένες Πολιτείες", "United States Minor Outlying Islands"};
		} else if (abbreviation.equalsIgnoreCase("UY")) {
			country = new String[]{"Ουραγουάη ", "Uruguay"};
		} else if (abbreviation.equalsIgnoreCase("UZ")) {
			country = new String[]{"Ουζμπεκιστάν", "Uzbekistan"};
		} else if (abbreviation.equalsIgnoreCase("VA")) {
			country = new String[]{"Αγία Έδρα (Πόλη του Βατικανού)", "Holy See (Vatican City State)"};
		} else if (abbreviation.equalsIgnoreCase("VC")) {
			country = new String[]{"Άγιος Βικέντιος και Γρεναδίνες", "Saint Vincent αnd Grenadines"};
		} else if (abbreviation.equalsIgnoreCase("VE")) {
			country = new String[]{"Μπολιβαριανή Δημοκρατία της Βενεζουέλας", " Bolivarian Republic of Venezuela"};
		} else if (abbreviation.equalsIgnoreCase("VG")) {
			country = new String[]{"Παρθένοι Νήσοι (Βρετανίας)", "Virgin Islands (British)"};
		} else if (abbreviation.equalsIgnoreCase("VI")) {
			country = new String[]{"Παρθένοι Νήσοι (Η.Π.Α.)", "Virgin Islands (U.S.)"};
		} else if (abbreviation.equalsIgnoreCase("VN")) {
			country = new String[]{"Βιετνάμ", "Vietnam"};
		} else if (abbreviation.equalsIgnoreCase("VU")) {
			country = new String[]{"Βανουάτου", "Vanuatu"};
		} else if (abbreviation.equalsIgnoreCase("WF")) {
			country = new String[]{"Νήσοι Ουώλις και Φουτούνα", "Wallis and Futuna"};
		} else if (abbreviation.equalsIgnoreCase("WS")) {
			country = new String[]{"Σαμόα", "Samoa"};
		} else if (abbreviation.equalsIgnoreCase("XC")) {
			country = new String[]{"Θέουτα", "Ceuta"};
		} else if (abbreviation.equalsIgnoreCase("XK")) {
			country = new String[]{"Κόσοβο", "Kosovo"};
		} else if (abbreviation.equalsIgnoreCase("XL")) {
			country = new String[]{"Μελίλια", "Melilla"};
		} else if (abbreviation.equalsIgnoreCase("XS")) {
			country = new String[]{"Σερβία", "Serbia"};
		} else if (abbreviation.equalsIgnoreCase("YE")) {
			country = new String[]{"Υεμένη", "Yemen"};
		} else if (abbreviation.equalsIgnoreCase("YT")) {
			country = new String[]{"Μαγιότ", "Mayotte"};
		} else if (abbreviation.equalsIgnoreCase("ZA")) {
			country = new String[]{"Νότια Αφρική", "South Africa"};
		} else if (abbreviation.equalsIgnoreCase("ZM")) {
			country = new String[]{"Ζάμπια", "Zambia"};
		} else if (abbreviation.equalsIgnoreCase("ZW")) {
			country = new String[]{"Ζιμπάμπουε", "Zimbabwe"};
		} else if (abbreviation.equalsIgnoreCase("BQ")) {
			country = new String[]{"Μπονέρ Αγίου Ευσταθίου και Σάμπα", "Bonaire, Sint Eustatius and Saba"};
		} else if (abbreviation.equalsIgnoreCase("MD")) {
			country = new String[]{"Δημοκρατία της Μολδαβίας", "Republic of Moldova"};
		} else if (abbreviation.equalsIgnoreCase("MF")) {
			country = new String[]{"Άγιος Μαρτίνος (Γαλλικό τμήμα)", "Saint Martin (French part)"};
		} else if (abbreviation.equalsIgnoreCase("SX")) {
			country = new String[]{"Άγιος Μαρτίνος (Ολλανδικό τμήμα)", "Sint Maarten (Dutch part)"};
		} else if (abbreviation.equalsIgnoreCase("SJ")) {
			country = new String[]{"Σβάλμπαρντ και Γιαν Μαγιέν", "Svalbard and Jan Mayen"};
		} else if (abbreviation.equalsIgnoreCase("EH")) {
			country = new String[]{"Δυτική Σαχάρα", "Western Sahara"};
		} else {
			writeUnknownAbbreviation(abbreviation);
		}
		
		return country;
	}
	
	/**
     * Write in a file the unknown country read from a CSV file.
     * 
     * @param String the unknown country
     */
	public void writeUnknownCountry(String country) {
		
		try {
		    PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter("countries.txt", true)));
		    out.println(country);
		    out.close();
		} catch (IOException e) {
		    e.printStackTrace();
		}
		
	}
	
	/**
     * Write in a file the unknown abbreviation of a country read from a CSV file.
     * 
     * @param String the unknown abbreviation
     */
	public void writeUnknownAbbreviation(String abbreviation) {
		
		try {
		    PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter("abbreviations.txt", true)));
		    out.println(abbreviation);
		    out.close();
		} catch (IOException e) {
		    e.printStackTrace();
		}
		
	}
	
	/**
     * Write in a file the unknown currency read from a CSV file.
     * 
     * @param String the unknown currency
     */
	public void writeUnknownCurrency(String currency) {
		
		try {
		    PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter("currencies.txt", true)));
		    out.println(currency);
		    out.close();
		} catch (IOException e) {
		    e.printStackTrace();
		}
		
	}

}