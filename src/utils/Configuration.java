package utils;

/**
 * @author G. Razis
 */
public class Configuration {
	
	public static final String queryGraphOrganizations = "http://linkedeconomy.org/Organizations";
	public static final String queryGraphGeoData = "http://linkedeconomy.org/GeoData";
	
	public static final String connectionString = "jdbc:virtuoso://143.233.226.49:1111/autoReconnect=true/charset=UTF-8/log_enable=2";
	
	public static final String username = "dba";
	public static final String password = "d3ll0lv@69";
	
	public static final String rdfName = "Subsidies.rdf";
	public static final String filePathOutput = "C:/Users/Makis/Desktop/eprocur/"; //"/home/makis/RDF/";
	public static final String filePathCsv = "C:/Users/Makis/Desktop/eprocur/ESPA/"; //"/home/makis/ESPA/";
	public static final String municipalitiesMapping = "C:/Users/Makis/Desktop/eprocur/"; //"/home/makis/subsidies/";
}