package ontology;

import com.hp.hpl.jena.rdf.model.Property;
import com.hp.hpl.jena.rdf.model.Resource;
import com.hp.hpl.jena.rdf.model.ResourceFactory;

/**
 * @author G. Razis
 */
public class Ontology {
	
	/** prefixes **/
	public static final String instancePrefix;
	public static final String eLodPrefix;
	public static final String eLodGeoPrefix;
	public static final String foafPrefix;
	public static final String publicContractsPrefix;
	public static final String skosPrefix;
	public static final String goodRelationsPrefix;
	public static final String regOrgPrefix;
	public static final String orgPrefix;
	public static final String xsdPrefix;
	public static final String dcPrefix;
	
	/**Classes**/
	//eLod
	public static final Resource subsidyResource;
	public static final Resource budgetItemResource;
	public static final Resource spendingItemResource;
	public static final Resource expenditureLineResource;
	public static final Resource currencyResource;
	public static final Resource projectStatusResource;
	//FOAF
	public static final Resource agentResource;
	public static final Resource organizationResource;
	//SKOS
	public static final Resource conceptResource;
	//GR
	public static final Resource businessEntityResource;
	public static final Resource unitPriceSpecificationResource;
	//RegOrg
	public static final Resource registeredOrganizationResource;
	//pc
	public static final Resource contractResource;
	//org
	public static final Resource orgOrganizationResource;
	//elodGeo
	public static final Resource countryResource;
	
	/**Object Properties**/
	//eLod
	public static final Property hasCurrency; //UnitPriceSpecification hasCurrency Currency
	public static final Property beneficiary; //Subsidy/BudgetItem/Contract/SpendingItem beneficiary Organization
	public static final Property amount; //SpendingItem amount UnitPriceSpecification
	public static final Property price; //BudgetItem price UnitPriceSpecification
	public static final Property hasExpenditureLine; //SpendingItem hasExpenditureLine ExpenditureLine
	public static final Property projectStatus; //Subsidy projectStatus ProjectStatus
	public static final Property hasRelatedBudgetItem; //Subsidy hasRelatedBudgetItem BudgetItem
	public static final Property hasRelatedContract; //Subsidy hasRelatedContract Contract
	public static final Property hasRelatedSpendingItem; //Subsidy hasRelatedSpendingItem SpendingItem
	public static final Property subsidyMunicipality; //Subsidy subsidyMunicipality Municipality
	//pc
	public static final Property agreedPrice; //Contract agreedPrice UnitPriceSpecification
	
	/**Data Properties**/
	//eLod
	public static final Property countryIsoCode;
	public static final Property currencyIsoCode;
	//gr
	public static final Property name;
	public static final Property hasCurrencyValue;
	public static final Property valueAddedTaxIncluded;
	//SKOS
	public static final Property prefLabel;
	//dc
	public static final Property subject;
	
	static {
		/** prefixes **/
		instancePrefix = "http://linkedeconomy.org/resource/";
		eLodPrefix = "http://linkedeconomy.org/ontology#";
		eLodGeoPrefix = "http://linkedeconomy.org/geoOntology#";
		foafPrefix = "http://xmlns.com/foaf/0.1/";
		publicContractsPrefix = "http://purl.org/procurement/public-contracts#";
		skosPrefix = "http://www.w3.org/2004/02/skos/core#";
		goodRelationsPrefix = "http://purl.org/goodrelations/v1#";
		regOrgPrefix = "http://www.w3.org/ns/regorg#";
		orgPrefix = "http://www.w3.org/ns/org#";
		xsdPrefix = "http://www.w3.org/2001/XMLSchema#";
		dcPrefix = "http://purl.org/dc/elements/1.1/";
		
		/** Resources **/
		//eLod
		subsidyResource = ResourceFactory.createResource(eLodPrefix + "Subsidy");
		budgetItemResource = ResourceFactory.createResource(eLodPrefix + "BudgetItem");
		spendingItemResource = ResourceFactory.createResource(eLodPrefix + "SpendingItem");
		expenditureLineResource = ResourceFactory.createResource(eLodPrefix + "ExpenditureLine");
		currencyResource = ResourceFactory.createResource(eLodPrefix + "Currency");
		projectStatusResource = ResourceFactory.createResource(eLodPrefix + "ProjectStatus");
		//FOAF
		agentResource = ResourceFactory.createResource(foafPrefix + "Agent");
		organizationResource = ResourceFactory.createResource(foafPrefix + "Organization");
		//SKOS
		conceptResource = ResourceFactory.createResource(skosPrefix + "Concept");
		//GR
		businessEntityResource = ResourceFactory.createResource(goodRelationsPrefix + "BusinessEntity");
		unitPriceSpecificationResource = ResourceFactory.createResource(goodRelationsPrefix + "UnitPriceSpecification");
		//RegOrg
		registeredOrganizationResource = ResourceFactory.createResource(regOrgPrefix + "RegisteredOrganization");
		//pc
		contractResource = ResourceFactory.createResource(publicContractsPrefix + "Contract");
		//org
		orgOrganizationResource = ResourceFactory.createResource(orgPrefix + "Organization");
		//elodGeo
		countryResource = ResourceFactory.createResource(eLodGeoPrefix + "Country");
		
		/** Object Properties **/
		//eLod
		hasCurrency = ResourceFactory.createProperty(eLodPrefix + "hasCurrency");
		beneficiary = ResourceFactory.createProperty(eLodPrefix + "beneficiary");
		amount = ResourceFactory.createProperty(eLodPrefix + "amount");
		price = ResourceFactory.createProperty(eLodPrefix + "price");
		hasExpenditureLine = ResourceFactory.createProperty(eLodPrefix + "hasExpenditureLine");
		projectStatus = ResourceFactory.createProperty(eLodPrefix + "projectStatus");
		hasRelatedBudgetItem = ResourceFactory.createProperty(eLodPrefix + "hasRelatedBudgetItem");
		hasRelatedContract = ResourceFactory.createProperty(eLodPrefix + "hasRelatedContract");
		hasRelatedSpendingItem = ResourceFactory.createProperty(eLodPrefix + "hasRelatedSpendingItem");
		subsidyMunicipality = ResourceFactory.createProperty(eLodPrefix + "subsidyMunicipality");
		//pc
		agreedPrice = ResourceFactory.createProperty(publicContractsPrefix + "agreedPrice");
		
		/** Data Properties **/
		//eLod
		countryIsoCode = ResourceFactory.createProperty(eLodPrefix + "countryIsoCode");
		currencyIsoCode = ResourceFactory.createProperty(eLodPrefix + "currencyIsoCode");
		//gr
		name = ResourceFactory.createProperty(goodRelationsPrefix + "name");
		hasCurrencyValue = ResourceFactory.createProperty(goodRelationsPrefix + "hasCurrencyValue");
		valueAddedTaxIncluded = ResourceFactory.createProperty(goodRelationsPrefix + "valueAddedTaxIncluded");
		//SKOS
		prefLabel = ResourceFactory.createProperty(skosPrefix + "prefLabel");
		//dc
		subject = ResourceFactory.createProperty(dcPrefix + "subject");
	}
	
}