package ontology;

import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.vocabulary.OWL;
import com.hp.hpl.jena.vocabulary.RDFS;

/**
 * @author G. Razis
 */
public class OntologyInitialization {
	
	/**
     * Add the necessary prefixes to the model we are currently 
     * working with.
     * 
     * @param Model the model we are currently working with
     */
	public void setPrefixes(Model model) {
		model.setNsPrefix("elod", Ontology.eLodPrefix);
		model.setNsPrefix("elodGeo", Ontology.eLodGeoPrefix);
    	model.setNsPrefix("pc", Ontology.publicContractsPrefix);
    	model.setNsPrefix("skos", Ontology.skosPrefix);
    	model.setNsPrefix("gr", Ontology.goodRelationsPrefix);
    	model.setNsPrefix("rov", Ontology.regOrgPrefix);
    	model.setNsPrefix("org", Ontology.orgPrefix);
    	model.setNsPrefix("foaf", Ontology.foafPrefix);
    	model.setNsPrefix("xsd", Ontology.xsdPrefix);
    	model.setNsPrefix("dc", Ontology.dcPrefix);
	}
	
	/**
     * Create the basic hierarchies of the OWL classes and their labels 
     * to the model we are currently working with.
     * 
     * @param Model the model we are currently working with
     */
	public void createHierarchies(Model model) {
		
		//Agent
		model.add(Ontology.agentResource, RDFS.subClassOf, OWL.Thing);
		model.add(Ontology.agentResource, RDFS.label, model.createLiteral("Agent", "en"));
		model.add(Ontology.agentResource, RDFS.label, model.createLiteral("Πράκτορας", "el"));
				
		//Concept
		model.add(Ontology.conceptResource, RDFS.subClassOf, OWL.Thing);
		
		//Concept - Currency
		model.add(Ontology.currencyResource, RDFS.subClassOf, Ontology.conceptResource);
		model.add(Ontology.currencyResource, RDFS.label, model.createLiteral("Currency", "en"));
		model.add(Ontology.currencyResource, RDFS.label, model.createLiteral("Νόμισμα", "el"));
		
		//Concept - Project Status
		model.add(Ontology.projectStatusResource, RDFS.subClassOf, Ontology.conceptResource);
		model.add(Ontology.projectStatusResource, RDFS.label, model.createLiteral("Project Status", "en"));
		model.add(Ontology.projectStatusResource, RDFS.label, model.createLiteral("Κατάσταση έργου", "el"));
		
		//Concept - Country
		model.add(Ontology.countryResource, RDFS.subClassOf, Ontology.conceptResource);
		model.add(Ontology.countryResource, RDFS.label, model.createLiteral("Country", "en"));
		model.add(Ontology.countryResource, RDFS.label, model.createLiteral("Χώρα", "el"));
			
		//Agent - BusinessEntity
		model.add(Ontology .businessEntityResource, RDFS.subClassOf, Ontology.agentResource);
		model.add(Ontology.businessEntityResource, RDFS.label, model.createLiteral("Business Entity", "en"));
		model.add(Ontology.businessEntityResource, RDFS.label, model.createLiteral("Επιχειρηματική Οντότητα", "el"));
		
		//Agent - Registered Organization
		model.add(Ontology.registeredOrganizationResource, RDFS.subClassOf, Ontology.agentResource);
		model.add(Ontology.registeredOrganizationResource, RDFS.label, model.createLiteral("Registered Organization", "en"));
		model.add(Ontology.registeredOrganizationResource, RDFS.label, model.createLiteral("Καταχωρημένος Οργανισμός", "el"));
		
		//Agent - Organization (FOAF)
		model.add(Ontology.organizationResource, RDFS.subClassOf, Ontology.agentResource);
		model.add(Ontology.organizationResource, RDFS.label, model.createLiteral("Organization", "en"));
		model.add(Ontology.organizationResource, RDFS.label, model.createLiteral("Οργανισμός", "el"));
		
		//Agent - Organization (org)
		model.add(Ontology.orgOrganizationResource, RDFS.subClassOf, Ontology.agentResource);
		model.add(Ontology.orgOrganizationResource, RDFS.label, model.createLiteral("Organization", "en"));
		model.add(Ontology.orgOrganizationResource, RDFS.label, model.createLiteral("Οργανισμός", "el"));
		
		//Subsidy
		model.add(Ontology.subsidyResource, RDFS.subClassOf, OWL.Thing);
		model.add(Ontology.subsidyResource, RDFS.label, model.createLiteral("Subsidy", "en"));
		model.add(Ontology.subsidyResource, RDFS.label, model.createLiteral("Επιδότηση", "el"));
		
		//Budget Item
		model.add(Ontology.budgetItemResource, RDFS.subClassOf, OWL.Thing);
		model.add(Ontology.budgetItemResource, RDFS.label, model.createLiteral("Budget Item", "en"));
		model.add(Ontology.budgetItemResource, RDFS.label, model.createLiteral("Αντικείμενο Προϋπολογισμού", "el"));
		
		//Contract
		model.add(Ontology.contractResource, RDFS.subClassOf, OWL.Thing);
		model.add(Ontology.contractResource, RDFS.label, model.createLiteral("Public Contract", "en"));
		model.add(Ontology.contractResource, RDFS.label, model.createLiteral("Δημόσια Σύμβαση", "el"));
		
		//Spending Item
		model.add(Ontology.spendingItemResource, RDFS.subClassOf, OWL.Thing);
		model.add(Ontology.spendingItemResource, RDFS.label, model.createLiteral("Spending Item", "en"));
		model.add(Ontology.spendingItemResource, RDFS.label, model.createLiteral("Αντικείμενο Δαπάνης", "el"));
		
		//Unit Price Specification
		model.add(Ontology.unitPriceSpecificationResource, RDFS.subClassOf, OWL.Thing);
		model.add(Ontology.unitPriceSpecificationResource, RDFS.label, model.createLiteral("Unit Price Specification", "en"));
		model.add(Ontology.unitPriceSpecificationResource, RDFS.label, model.createLiteral("Προδιαγραφή τιμής ανά μονάδα", "el"));
		
		//Expenditure Line
		model.add(Ontology.expenditureLineResource, RDFS.subClassOf, OWL.Thing);
		model.add(Ontology.expenditureLineResource, RDFS.label, model.createLiteral("Expenditure Line", "en"));
		model.add(Ontology.expenditureLineResource, RDFS.label, model.createLiteral("Τμήμα Δαπάνης", "el"));
	}

}