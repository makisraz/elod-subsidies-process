package objects;

import com.hp.hpl.jena.rdf.model.Resource;

/**
 * @author G. Razis
 */
public class Subsidy {
	
	private String id = null;
	private String beneficiaryName = null;
	private String title = null;
	private float budgetAmount = -1;
	private float contractAmount = -1;
	private float paymentAmount = -1;
	private String projectStatus = null;
	private Resource asSellerUri = null;
	private Resource asBuyerUri = null;
	private String district = null;
	
	public Subsidy(String id, String beneficiaryName, String title, float budgetAmount, float contractAmount, 
			float paymentAmount, String projectStatus, Resource asSellerUri, Resource asBuyerUri, String district) {
		
		this.id = id;
		this.beneficiaryName = beneficiaryName;
		this.title = title;
		this.budgetAmount = budgetAmount;
		this.contractAmount = contractAmount;
		this.paymentAmount = paymentAmount;
		this.projectStatus = projectStatus;
		this.asSellerUri = asSellerUri;
		this.asBuyerUri = asBuyerUri;
		this.district = district;
		
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getBeneficiaryName() {
		return beneficiaryName;
	}

	public void setBeneficiaryName(String beneficiaryName) {
		this.beneficiaryName = beneficiaryName;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public float getBudgetAmount() {
		return budgetAmount;
	}

	public void setBudgetAmount(float budgetAmount) {
		this.budgetAmount = budgetAmount;
	}

	public float getContractAmount() {
		return contractAmount;
	}

	public void setContractAmount(float contractAmount) {
		this.contractAmount = contractAmount;
	}

	public float getPaymentAmount() {
		return paymentAmount;
	}

	public void setPaymentAmount(float paymentAmount) {
		this.paymentAmount = paymentAmount;
	}

	public String getProjectStatus() {
		return projectStatus;
	}

	public void setProjectStatus(String projectStatus) {
		this.projectStatus = projectStatus;
	}

	public Resource getAsSellerUri() {
		return asSellerUri;
	}

	public void setAsSellerUri(Resource asSellerUri) {
		this.asSellerUri = asSellerUri;
	}

	public Resource getAsBuyerUri() {
		return asBuyerUri;
	}

	public void setAsBuyerUri(Resource asBuyerUri) {
		this.asBuyerUri = asBuyerUri;
	}

	public String getDistrict() {
		return district;
	}

	public void setDistrict(String district) {
		this.district = district;
	}
	
}