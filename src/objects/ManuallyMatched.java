package objects;

/**
 * @author G. Razis
 */
public class ManuallyMatched {

	private String vatId = null;
	private String name = null;

	public ManuallyMatched(String vatId, String name) {
		
		this.vatId = vatId;
		this.name = name;
	}

	public String getVatId() {
		return vatId;
	}

	public void setVatId(String vatId) {
		this.vatId = vatId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	
}