package objects;

/**
 * @author G. Razis
 */
public class Municipality {

	private String fileName = null;
	private String uri = null;
	private String nameInGraph = null;

	public Municipality(String fileName, String uri, String nameInGraph) {
		
		this.fileName = fileName;
		this.uri = uri;
		this.nameInGraph = nameInGraph;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getUri() {
		return uri;
	}

	public void setUri(String uri) {
		this.uri = uri;
	}

	public String getNameInGraph() {
		return nameInGraph;
	}

	public void setNameInGraph(String nameInGraph) {
		this.nameInGraph = nameInGraph;
	}

}