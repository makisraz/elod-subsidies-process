package other;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

import json.QueryConfiguration;

import virtuoso.jena.driver.VirtGraph;
import virtuoso.jena.driver.VirtuosoQueryExecution;
import virtuoso.jena.driver.VirtuosoQueryExecutionFactory;

import com.hp.hpl.jena.query.QuerySolution;
import com.hp.hpl.jena.query.ResultSet;

/**
 * @author G. Razis
 */
public class UnmatchedSums {

	/** Read the names from the text file and query their sums. 
	 *  Due to minor mismatches (e.g. missing name parts or capitalized letters) 
	 *  two queries are used: exact match and regex.
	 **/

	public static void main(String[] args) throws IOException {
		
		/* Subsidies Graph */
		VirtGraph graphSubsidies = new VirtGraph(QueryConfiguration.queryGraphSubsidies, QueryConfiguration.connectionString, 
											 	 QueryConfiguration.username, QueryConfiguration.password);
		System.out.println("Connected to Subsidies Graph!");
		
		String benName = null;

		BufferedReader br = new BufferedReader(new FileReader("C:/Users/Makis/workspace/Subsidies/src/other/unmatched.txt"));
		
		while ((benName = br.readLine()) != null) {
			System.out.println(benName);
			String[] amounts = getBeneficiarySums(graphSubsidies, benName);
			writeArrayDataToFile("results", amounts);
		}
		
		br.close();
		
		System.out.println("\nFinished!");
	}
	
	private static String[] getBeneficiarySums(VirtGraph graphSubsidies, String beneficiaryName) {
		
		String[] amounts = null; 

		String querySums = "PREFIX elod: <http://linkedeconomy.org/ontology#> " +
				"PREFIX gr: <http://purl.org/goodrelations/v1#> " +
				"SELECT (SUM(xsd:decimal(?biAmount)) AS ?biAmount) (SUM(xsd:decimal(?cntrAmount)) AS ?cntrAmount) " +
					   "(SUM(xsd:decimal(?siAmount)) AS ?siAmount) " +
				"FROM <" + QueryConfiguration.queryGraphSubsidies + "> " +
				"WHERE { " +
				"?subsidy rdf:type elod:Subsidy ; " +
				         "elod:beneficiary ?beneficiary ; " +
				         "elod:hasRelatedBudgetItem ?relatedBudgetItem ; " +
				         "elod:hasRelatedContract ?relatedContract ; " +
				         "elod:hasRelatedSpendingItem ?relatedSpendingItem . " +
				"?relatedBudgetItem elod:price ?biUps . " +
				"?biUps gr:hasCurrencyValue ?biAmount . " +
				"?relatedContract pc:agreedPrice ?cntrUps . " +
				"?cntrUps gr:hasCurrencyValue ?cntrAmount . " +
				"?relatedSpendingItem elod:amount ?siUps . " +
				"?siUps gr:hasCurrencyValue ?siAmount . " +
				"?beneficiary gr:name ?name . " +
				//"FILTER (?name = \"" + beneficiaryName + "\"^^<http://www.w3.org/2001/XMLSchema#string>) . " +
				"FILTER regex(?name, \"" + beneficiaryName + "\", \"i\") . " +
				"}";

		VirtuosoQueryExecution vqeSums = VirtuosoQueryExecutionFactory.create(querySums, graphSubsidies);
		ResultSet resultsSums = vqeSums.execSelect();

		if (resultsSums.hasNext()) {
			try {
				QuerySolution result = resultsSums.nextSolution();
				String biAmount = result.getLiteral("biAmount").getString();
				String cntrAmount = result.getLiteral("cntrAmount").getString();
				String siAmount = result.getLiteral("siAmount").getString();
				amounts = new String[] {biAmount, cntrAmount, siAmount};
			} catch (Exception e) {
				writeDataToFile("unhandled", beneficiaryName);
				amounts = new String[] {"-", "-", "-"};
			}
		}

		vqeSums.close();
			
		return amounts;
	}
	
	private static void writeArrayDataToFile(String fileName, String[] data) {
		
		try {
		    PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(fileName + ".txt", true)));
		    out.println(data[0] + "	" + data[1] + "	" + data[2]);
		    out.close();
		} catch (IOException e) {
		    e.printStackTrace();
		}
		
	}
	
	private static void writeDataToFile(String fileName, String data) {
		
		try {
		    PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(fileName + ".txt", true)));
		    out.println(data);
		    out.close();
		} catch (IOException e) {
		    e.printStackTrace();
		}
		
	}

}