package csv;

import java.util.List;

import virtuoso.jena.driver.VirtGraph;

/**
 * @author G. Razis
 */
public class ExportAllSubsidies {
	
	public static void main(String[] args) {
		
		Queries qs = new Queries();
		HelperMethods hm = new HelperMethods();
		
		String csvFile = QueryConfiguration.csvFilepath + "BeneficiarySumsAndCounters.csv";
		
		int counter = 0;
		
		//create the CSV file
		hm.createCsvFile(csvFile);
		
		/* Subsidies */
		VirtGraph graphSubsidies = new VirtGraph(QueryConfiguration.queryGraphSubsidies, QueryConfiguration.connectionString, 
												 QueryConfiguration.username, QueryConfiguration.password);
		System.out.println("Connected to Subsidies Graph!");
		
		/* Organizations Graph */
		VirtGraph graphOrganizations = new VirtGraph(QueryConfiguration.queryGraphOrganizations, QueryConfiguration.connectionString, 
													 QueryConfiguration.username, QueryConfiguration.password);
		System.out.println("Connected to Organizations Graph!");
		
		/** export Payments to Invalid Sellers **/
		System.out.println("\nQuerying Subsidies");
		List<String[]> subsidiesList = qs.getSubsidies(graphSubsidies, graphOrganizations, true);
		
		for (String[] data : subsidiesList) {
			counter += 1;
			System.out.println("Writing row " + counter);
			hm.writeCsvRecord(csvFile, data[0], data[1], data[2], data[3], data[4], data[5], data[6]);
		}
		
		System.out.println("\nFinished!");
	}

}