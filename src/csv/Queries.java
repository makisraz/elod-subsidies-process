package csv;

import java.util.ArrayList;
import java.util.List;

import json.HelperMethods;

import virtuoso.jena.driver.VirtGraph;
import virtuoso.jena.driver.VirtuosoQueryExecution;
import virtuoso.jena.driver.VirtuosoQueryExecutionFactory;

import com.hp.hpl.jena.query.QuerySolution;
import com.hp.hpl.jena.query.ResultSet;
import com.hp.hpl.jena.rdf.model.Literal;
import com.hp.hpl.jena.rdf.model.Resource;

/**
 * @author G. Razis
 */
public class Queries {
	
	private HelperMethods hm = new HelperMethods();
	
	/** Buyer name **/
	public Literal getBuyerLegalName(VirtGraph graphOrganizations, String buyerUri) {
		
		Literal orgName = null;
		
		if (checkUri(buyerUri)) {
			String queryName = "PREFIX gr: <http://purl.org/goodrelations/v1#> " +
			  		  "SELECT ?legalName " +
			  		  "FROM <" + QueryConfiguration.queryGraphOrganizations + "> " +
			  		  "WHERE { " +
			  		  "<" + buyerUri +"> gr:legalName ?legalName . " +
			  		  "}";
	
			VirtuosoQueryExecution vqeName = VirtuosoQueryExecutionFactory.create(queryName, graphOrganizations);		
			ResultSet resultsName = vqeName.execSelect();
			
			while (resultsName.hasNext()) {
				QuerySolution result = resultsName.nextSolution();
				orgName = result.getLiteral("legalName");
			}
			
			vqeName.close();
		}
		
		return orgName;
	}
	
	public Literal getBuyerName(VirtGraph graphOrganizations, String buyerUri) {
		
		Literal orgName = null;
		
		if (checkUri(buyerUri)) {
			String queryName = "PREFIX gr: <http://purl.org/goodrelations/v1#> " +
			  		  "SELECT ?name " +
			  		  "FROM <" + QueryConfiguration.queryGraphOrganizations + "> " +
			  		  "WHERE { " +
			  		  "<" + buyerUri +"> gr:name ?name . " +
			  		  "}";
	
			VirtuosoQueryExecution vqeName = VirtuosoQueryExecutionFactory.create(queryName, graphOrganizations);		
			ResultSet resultsName = vqeName.execSelect();
			
			while (resultsName.hasNext()) {
				QuerySolution result = resultsName.nextSolution();
				orgName = result.getLiteral("name");
			}
			
			vqeName.close();
		}
		
		return orgName;
	}
	
	public Literal getOrganizationUnitName(VirtGraph graphOrganizations, String buyerUnitUri) {
		
		Literal orgName = null;
		
		if (checkUri(buyerUnitUri)) {
			String queryName = "SELECT ?name " +
				  		  "FROM <" + QueryConfiguration.queryGraphOrganizations + "> " +
				  		  "WHERE { " +
				  		  "<" + buyerUnitUri +"> rdfs:label ?name . " +
				  		  "}";
	
			VirtuosoQueryExecution vqeName = VirtuosoQueryExecutionFactory.create(queryName, graphOrganizations);		
			ResultSet resultsName = vqeName.execSelect();
			
			while (resultsName.hasNext()) {
				QuerySolution result = resultsName.nextSolution();
				orgName = result.getLiteral("name");
			}
			
			vqeName.close();
		}
		
		return orgName;
	}
	
	public String configureBuyerOrgOrUnitName(VirtGraph graphOrganizations, Literal buyerlegalName, Resource orgUri) {
		
		String buyerNameStr = "";
		
		if (buyerlegalName != null) { //Legal Name
			buyerNameStr = hm.cleanInvalidCharsJsonData(buyerlegalName.getString());
		} else { //Name
			Literal buyerName = getBuyerName(graphOrganizations, orgUri.getURI());
			if (buyerName != null) {
				buyerNameStr = hm.cleanInvalidCharsJsonData(buyerName.getString());
			} else {
				Literal buyerUnitName = getOrganizationUnitName(graphOrganizations, orgUri.getURI());
				if (buyerUnitName != null) { //Label
					buyerNameStr = hm.cleanInvalidCharsJsonData(buyerUnitName.getString());
				} else { //Nothing
					buyerNameStr = "Name not found";
				}
			}
		}
		
		return buyerNameStr;
	}
	
	public boolean checkUri(String uri) {
		
		boolean correctUriFlag = true;
		
		if (uri.equalsIgnoreCase("http://linkedeconomy.org/resource/Organization/")) {
			correctUriFlag = false;
		} else if (uri.equalsIgnoreCase("http://linkedeconomy.org/resource/OrganizationalUnit/")) {
			correctUriFlag = false;
		} else if (uri.equalsIgnoreCase("http://linkedeconomy.org/resource/Person/")) {
			correctUriFlag = false;
		} else if ( uri.equalsIgnoreCase("http://linkedeconomy.org/resource/Organization/..") || 
				    uri.equalsIgnoreCase("http://linkedeconomy.org/resource/Organization/.") ) {
			correctUriFlag = false;
		}
		
		if ( uri.contains(" ") || uri.contains("`") || uri.contains("\"") ) {
			correctUriFlag = false;
		}
		
		return correctUriFlag;
	}
	
	/** Seller Name **/
	public Literal getSellerLegalName(VirtGraph graphOrganizations, String sellerUri) {
		
		Literal orgName = null;
		
		if (checkUri(sellerUri)) {
			String queryName = "PREFIX gr: <http://purl.org/goodrelations/v1#> " +
			  		  "SELECT ?legalName ?afm " +
			  		  "FROM <" + QueryConfiguration.queryGraphOrganizations + "> " +
			  		  "WHERE { " +
			  		  "<" + sellerUri +"> gr:legalName ?legalName ; " +
			  		  		             "gr:vatID ?afm . " +
			  		  "}";
	
			VirtuosoQueryExecution vqeName = VirtuosoQueryExecutionFactory.create(queryName, graphOrganizations);		
			ResultSet resultsName = vqeName.execSelect();
			
			if (resultsName.hasNext()) {
				QuerySolution result = resultsName.nextSolution();
				orgName = result.getLiteral("legalName");
			}
			
			vqeName.close();
		}
		
		return orgName;
	}
	
	public Literal getSellerName(VirtGraph graphOrganizations, String sellerUri) {
		
		Literal orgName = null;
		
		if (checkUri(sellerUri)) {
			String queryName = "PREFIX gr: <http://purl.org/goodrelations/v1#> " +
			  		  "SELECT (MAX(?name) AS ?otherName) ?afm " +
			  		  "FROM <" + QueryConfiguration.queryGraphOrganizations + "> " +
			  		  "WHERE { " +
			  		  "<" + sellerUri +"> gr:name ?name ; " +
			  		  		             "gr:vatID ?afm . " +
			  		  "}";
	
			VirtuosoQueryExecution vqeName = VirtuosoQueryExecutionFactory.create(queryName, graphOrganizations);		
			ResultSet resultsName = vqeName.execSelect();
			
			if (resultsName.hasNext()) {
				QuerySolution result = resultsName.nextSolution();
				orgName = result.getLiteral("otherName");
			}
			
			vqeName.close();
		}
		
		return orgName;
	}
	
	public Literal getSellerNameTemp(VirtGraph graphToSearch, String graphName, String sellerUri) {
		
		Literal orgName = null;
		
		if (checkUri(sellerUri)) {
			String queryName = "PREFIX gr: <http://purl.org/goodrelations/v1#> " +
			  		  "SELECT (MAX(?name) AS ?otherName) ?afm " +
			  		  "FROM <" + graphName + "> " +
			  		  "WHERE { " +
			  		  "<" + sellerUri +"> gr:name ?name ; " +
			  		  		             "gr:vatID ?afm . " +
			  		  "}";
	
			VirtuosoQueryExecution vqeName = VirtuosoQueryExecutionFactory.create(queryName, graphToSearch);		
			ResultSet resultsName = vqeName.execSelect();
			
			if (resultsName.hasNext()) {
				QuerySolution result = resultsName.nextSolution();
				orgName = result.getLiteral("otherName");
			}
			
			vqeName.close();
		}
		
		return orgName;
	}
	
	public String configureSellerOrgOrUnitOrPersonName(VirtGraph graphOrganizations, Literal sellerName, Resource sellerUri) {
		
		String sellerNameStr = "";
		
		if (sellerName != null) {
			sellerNameStr = hm.cleanInvalidCharsJsonData(sellerName.getString());
		} else {
			Literal sellerUnitName = getOrganizationUnitName(graphOrganizations, sellerUri.getURI());
			if (sellerUnitName != null) {
				sellerNameStr = hm.cleanInvalidCharsJsonData(sellerUnitName.getString());
			} else {
				Literal sellerPersonName = getPersonName(graphOrganizations, sellerUri.getURI());
				if (sellerPersonName != null) {
					sellerNameStr = hm.cleanInvalidCharsJsonData(sellerPersonName.getString());
				} else {
					sellerNameStr = "Name not found";
				}
			}
		}
		
		return sellerNameStr;
	}
	
	public Literal getPersonName(VirtGraph graphOrganizations, String personUri) {
		
		Literal personName = null;
		
		if (checkUri(personUri)) {
			String queryName = "PREFIX gr: <http://purl.org/goodrelations/v1#> " +
						  "SELECT ?name " +
				  		  "FROM <" + QueryConfiguration.queryGraphOrganizations + "> " +
				  		  "WHERE { " +
				  		  "<" + personUri +"> gr:name ?name . " +
				  		  "}";
	
			VirtuosoQueryExecution vqeName = VirtuosoQueryExecutionFactory.create(queryName, graphOrganizations);		
			ResultSet resultsName = vqeName.execSelect();
			
			while (resultsName.hasNext()) {
				QuerySolution result = resultsName.nextSolution();
				personName = result.getLiteral("name");
			}
			
			vqeName.close();
		}
		
		return personName;
	}
	
	/** tempId_ Beneficiary **/
	public Literal getBeneficiaryTempName(VirtGraph graphSubsidies, String beneficiaryUri) {
		
		Literal orgName = null;
		
		String queryName = "PREFIX gr: <http://purl.org/goodrelations/v1#> " +
		  		  "SELECT ?name " +
		  		  "FROM <" + QueryConfiguration.queryGraphSubsidies + "> " +
		  		  "WHERE { " +
		  		  "<" + beneficiaryUri +"> gr:name ?name . " +
		  		  "}";
		
		VirtuosoQueryExecution vqeName = VirtuosoQueryExecutionFactory.create(queryName, graphSubsidies);		
		ResultSet resultsName = vqeName.execSelect();
		
		while (resultsName.hasNext()) {
			QuerySolution result = resultsName.nextSolution();
			orgName = result.getLiteral("name");
		}
		
		vqeName.close();
		
		return orgName;
	}
		
	/** Export queries **/
	public List<String[]> getSubsidies(VirtGraph graphSubsidies, VirtGraph graphOrganizations, boolean projectCompleted) {
		
		int counter = 0;
		String queryPart = "";
		List<String[]> subsidies = new ArrayList<String[]>();
		
		if (projectCompleted) {
			queryPart = "elod:projectStatus <http://linkedeconomy.org/resource/ProjectStatus/Completed> ; ";
		}
		
		String querySubsidies = "PREFIX gr: <http://purl.org/goodrelations/v1#> " +
				"PREFIX elod: <http://linkedeconomy.org/ontology#> " +
				"PREFIX pc: <http://purl.org/procurement/public-contracts#> " +
				"SELECT ?beneficiary (COUNT(?subsidy) AS ?subsidyCounter) (SUM(xsd:decimal(?biAmount)) AS ?biAmount) " +
						"(SUM(xsd:decimal(?cntrAmount)) AS ?cntrAmount) (SUM(xsd:decimal(?siAmount)) AS ?siAmount) " +
				"FROM <" + QueryConfiguration.queryGraphSubsidies + "> " +
				"WHERE { " +
				"?subsidy elod:hasRelatedBudgetItem ?budgetItem ; " +
				         "elod:hasRelatedContract ?contract ; " +
				         "elod:hasRelatedSpendingItem ?spendingItem ; " +
				         queryPart + 
				         "elod:beneficiary ?beneficiary . " +
				"?budgetItem elod:price ?biUps . " +
				"?biUps gr:hasCurrencyValue ?biAmount . " +
				"?contract pc:agreedPrice ?cntrUps . " +
				"?cntrUps gr:hasCurrencyValue ?cntrAmount. " +
				"?spendingItem elod:amount ?siUps . " +
				"?siUps gr:hasCurrencyValue ?siAmount. " +
				"FILTER (!regex(?beneficiary, \"tempId_\")) . " +
				"} " +
				"GROUP BY ?beneficiary " +
				"ORDER BY DESC (?subsidyCounter) " +
				"LIMIT 500";

		VirtuosoQueryExecution vqeSubsidies = VirtuosoQueryExecutionFactory.create(querySubsidies, graphSubsidies);		
		ResultSet resultsSubsidies = vqeSubsidies.execSelect();		
		
		while (resultsSubsidies.hasNext()) {
			QuerySolution result = resultsSubsidies.nextSolution();
			System.out.println("Subsidy " + counter++);
			Resource beneficiaryUri = result.getResource("beneficiary");
			String subsidyCounter = result.getLiteral("subsidyCounter").getString();
			Literal biAmount = result.getLiteral("biAmount");
			String biAmountStr = hm.roundAmountWithDecimals(biAmount);
			Literal cntrAmount = result.getLiteral("cntrAmount");
			String cntrAmountStr = hm.roundAmountWithDecimals(cntrAmount);
			Literal siAmount = result.getLiteral("siAmount");
			String siAmountStr = hm.roundAmountWithDecimals(siAmount);
			
			String beneficiaryName = null;
			Literal asBuyerName = getBuyerLegalName(graphOrganizations, beneficiaryUri.toString());
			String asBuyerNameStr = configureBuyerOrgOrUnitName(graphOrganizations, asBuyerName, beneficiaryUri);
			
			if (!beneficiaryUri.getURI().contains("tempId_")) {
				if (asBuyerNameStr.equalsIgnoreCase("Name not found")) {
					Literal asSellerName = getSellerLegalName(graphOrganizations, beneficiaryUri.toString());
					String asSellerNameStr = configureSellerOrgOrUnitOrPersonName(graphOrganizations, asSellerName, beneficiaryUri);
					beneficiaryName = asSellerNameStr;
				} else {
					beneficiaryName = asBuyerNameStr;
				}
			} else {
				beneficiaryName = getBeneficiaryTempName(graphSubsidies, beneficiaryUri.getURI()).getString();
			}
				
			subsidies.add(new String[] {beneficiaryUri.toString(), beneficiaryName, subsidyCounter, 
										biAmountStr, cntrAmountStr, siAmountStr, String.valueOf(counter)});
		}
		
		vqeSubsidies.close();
		
		return subsidies;
	}
	
}