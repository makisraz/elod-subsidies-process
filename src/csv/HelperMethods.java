package csv;

import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;

import com.hp.hpl.jena.rdf.model.Literal;

import au.com.bytecode.opencsv.CSVWriter;

/**
 * @author G. Razis
 */
public class HelperMethods {
	
	public void createCsvFile(String filename) {
		
		CSVWriter writer = null;
		try {
			//UTF-8 with BOM charset to the CSVs
		    byte[] bom = new byte[]{(byte)0xEF, (byte)0xBB, (byte)0xBF};
		    FileOutputStream fos = new FileOutputStream(filename);
		    fos.write(bom, 0, bom.length);
		    writer = new CSVWriter(new OutputStreamWriter(fos));
			String[] headers = new String[]{"beneficiaryUri", "beneficiaryName", "subsidyCounter", "budgetAmount", "contractAmount", "paymentAmount", "counter"};
			writer.writeNext(headers);
		    writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
	public void writeCsvRecord(String filename, String beneficiaryUri, String beneficiaryName, String subsidyCounter,
							   String budgetAmount, String contractAmount, String paymentAmount, String counter) {
		
		CSVWriter writer = null;
		
		try {
			writer = new CSVWriter(new FileWriter(filename, true));
			String[] record = new String[] {beneficiaryUri, beneficiaryName, subsidyCounter, budgetAmount, contractAmount, paymentAmount, counter};
		    writer.writeNext(record);
		    writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
	public String roundAmountWithDecimals(Literal totalAmount) {
		
		String totalAmountStr = "";
		
		if (totalAmount != null) {
			String totalAmountParts[] = totalAmount.toString().split("http");
			String amount = totalAmountParts[0].replace("^", "");
			try {
				String[] numParts = amount.split("\\.");
				String twoDecimalsStr = "0." + numParts[1].subSequence(0, 2).toString();
				totalAmountStr = numParts[0] + "." + twoDecimalsStr.split("\\.")[1];
			} catch (Exception e) {
				System.out.println("Integer or Less than 3 decimals");
				totalAmountStr = amount;
			}
		} else {
			totalAmountStr = "0";
		}
		
		return totalAmountStr;
	}

}