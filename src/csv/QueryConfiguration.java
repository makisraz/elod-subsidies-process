package csv;

/**
 * @author G. Razis
 */
public class QueryConfiguration {
	
	public static final String queryGraphSubsidies = "http://linkedeconomy.org/Subsidies";
	public static final String queryGraphOrganizations = "http://linkedeconomy.org/Organizations";
	
	public static final String connectionString = "jdbc:virtuoso://143.233.226.49:1111/autoReconnect=true/charset=UTF-8/log_enable=2";
	
	public static final String username = "dba";
	public static final String password = "d3ll0lv@69";
	
	public static final String jsonFilepath = "C:/Users/Makis/Desktop/eprocur/JSON/";//"/home/makis/JSON/diavgeiaII/"; "/home/makis/JSON/diavgeiaII/couchDb/";
	public static final String csvFilepath = "C:/Users/Makis/Desktop/eprocur/CSV/"; //"/home/makis/CSV/";
}