package afterMatch;

import java.io.FileReader;

import java.util.ArrayList;
import java.util.List;

import objects.ManuallyMatched;

import utils.HelperMethods;

import au.com.bytecode.opencsv.CSVReader;

/**
 * @author G. Razis
 */
public class CsvActions {
	
	public List<ManuallyMatched> readManuallyMatchedCsv(String csvFilePath) {
		
		HelperMethods hm = new HelperMethods();
		
		String afm = null;
		String beneficiaryName = null;
		
		List<ManuallyMatched> matchedList = new ArrayList<ManuallyMatched>();
		
		try {
			CSVReader reader = new CSVReader(new FileReader(csvFilePath), ';', '"', '\0');
			String[] nextLine = null;
			System.out.println("\n" + csvFilePath);
			
			while ((nextLine = reader.readNext()) != null) {

				for (int i = 0; i < 2; i++) {
					if (i == 0) {
						afm = hm.cleanCsvData(nextLine[i]);
					} else {
						beneficiaryName = hm.cleanCsvData(nextLine[i]);
					}
				}
				
				ManuallyMatched matchedObject = new ManuallyMatched(afm, beneficiaryName);
				matchedList.add(matchedObject);
				
				//re-initialize the variables
				afm = null;
				beneficiaryName = null;
			}

			reader.close();

		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return matchedList;
	}
	
}