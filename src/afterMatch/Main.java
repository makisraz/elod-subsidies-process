package afterMatch;

import java.util.List;

import json.QueryConfiguration;

import objects.ManuallyMatched;

import organizations.AgentHandler;
import organizations.Queries;

import com.hp.hpl.jena.query.QuerySolution;
import com.hp.hpl.jena.query.ResultSet;
import com.hp.hpl.jena.rdf.model.Resource;

import virtuoso.jena.driver.VirtGraph;
import virtuoso.jena.driver.VirtuosoQueryExecution;
import virtuoso.jena.driver.VirtuosoQueryExecutionFactory;

/**
 * @author G. Razis
 */
public class Main {

	public static VirtGraph graphOrgs = null;

	/** Read the names of the manually matched beneficiaries 
	 *  from the text file and query their Subsidies. 
	 *  Through the vatId service create their permanent URI. 
	 *  Then delete the temporary with the permanent one.
	 **/
	public static void main(String[] args) {
		
		Queries qsOrg = new Queries();
		AgentHandler agents = new AgentHandler();
		CsvActions csvMethods = new CsvActions();
		
		/* Subsidies Graph */
		VirtGraph graphSubsidies = new VirtGraph(QueryConfiguration.queryGraphSubsidies, QueryConfiguration.connectionString, 
											 	 QueryConfiguration.username, QueryConfiguration.password);
		System.out.println("Connected to Subsidies Graph!");
		
		/* Organizations Graph */
		graphOrgs = new VirtGraph(QueryConfiguration.queryGraphOrganizations, QueryConfiguration.connectionString, 
								  QueryConfiguration.username, QueryConfiguration.password);
		System.out.println("Connected to Organizations Graph!");
		
		//create the subsidy Objects list
		List<ManuallyMatched> matchedList = csvMethods.readManuallyMatchedCsv("C:/Users/Makis/workspace/Subsidies/src/afterMatch/matched.csv");
		
		for (ManuallyMatched matchedObj : matchedList) {
			//Get Subsidy details {subsidyUri, relatedBudgetItemUri, relatedContractUri, relatedSpendingItemUri, tempBeneficiaryUri}
			String[] subsidyInfo =  getBeneficiarySubsidies(graphSubsidies, matchedObj.getName());
			
			if (subsidyInfo != null) { //in case already processed
				Object[] agentsDtls = agents.handleAgent(matchedObj.getVatId());
				Resource beneficiaryProperResource = (Resource) agentsDtls[0];
				
				//Replace Subsidy beneficiary
				qsOrg.insertSubsidyBeneficiary(graphSubsidies, subsidyInfo[0], beneficiaryProperResource.getURI());
				qsOrg.deleteSubsidyBeneficiary(graphSubsidies, subsidyInfo[0], subsidyInfo[4]);
				
				//Replace Subsidy beneficiary
				qsOrg.insertBudgetItemBeneficiary(graphSubsidies, subsidyInfo[1], beneficiaryProperResource.getURI());
				qsOrg.deleteBudgetItemBeneficiary(graphSubsidies, subsidyInfo[1], subsidyInfo[4]);
				
				//Replace Contract beneficiary
				qsOrg.insertContractBeneficiary(graphSubsidies, subsidyInfo[2], beneficiaryProperResource.getURI());
				qsOrg.deleteContractBeneficiary(graphSubsidies, subsidyInfo[2], subsidyInfo[4]);
				
				//Replace SpendingItem beneficiary
				qsOrg.insertSpendingItemBeneficiary(graphSubsidies, subsidyInfo[3], beneficiaryProperResource.getURI());
				qsOrg.deleteSpendingItemBeneficiary(graphSubsidies, subsidyInfo[3], subsidyInfo[4]);
				
				//Delete beneficiary temp details
				qsOrg.deleteBeneficiary(graphSubsidies, subsidyInfo[4]);
			}
		}
		
		System.out.println("\nFinished!");
	}
	
	private static String[] getBeneficiarySubsidies(VirtGraph graphSubsidies, String beneficiaryName) {
		
		String[] info = null; 
		
		String queryInfo = "PREFIX elod: <http://linkedeconomy.org/ontology#> " +
				"PREFIX gr: <http://purl.org/goodrelations/v1#> " +
				"SELECT ?subsidy ?relatedBudgetItem ?relatedContract ?relatedSpendingItem ?beneficiary " +
				"FROM <" + QueryConfiguration.queryGraphSubsidies + "> " +
				"WHERE { " +
				"?subsidy rdf:type elod:Subsidy ; " +
				         "elod:beneficiary ?beneficiary ; " +
				         "elod:hasRelatedBudgetItem ?relatedBudgetItem ; " +
				         "elod:hasRelatedContract ?relatedContract ; " +
				         "elod:hasRelatedSpendingItem ?relatedSpendingItem . " +
				"?beneficiary gr:name ?name . " +
				"FILTER regex(?name, \"" + beneficiaryName + "\", \"i\") . " +
				"}";
		
		VirtuosoQueryExecution vqeInfo = VirtuosoQueryExecutionFactory.create(queryInfo, graphSubsidies);
		ResultSet resultsInfo = vqeInfo.execSelect();
		
		if (resultsInfo.hasNext()) {
			QuerySolution result = resultsInfo.nextSolution();
			String subsidyUri = result.getResource("subsidy").getURI();
			String relatedBudgetItemUri = result.getResource("relatedBudgetItem").getURI();
			String relatedContractUri = result.getResource("relatedContract").getURI();
			String relatedSpendingItemUri = result.getResource("relatedSpendingItem").getURI();
			String beneficiaryUri = result.getResource("beneficiary").getURI();
			info = new String[] {subsidyUri, relatedBudgetItemUri, relatedContractUri, relatedSpendingItemUri, beneficiaryUri};
		}
		
		vqeInfo.close();
				
		return info;
	}

}