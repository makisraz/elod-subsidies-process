package json;

import java.math.BigInteger;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.hp.hpl.jena.rdf.model.Resource;

import utils.TransliterationMethods;

import virtuoso.jena.driver.VirtGraph;

/**
 * @author G. Razis
 */
public class StatsTables {
	
	/** http://linkedeconomy.org/el/stats-espa (All Tables & tabs) **/
	
	/** 
	 * Set the Filter at the Queries.getOrgrsWithStats concerning 
	 * the Beneficiaries (tempId or normal)
	 */
	public static void main(String[] args) {
		
		final boolean cummulative = true; /** Συνολική εικόνα **/
		final boolean allBeneficiaries = true; /** Δικαιούχοι ΕΣΠΑ **/
		final boolean beneficiaryAndSeller = true; /** Δικαιούχοι ΕΣΠΑ και Ανάδοχοι Δημοσίου **/
		final boolean allSubsidies = true; /** Επιδοτήσεις ΕΣΠΑ **/
		
		Queries qs = new Queries();
		HelperMethods hm = new HelperMethods();
		TransliterationMethods trans = new TransliterationMethods();
		
		/** Graphs **/	
		/* Subsidies Graph */
		VirtGraph graphSubsidies = new VirtGraph(QueryConfiguration.queryGraphSubsidies, QueryConfiguration.connectionString, 
											 	 QueryConfiguration.username, QueryConfiguration.password);
		System.out.println("Connected to Subsidies Graph!");
		
		/* DiavgeiaII 2014 Graph */
		VirtGraph graphDiavgeiaII2014 = new VirtGraph(QueryConfiguration.queryGraphDiavgeiaII2014, QueryConfiguration.connectionString, 
												      QueryConfiguration.username, QueryConfiguration.password);
		System.out.println("Connected to DiavgeiaII 2014 Graph!");
		
		/* DiavgeiaII 2015 Graph */
		VirtGraph graphDiavgeiaII2015 = new VirtGraph(QueryConfiguration.queryGraphDiavgeiaII2015, QueryConfiguration.connectionString, 
												      QueryConfiguration.username, QueryConfiguration.password);
		System.out.println("Connected to DiavgeiaII 2015 Graph!");
		
		/* E-Procurement Graph */
		VirtGraph graphEproc = new VirtGraph (QueryConfiguration.queryGraphEproc, QueryConfiguration.connectionString, 
											  QueryConfiguration.username, QueryConfiguration.password);
		System.out.println("Connected to Eproc Graph!");
		
		/* Geo Graph */
		VirtGraph graphGeo = new VirtGraph(QueryConfiguration.queryGraphGeo, QueryConfiguration.connectionString, 
										   QueryConfiguration.username, QueryConfiguration.password);
		System.out.println("Connected to Subsidies Statistics Graph!");
		
		/* Organizations Graph */
		VirtGraph graphOrganizations = new VirtGraph(QueryConfiguration.queryGraphOrganizations, QueryConfiguration.connectionString, 
												  QueryConfiguration.username, QueryConfiguration.password);
		System.out.println("Connected to Organizations Graph!");
		
		/** Συνολική εικόνα
		 * # Δικαιούχοι ΕΣΠΑ - # Επιδοτήσεων - Συνολικός Προϋπολογισμός - Συνολικές Συμβάσεις - Συνολικές Πληρωμές - # Δικαιούχοι ΕΣΠΑ & Ανάδοχοι Δημοσίου
		 */
		if (cummulative) {
			System.out.println("Querying Cummulative");
			
			/* initialize the JSON */
			String jsonString = "";
			
			/* start the 'couchDb' JSON */
			jsonString += "{\"docs\":";
			
			/* start the 'normal' JSON */
			jsonString += "[{";
			
			/* create _id tag */
			jsonString += "\"_id\": \"subsidiesCummulative\",";
			
			String numBeneficiers = qs.getTotalBeneficieries(graphSubsidies);
			
			String sums[] = qs.getSubsidiesTotalSums(graphSubsidies);
			
			String numBeneficiariesAndSellers = qs.getTotalBeneficiariesAndSellers(graphSubsidies, graphDiavgeiaII2014, graphDiavgeiaII2015, graphEproc);  
			
			jsonString += "\"subsidiesCummulative\": [";
			
			jsonString += "{\"numBeneficiaries\": \"" + numBeneficiers + "\"," + "\"numSubsidies\": \"" + sums[0] + "\"," + 
						  "\"numBudget\": \"" + sums[1] + "\"," + "\"numContract\": \"" + sums[2] + "\"," + "\"numPayments\": \"" + 
						  sums[3] + "\"," + "\"numBeneficiariesAndSellers\": \"" + numBeneficiariesAndSellers + "\"}";
					
			/* close the 'normal' JSON */
			jsonString += "]}]";
			
			/* close the 'couchDb' JSON */
			jsonString += "}";
			
			hm.writeJsonFile(jsonString, "others", "subsidiesCummulative");
		}
		
		/** Δικαιούχοι ΕΣΠΑ 
		 * top 2000 Subsidies by budget for Unmatched Beneficiaries
		 */
		if (allBeneficiaries) {
			/* Beneficiaries */
			System.out.println("Querying Beneficiaries");
			ArrayList<Resource> beneficiariesList = qs.getBeneficiariesStatsTables(graphSubsidies, true);
			
			/* initialize the JSON */
			String jsonString = "";
			
			/* start the 'couchDb' JSON */
			jsonString += "{\"docs\":";
			
			/* start the 'normal' JSON */
			jsonString += "[{";
			
			/* create _id tag */
			jsonString += "\"_id\": \"subsidiesAllBeneficiaries\",";
			
			jsonString += "\"beneficiaries\": [";
			
			for (Resource beneficiaryUri : beneficiariesList) {
				System.out.println("Querying Beneficiary: " + beneficiaryUri.getURI());
				
				/* counter and sums */
				String[] sumsAndCounter = qs.getBeneficiarySumsAndCounter(graphSubsidies, beneficiaryUri.getURI());
				
				/* percentage */
				int paymentsPercentage = -1;
				if (sumsAndCounter[3].equalsIgnoreCase("0")) {
					paymentsPercentage = 0;
				} else {
					paymentsPercentage = Math.round( (Float.valueOf(sumsAndCounter[3]) * 100) / Float.valueOf(sumsAndCounter[1]) );
				}
				
				/* beneficiary municipalities */
				String municNameEl = qs.getBeneficiaryMunicipalities(graphSubsidies, graphGeo, beneficiaryUri.getURI(), "el");
				String municNameEn = qs.getBeneficiaryMunicipalities(graphSubsidies, graphGeo, beneficiaryUri.getURI(), "en");
				
				/* beneficiary name */
				String benName = qs.getBeneficiaryNameInfo(graphOrganizations, graphSubsidies, beneficiaryUri.getURI());
				String translation = qs.getBeneficiaryEnglishName(graphOrganizations, beneficiaryUri.getURI());
				if (translation == null) {
					translation = trans.transliterationGenerator(benName);
				}
				
				jsonString += "{\"beneficiary\": \"" + "<a href=\\\"" + hm.urlProfilePage(beneficiaryUri.getURI(), "Beneficiary") + 
							  "\\\" target=\\\"_blank\\\">" + benName + "</a>\"," + "\"beneficiaryEng\": \"" + "<a href=\\\"" + 
							  hm.urlProfilePage(beneficiaryUri.getURI(), "Beneficiary") + "\\\" target=\\\"_blank\\\">" + translation +
							  "</a>\"," + "\"counter\": \"" + sumsAndCounter[0] + "\"," + "\"budget\": \"" + sumsAndCounter[1] + 
							  "\"," + "\"contract\": \"" + sumsAndCounter[2] + "\"," + "\"payment\": \"" + sumsAndCounter[3] + 
							  "\"," + "\"percentage\": \"" + String.valueOf(paymentsPercentage) + "\"," + "\"municipality\": \"" + 
							  municNameEl + "\"," + "\"municipalityEng\": \"" + municNameEn + "\"},";
			}
			
			//remove the last comma
			jsonString = jsonString.substring(0, jsonString.length() - 1) + "]";
			
			/* close the 'normal' JSON */
			jsonString += "}]";
			
			/* close the 'couchDb' JSON */
			jsonString += "}";
			
			hm.writeJsonFile(jsonString, "others", "subsidiesTopBeneficiaries");
		}
		
		/** Δικαιούχοι ΕΣΠΑ και Ανάδοχοι Δημοσίου
		 * top 2000 Subsidies by budget for Matched Beneficiaries
		 */
		if (beneficiaryAndSeller) {
			/* Beneficiaries */
			System.out.println("Querying 'Valid' Beneficiaries");
			ArrayList<Resource> beneficiariesList = qs.getBeneficiariesStatsTables(graphSubsidies, false);
			
			/* initialize the JSON */
			String jsonString = "";
			
			/* start the 'couchDb' JSON */
			jsonString += "{\"docs\":";
			
			/* start the 'normal' JSON */
			jsonString += "[{";
			
			/* create _id tag */
			jsonString += "\"_id\": \"subsidiesBeneficiaryAndSeller\",";
			
			jsonString += "\"beneficiariesDetails\": [";
			
			for (Resource beneficiaryUri : beneficiariesList) {
				System.out.println("Querying Beneficiary: " + beneficiaryUri.getURI());
				
				/* counter and sums */
				String[] sumsAndCounter = qs.getBeneficiarySumsAndCounter(graphSubsidies, beneficiaryUri.getURI());
				
				/* Diavgeia 2 payments */
				Map<String, Object[]> diavgeiaGraphs = new HashMap<String, Object[]>();
				diavgeiaGraphs.put("2014", new Object[]{QueryConfiguration.queryGraphDiavgeiaII2014, graphDiavgeiaII2014});
				diavgeiaGraphs.put("2015", new Object[]{QueryConfiguration.queryGraphDiavgeiaII2015, graphDiavgeiaII2015});
				
				List<String> diavgeiaSubAmountsList = new ArrayList<String>();
				for (String key: diavgeiaGraphs.keySet()) {
					diavgeiaSubAmountsList.add(qs.getDiavgeiaTotalPaymentsOfBeneficiary(diavgeiaGraphs.get(key)[0].toString(), (VirtGraph) diavgeiaGraphs.get(key)[1], beneficiaryUri.getURI(), key));
				}
				
				//just in case that the sum is greater than MAX_INT
				BigInteger diavgeiaAmount = new BigInteger("0");
				for (String amount : diavgeiaSubAmountsList) {
					BigInteger a = new BigInteger(String.valueOf(amount));
					diavgeiaAmount = diavgeiaAmount.add(a);
				}
				
				/* e-Procurement payments */
				String khmdhsAmount = qs.getKhmdhsTotalPaymentsOfBeneficiary(graphEproc, beneficiaryUri.getURI());
				
				/* beneficiary name */
				String benName = qs.getBeneficiaryNameInfo(graphOrganizations, graphSubsidies, beneficiaryUri.getURI());
				String translation = qs.getBeneficiaryEnglishName(graphOrganizations, beneficiaryUri.getURI());
				if (translation == null) {
					translation = trans.transliterationGenerator(benName);
				}
				
				jsonString += "{\"beneficiary\": \"" + "<a href=\\\"" + hm.urlProfilePage(beneficiaryUri.getURI(), "Beneficiary") + 
						  	  "\\\" target=\\\"_blank\\\">" + benName + "</a>\"," + "\"beneficiaryEng\": \"" + "<a href=\\\"" + 
						  	  hm.urlProfilePage(beneficiaryUri.getURI(), "Beneficiary") + "\\\" target=\\\"_blank\\\">" + translation +
						  	  "</a>\"," + "\"counter\": \"" + sumsAndCounter[0] + "\"," + "\"budget\": \"" + sumsAndCounter[1] + 
						  	  "\"," + "\"contract\": \"" + sumsAndCounter[2] + "\"," + "\"payment\": \"" + sumsAndCounter[3] + 
						  	  "\"," + "\"diavgeiaAmount\": \"" + diavgeiaAmount + "\"," + "\"khmdhsAmount\": \"" + khmdhsAmount + "\"},";
			}
			
			//remove the last comma
			jsonString = jsonString.substring(0, jsonString.length() - 1) + "],";
			
			/* About */
			jsonString = qs.addAbout(jsonString, hm.getFormattedDateFromDateString(QueryConfiguration.lastUpdateDate));
			
			/* close the 'normal' JSON */
			jsonString += "}]";
			
			/* close the 'couchDb' JSON */
			jsonString += "}";
			
			hm.writeJsonFile(jsonString, "others", "subsidiesBeneficiaryAndSeller");
		}
		
		/** Επιδοτήσεις ΕΣΠΑ 
		 * top 2000 Subsidies by budget
		 */
		if (allSubsidies) {
			/* initialize the JSON */
			String jsonString = "";
			
			/* start the 'couchDb' JSON */
			jsonString += "{\"docs\":";
			
			/* start the 'normal' JSON */
			jsonString += "[{";
			
			/* create _id tag */
			jsonString += "\"_id\": \"subsidiesAllSubsidies\",";
			
			jsonString += "\"subsidiesDetails\": [";
			
			jsonString = qs.getTopSubsidies(jsonString, graphSubsidies, graphOrganizations, graphGeo);
			
			/* close the array */
			jsonString += "]";
			
			/* close the 'normal' JSON */
			jsonString += "}]";
			
			/* close the 'couchDb' JSON */
			jsonString += "}";
			
			hm.writeJsonFile(jsonString, "others", "subsidiesTopSubsidies");
		}

		System.out.println("\nFinished!");
	}
	
}