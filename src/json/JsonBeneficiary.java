package json;

import com.hp.hpl.jena.rdf.model.Resource;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import virtuoso.jena.driver.VirtGraph;

/**
 * @author G. Razis
 */
public class JsonBeneficiary {
	
	/** 
	 * Steps for creating theJSON files
	 * 1) Set by hand the (QueryConfiguration) lastUpdateDate and jsonFilepath
	 */	
	public static void main(String[] args) {
		
		Queries qs = new Queries();
		HelperMethods hm = new HelperMethods();
		
		/* Statistics Graph */
		VirtGraph graphStats = new VirtGraph(QueryConfiguration.queryGraphStats, QueryConfiguration.connectionString, 
											 QueryConfiguration.username, QueryConfiguration.password);
		System.out.println("Connected to Subsidies Statistics Graph!");
		
		/* Subsidies Graph */
		VirtGraph graphSubsidies = new VirtGraph(QueryConfiguration.queryGraphSubsidies, QueryConfiguration.connectionString, 
											 	 QueryConfiguration.username, QueryConfiguration.password);
		System.out.println("Connected to Subsidies Graph!");
		
		/* Geo Graph */
		VirtGraph graphGeo = new VirtGraph(QueryConfiguration.queryGraphGeo, QueryConfiguration.connectionString, 
										   QueryConfiguration.username, QueryConfiguration.password);
		System.out.println("Connected to Subsidies Statistics Graph!");
		
		/* Organizations Graph */
		VirtGraph graphOrganizations = new VirtGraph(QueryConfiguration.queryGraphOrganizations, QueryConfiguration.connectionString, 
												  QueryConfiguration.username, QueryConfiguration.password);
		System.out.println("Connected to Organizations Graph!");
		
		/* DiavgeiaII 2014 Graph */
		VirtGraph graphDiavgeiaII2014 = new VirtGraph(QueryConfiguration.queryGraphDiavgeiaII2014, QueryConfiguration.connectionString, 
												      QueryConfiguration.username, QueryConfiguration.password);
		System.out.println("Connected to DiavgeiaII 2014 Graph!");
		
		/* DiavgeiaII 2015 Graph */
		VirtGraph graphDiavgeiaII2015 = new VirtGraph(QueryConfiguration.queryGraphDiavgeiaII2015, QueryConfiguration.connectionString, 
												      QueryConfiguration.username, QueryConfiguration.password);
		System.out.println("Connected to DiavgeiaII 2015 Graph!");
		
		/* E-Procurement Graph */
		VirtGraph graphEproc = new VirtGraph (QueryConfiguration.queryGraphEproc, QueryConfiguration.connectionString, 
											  QueryConfiguration.username, QueryConfiguration.password);
		System.out.println("Connected to Eproc Graph!");
		
		Map<String, VirtGraph> graphs = new HashMap<String, VirtGraph>();
		graphs.put(QueryConfiguration.queryGraphDiavgeiaII2014, graphDiavgeiaII2014);
		graphs.put(QueryConfiguration.queryGraphDiavgeiaII2015, graphDiavgeiaII2015);
		
		/** Beneficiaries **/
		System.out.println("Querying Beneficiaries");
		ArrayList<Resource> beneficiariesList = qs.getOrgrsWithStats(graphStats, QueryConfiguration.lastUpdateDate);
		
		for (Resource beneficiaryUri : beneficiariesList) {
			
			/* initialize the JSON */
			String jsonString = "";
			
			/* start the 'couchDb' JSON */
			if (QueryConfiguration.couchDbUsage) {
				jsonString += "{\"docs\":";
			}
			
			/* start the 'normal' JSON */
			jsonString += "[{";
				
			/** create _id tag **/
			if (QueryConfiguration.couchDbUsage) {
				jsonString += "\"_id\": \"" + hm.urlProfilePage(beneficiaryUri.getURI(), "Beneficiary") + "\",";
			}
				
			System.out.println("Querying Beneficiary: " + beneficiaryUri.getURI());
			jsonString = qs.getBeneficiaryDetails(jsonString, graphOrganizations, graphSubsidies, beneficiaryUri.getURI());
				
			jsonString += "\"subsidiesDetails\": [";
			jsonString = qs.getBuyerCategoryStats(jsonString, graphStats, beneficiaryUri.getURI(), QueryConfiguration.lastUpdateDate);
			
			jsonString = jsonString.substring(0, jsonString.length() - 1) + "],";
			
			System.out.println("Querying Subsidy Items of: " + beneficiaryUri.getURI());
			jsonString = qs.getBeneficiarySubsidies(jsonString, graphSubsidies, graphGeo, beneficiaryUri.getURI());
			
			if (!beneficiaryUri.getURI().contains("tempId_")) {
				System.out.println("Discovering page kind of: " + beneficiaryUri.getURI());
				/* search in Diavgeia dataset */
				//as Buyer
				boolean asBuyerDiavgeia = false;
				for (String key: graphs.keySet()) {
					asBuyerDiavgeia = qs.isBeneficiaryInDiavgeia(key, graphs.get(key), beneficiaryUri.getURI(), "Buyer");
					if (asBuyerDiavgeia) {
						break; //don't search the other graph
					}
				}
				//as Seller
				boolean asSellerDiavgeia = false;
				for (String key: graphs.keySet()) {
					asSellerDiavgeia = qs.isBeneficiaryInDiavgeia(key, graphs.get(key), beneficiaryUri.getURI(), "Seller");
					if (asSellerDiavgeia) {
						break; //don't search the other graph
					}
				}
				
				/* search in Khmdhs dataset */
				//as Buyer
				boolean asBuyerEproc = qs.isBeneficiaryInEproc(graphEproc, beneficiaryUri.getURI(), "Buyer");
				//as Seller
				boolean asSellerEproc = qs.isBeneficiaryInEproc(graphEproc, beneficiaryUri.getURI(), "Seller");
				
				/* profile page kind */
				jsonString = qs.setBeneficiaryPageKind(jsonString, asBuyerDiavgeia, asSellerDiavgeia, asBuyerEproc, asSellerEproc);
			}
				
			/** About **/
			jsonString = qs.addAbout(jsonString, hm.getFormattedDateFromDateString(QueryConfiguration.lastUpdateDate));
				
			/* close the 'normal' JSON */
			jsonString += "}]";
			
			/* close the 'couchDb' JSON */
			if (QueryConfiguration.couchDbUsage) {
				jsonString += "}";
			}
				
			hm.writeJsonFile(jsonString, "beneficiaries", beneficiaryUri.getURI());
			
		}

		System.out.println("\nFinished!");
	}
	
}