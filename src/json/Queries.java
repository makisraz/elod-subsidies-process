package json;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import utils.TransliterationMethods;

import virtuoso.jena.driver.VirtGraph;
import virtuoso.jena.driver.VirtuosoQueryExecution;
import virtuoso.jena.driver.VirtuosoQueryExecutionFactory;

import com.hp.hpl.jena.query.QuerySolution;
import com.hp.hpl.jena.query.ResultSet;
import com.hp.hpl.jena.rdf.model.Literal;
import com.hp.hpl.jena.rdf.model.Resource;

public class Queries {
	
	private HelperMethods hm = new HelperMethods();
	private TransliterationMethods trans = new TransliterationMethods();
	
	/** Beneficiary info **/
	public String getBeneficiaryDetails(String jsonString, VirtGraph graphOrganizations, VirtGraph graphSubsidies, String beneficiaryUri) {
		
		boolean resultsFlag = false;
		
		if (!beneficiaryUri.contains("tempId_")) {
			Literal beneficiaryName = getBeneficiaryLegalName(graphOrganizations, beneficiaryUri.toString());
			
			String beneficiaryNameStr = "";
			
			if (beneficiaryName != null) {
				beneficiaryNameStr = hm.cleanInvalidCharsJsonData(beneficiaryName.getString()).trim();
			} else {
				Literal beneficiaryUnitName = getOrganizationUnitName(graphOrganizations, beneficiaryUri.toString());
				if (beneficiaryUnitName != null) {
					beneficiaryNameStr = hm.cleanInvalidCharsJsonData(beneficiaryUnitName.getString()).trim();
				} else {
					beneficiaryName = getBeneficiaryName(graphOrganizations, beneficiaryUri.toString());
					if (beneficiaryName != null) {
						beneficiaryNameStr = hm.cleanInvalidCharsJsonData(beneficiaryName.getString()).trim();
					} else {
						beneficiaryNameStr = "Το όνομα δεν έχει καταχωρηθεί";
					}
				}
			}
			
			String queryOrg = "PREFIX vcard: <http://www.w3.org/2006/vcard/ns#> " +
			  		  "PREFIX gr: <http://purl.org/goodrelations/v1#> " +
			  		  "PREFIX elod: <http://linkedeconomy.org/ontology#> " +
			  		  "SELECT ?afm ?validVatId ?addressRes ?address ?pc ?city ?translation " +
			  		  "FROM <" + QueryConfiguration.queryGraphOrganizations + "> " +
			  		  "WHERE { " +
			  		  "OPTIONAL { " +
			  		  	"<" + beneficiaryUri +"> gr:vatID ?afm . " +
			  		  "} . " +
				  	  "OPTIONAL { " +
			  		  	"<" + beneficiaryUri +"> elod:validVatId ?validVatId . " +
			  		  "} . " + 
			  		  "OPTIONAL { " +
				  		  "<" + beneficiaryUri +"> vcard:hasAddress ?addressRes . " +
				  		  "?addressRes vcard:postal-code ?pc ; " +
				  		  "vcard:street-address ?address; " +
				  		  "vcard:locality ?city ; " +
				  		  "vcard:country-name ?cn . " +
			  		  "} . " +
			  		  "OPTIONAL { " +
		  		  		"<" + beneficiaryUri +"> elod:translation ?translation . " +
		  		  	  "} . " +
					  "}";
	
			VirtuosoQueryExecution vqeOrg = VirtuosoQueryExecutionFactory.create(queryOrg, graphOrganizations);		
			ResultSet resultsOrg = vqeOrg.execSelect();
			
			jsonString += "\"beneficiaryDtls\": ";
			
			if (resultsOrg.hasNext()) {
				QuerySolution result = resultsOrg.nextSolution();
				Literal afm = result.getLiteral("afm");
				Resource addressRes = result.getResource("addressRes");
				Literal address = result.getLiteral("address");
				Literal pc = result.getLiteral("pc");
				Literal city = result.getLiteral("city");
				Literal translation = result.getLiteral("translation");
				Literal validVatId = result.getLiteral("validVatId");
				boolean validVat = false;
				if (validVatId != null) {
					validVat = hm.validVatIdToBoolean(validVatId.getString());
				}
				
				jsonString += "{\"beneficiary\": \"" + beneficiaryNameStr;
				
				if (translation != null) {
					jsonString += "\"," + "\"beneficiaryEng\": \"" + translation.getString().trim();
				} else {
					if (beneficiaryNameStr.equalsIgnoreCase("Το όνομα δεν έχει καταχωρηθεί")) {
						jsonString += "\"," + "\"beneficiaryEng\": \"" + "N/A";
					} else {
						jsonString += "\"," + "\"beneficiaryEng\": \"" + trans.transliterationGenerator(beneficiaryNameStr);
					}
				}
				
				if (validVat) {
					if (afm != null) {
						jsonString += "\"," + "\"vatId\": \"" + afm.getString() + 
									  "\"," + "\"vatIdEng\": \"" + afm.getString();
					}
				} else {
					if (afm != null) {
						jsonString += "\"," + "\"vatId\": \"" + "Μη έγκυρος ΑΦΜ" + 
									  "\"," + "\"vatIdEng\": \"" + "Invalid VAT ID";
					} else {
						jsonString += "\"," + "\"vatId\": \"" + "Ο ΑΦΜ δεν έχει καταχωρηθεί" + 
									  "\"," + "\"vatIdEng\": \"" + "N/A";
					}
				}
				
				if (validVat) {
					if (addressRes != null) {
						jsonString += "\"," + "\"address\": \"" + hm.cleanInvalidCharsJsonData(address.getString()) + 
									  "\"," + "\"addressEng\": \"" + trans.transliterationGenerator( hm.cleanInvalidCharsJsonData(address.getString()) ) + 
									  "\"," + "\"pc\": \"" + pc.getString() + "\"," + "\"pcEng\": \"" + pc.getString() + 
									  "\"," + "\"city\": \"" + hm.cleanInvalidCharsJsonData(city.getString()) + 
									  "\"," + "\"cityEng\": \"" + trans.transliterationGenerator( hm.cleanInvalidCharsJsonData(city.getString()) ) + "\"},";
					} else {
						jsonString += "\"," + "\"address\": \"" + "Μη Διαθέσιμο" + "\"," + "\"addressEng\": \"" + "N/A" + 
									  "\"," + "\"pc\": \"" + "Μη Διαθέσιμο" + "\"," + "\"pcEng\": \"" + "N/A" + 
									  "\"," + "\"city\": \"" + "Μη Διαθέσιμο" + "\"," + "\"cityEng\": \"" + "N/A" + "\"},";
					}
				} else {
						jsonString += "\"," + "\"address\": \"" + "Μη Διαθέσιμο" + "\"," + "\"addressEng\": \"" + "N/A" + 
								  	  "\"," + "\"pc\": \"" + "Μη Διαθέσιμο" + "\"," + "\"pcEng\": \"" + "N/A" + 
								  	  "\"," + "\"city\": \"" + "Μη Διαθέσιμο" + "\"," + "\"cityEng\": \"" + "N/A" + "\"},";
				}
				
				resultsFlag = true;
			}
			
			vqeOrg.close();
		} else {
			String name = getBeneficiarySubsidiesName(graphSubsidies, beneficiaryUri);
			
			jsonString += "\"beneficiaryDtls\": ";
			
			if (name != null) {
				jsonString += "{\"beneficiary\": \"" + name + "\"," + "\"beneficiaryEng\": \"" + 
							  trans.transliterationGenerator(hm.beneficiaryWithFatherName(name));
			} else {
				jsonString += "{\"beneficiary\": \"" + "Το όνομα του δικαιούχου δεν βρέθηκε" + 
							  "\"," + "\"beneficiaryEng\": \"" + "Beneficiary name is not found";
			}
			
			jsonString += "\"," + "\"vatId\": \"" + "Ο ΑΦΜ δεν έχει καταχωρηθεί" + 
					  	  "\"," + "\"vatIdEng\": \"" + "N/A";
			
			jsonString += "\"," + "\"address\": \"" + "Μη Διαθέσιμο" + "\"," + "\"addressEng\": \"" + "N/A" + 
				  	  	  "\"," + "\"pc\": \"" + "Μη Διαθέσιμο" + "\"," + "\"pcEng\": \"" + "N/A" + 
				  	  	  "\"," + "\"city\": \"" + "Μη Διαθέσιμο" + "\"," + "\"cityEng\": \"" + "N/A" + "\"},";
			
			resultsFlag = true;
		}
			
		if (resultsFlag) {
			jsonString = jsonString.substring(0, jsonString.length() - 1) + ",";
		} else {
			jsonString = jsonString.substring(0, jsonString.length()) + "{},";
		}
		
		return jsonString;
	}
	
	public String getBeneficiaryNameInfo(VirtGraph graphOrganizations, VirtGraph graphSubsidies, String beneficiaryUri) {
		
		String beneficiaryNameStr = "";
		
		if (!beneficiaryUri.contains("tempId_")) {
			Literal beneficiaryName = getBeneficiaryLegalName(graphOrganizations, beneficiaryUri.toString());
			
			if (beneficiaryName != null) {
				beneficiaryNameStr = hm.cleanInvalidCharsJsonData(beneficiaryName.getString());
			} else {
				Literal beneficiaryUnitName = getOrganizationUnitName(graphOrganizations, beneficiaryUri.toString());
				if (beneficiaryUnitName != null) {
					beneficiaryNameStr = hm.cleanInvalidCharsJsonData(beneficiaryUnitName.getString());
				} else {
					beneficiaryName = getBeneficiaryName(graphOrganizations, beneficiaryUri.toString());
					if (beneficiaryName != null) {
						beneficiaryNameStr = hm.cleanInvalidCharsJsonData(beneficiaryName.getString());
					} else {
						beneficiaryNameStr = "Το όνομα δεν έχει καταχωρηθεί";
					}
				}
			}
		} else {
			String bName = getBeneficiarySubsidiesName(graphSubsidies, beneficiaryUri);
			
			if (bName != null) {
				beneficiaryNameStr = bName;
			} else {
				beneficiaryNameStr = "Το όνομα δεν έχει καταχωρηθεί";
			}
		}
		
		return beneficiaryNameStr;
	}
	
	public String getBeneficiaryEnglishName(VirtGraph graphOrganizations, String beneficiaryUri) {
		
		String engName = null;
		
		if (!beneficiaryUri.contains("tempId_")) {
			
			String queryOrg = "PREFIX elod: <http://linkedeconomy.org/ontology#> " +
			  		  "SELECT ?translation " +
			  		  "FROM <" + QueryConfiguration.queryGraphOrganizations + "> " +
			  		  "WHERE { " +
		  		  	  "<" + beneficiaryUri +"> elod:translation ?translation . " +
					  "}";
	
			VirtuosoQueryExecution vqeOrg = VirtuosoQueryExecutionFactory.create(queryOrg, graphOrganizations);		
			ResultSet resultsOrg = vqeOrg.execSelect();
			
			if (resultsOrg.hasNext()) {
				QuerySolution result = resultsOrg.nextSolution();
				Literal translation = result.getLiteral("translation");
				
				if (translation != null) {
					engName = translation.getString();
				}
			}
			
			vqeOrg.close();
		}
		
		return engName;
	}
	
	/** queries regarding the names **/	
	public Literal getBeneficiaryLegalName(VirtGraph graphOrganizations, String beneficiaryUri) {
		
		Literal orgName = null;
		
		if (checkUri(beneficiaryUri)) {
			String queryName = "PREFIX gr: <http://purl.org/goodrelations/v1#> " +
			  		  "SELECT ?legalName " +
			  		  "FROM <" + QueryConfiguration.queryGraphOrganizations + "> " +
			  		  "WHERE { " +
			  		  "<" + beneficiaryUri +"> gr:legalName ?legalName . " +
			  		  "}";
	
			VirtuosoQueryExecution vqeName = VirtuosoQueryExecutionFactory.create(queryName, graphOrganizations);		
			ResultSet resultsName = vqeName.execSelect();
			
			while (resultsName.hasNext()) {
				QuerySolution result = resultsName.nextSolution();
				orgName = result.getLiteral("legalName");
			}
			
			vqeName.close();
		}
		
		return orgName;
	}
	
	public Literal getBeneficiaryName(VirtGraph graphOrganizations, String beneficiaryUri) {
		
		Literal orgName = null;
		
		if (checkUri(beneficiaryUri)) {
			String queryName = "PREFIX gr: <http://purl.org/goodrelations/v1#> " +
			  		  "SELECT ?name " +
			  		  "FROM <" + QueryConfiguration.queryGraphOrganizations + "> " +
			  		  "WHERE { " +
			  		  "<" + beneficiaryUri +"> gr:name ?name . " +
			  		  "}";
	
			VirtuosoQueryExecution vqeName = VirtuosoQueryExecutionFactory.create(queryName, graphOrganizations);		
			ResultSet resultsName = vqeName.execSelect();
			
			while (resultsName.hasNext()) {
				QuerySolution result = resultsName.nextSolution();
				orgName = result.getLiteral("name");
			}
			
			vqeName.close();
		}
		
		return orgName;
	}
	
	public Literal getOrganizationUnitName(VirtGraph graphOrganizations, String beneficiaryUnitUri) {
		
		Literal orgName = null;
		
		if (checkUri(beneficiaryUnitUri)) {
			String queryName = "SELECT ?name " +
				  		  "FROM <" + QueryConfiguration.queryGraphOrganizations + "> " +
				  		  "WHERE { " +
				  		  "<" + beneficiaryUnitUri +"> rdfs:label ?name . " +
				  		  "}";
	
			VirtuosoQueryExecution vqeName = VirtuosoQueryExecutionFactory.create(queryName, graphOrganizations);		
			ResultSet resultsName = vqeName.execSelect();
			
			while (resultsName.hasNext()) {
				QuerySolution result = resultsName.nextSolution();
				orgName = result.getLiteral("name");
			}
			
			vqeName.close();
		}
		
		return orgName;
	}
	
	public String getBeneficiarySubsidiesName(VirtGraph graphSubsidies, String beneficiaryUri) {
		
		String name = null;
		
		if (checkUri(beneficiaryUri)) {
			String queryOrg = "PREFIX gr: <http://purl.org/goodrelations/v1#> " +
			  		  "SELECT ?name " +
			  		  "FROM <" + QueryConfiguration.queryGraphSubsidies + "> " +
			  		  "WHERE { " +
			  		  "<" + beneficiaryUri +"> gr:name ?name " +
					  "}";
	
			VirtuosoQueryExecution vqeOrg = VirtuosoQueryExecutionFactory.create(queryOrg, graphSubsidies);		
			ResultSet resultsOrg = vqeOrg.execSelect();
			
			if (resultsOrg.hasNext()) {
				QuerySolution result = resultsOrg.nextSolution();
				name = result.getLiteral("name").getString();
			}
			
			vqeOrg.close();
		}
		
		return name;
	}
	
	public boolean checkUri(String uri) {
		
		boolean correctUriFlag = true;
		
		if (uri.equalsIgnoreCase("http://linkedeconomy.org/resource/Organization/")) {
			correctUriFlag = false;
		} else if (uri.equalsIgnoreCase("http://linkedeconomy.org/resource/OrganizationalUnit/")) {
			correctUriFlag = false;
		} else if (uri.equalsIgnoreCase("http://linkedeconomy.org/resource/Person/")) {
			correctUriFlag = false;
		} else if ( uri.equalsIgnoreCase("http://linkedeconomy.org/resource/Organization/..") || 
				    uri.equalsIgnoreCase("http://linkedeconomy.org/resource/Organization/.") ) {
			correctUriFlag = false;
		}
		
		if ( uri.contains(" ") || uri.contains("`") || uri.contains("\"") ) {
			correctUriFlag = false;
		}
		
		return correctUriFlag;
	}
	
	/** queries regarding the Statistics **/
	public ArrayList<Resource> getOrgrsWithStats(VirtGraph graphStats, String lastUpdateDate) {
		
		ArrayList<Resource> orgsList = new ArrayList<Resource>();
		
		String queryOrgs = "PREFIX elod: <http://linkedeconomy.org/ontology#> " +
					"SELECT DISTINCT ?org " +
					"FROM <" + QueryConfiguration.queryGraphStats + "> " +
					"WHERE { " +
					"?statistic elod:isStatisticOf ?org ; " +
							   "elod:lastUpdateDate ?lastUpdateDate . " +
					"FILTER (?lastUpdateDate = \"" + lastUpdateDate + "\"^^xsd:date) . " +
					//"FILTER (!regex(?org, \"tempId_\")) . " +
					//"FILTER (regex(?org, \"082785679\")) . " +
					"}";

		VirtuosoQueryExecution vqeOrgs = VirtuosoQueryExecutionFactory.create(queryOrgs, graphStats);		
		ResultSet resultsOrgs = vqeOrgs.execSelect();
		
		while (resultsOrgs.hasNext()) {
			QuerySolution result = resultsOrgs.nextSolution();
			orgsList.add(result.getResource("org"));
		}
		
		vqeOrgs.close();
		
		return orgsList;
	}
	
	public String getBuyerCategoryStats(String jsonString, VirtGraph graphStats, String beneficiaryUri, String lastUpdateDate) {
		
		List<String> categories = Arrays.asList("Budget", "Contract", "Payment");
		Map<String, String[]> dtls = new LinkedHashMap<String, String[]>();
		
		for (String category : categories) {
			String amountCategory = null;
			String counterCategoryStr = null;
			String rankCategory = null;
			
			String queryStats = "PREFIX elod: <http://linkedeconomy.org/ontology#> " +
					"SELECT ?statistic ?aggregateRes (xsd:decimal(?amount) as ?amount) ?counterRes ?counter " +
					"?rankRes ?rank ?category ?activity " +
					"FROM <" + QueryConfiguration.queryGraphStats + "> " +
					"WHERE { " +
					"?statistic elod:isStatisticOf <" + beneficiaryUri + "> ; " +
							   "elod:lastUpdateDate ?lastUpdateDate ; " +
							   "elod:hasAggregate ?aggregateRes ; " +
							   "elod:hasCounter ?counterRes ; " +
							   "elod:hasRank ?rankRes ; " +
							   "elod:orgActivity ?activity . " +
					"?aggregateRes elod:hasCategory ?category ; " +
								  "elod:aggregatedAmount ?amount . " +
					"?counterRes elod:hasCategory ?category ; " +
								"elod:counter ?counter . " +
					"?rankRes elod:hasCategory ?category ; " +
							 "elod:rank ?rank . " +
					"FILTER (?lastUpdateDate = \"" + lastUpdateDate + "\"^^xsd:date) . " +
					"FILTER (?category = <http://linkedeconomy.org/ontology#" + category + ">) . " +
					"FILTER (?activity = \"Beneficiary\"^^xsd:string) . " +
					"}";

			VirtuosoQueryExecution vqeStats = VirtuosoQueryExecutionFactory.create(queryStats, graphStats);
			ResultSet resultsStats = vqeStats.execSelect();
			
			if (resultsStats.hasNext()) {
				QuerySolution result = resultsStats.nextSolution();
				counterCategoryStr = result.getLiteral("counter").getString();
				amountCategory = hm.convertAmount(result.getLiteral("amount"));
				rankCategory = result.getLiteral("rank").getString();
			}
		
			vqeStats.close();
		
			if (rankCategory.equalsIgnoreCase("-1")) {
				rankCategory = "-";
			}
			
			dtls.put( category, new String[] {amountCategory, counterCategoryStr, rankCategory} );
			
		}
		
		for (String key: dtls.keySet()) {
			jsonString += "{\"category\": \"" + key + "\"," + "\"amount\": \"€" + dtls.get(key)[0] + "\"," + 
						  "\"counter\": \"" + dtls.get(key)[1] + "\"," + "\"rank\": \"" + dtls.get(key)[2] + "\"},";
		}
		
		jsonString = jsonString.substring(0, jsonString.length() - 1)+ "}";

		return jsonString;		
	}
	
	public String getBeneficiarySubsidies(String jsonString, VirtGraph graphSubsidies, VirtGraph graphGeo, String beneficiaryUri) {
		
		boolean resultsFlag = false;
		
		String querySubsidies = "PREFIX pc: <http://purl.org/procurement/public-contracts#> " +
				"PREFIX elodGeo: <http://linkedeconomy.org/geoOntology#> " +
				"PREFIX elod: <http://linkedeconomy.org/ontology#> " +
				"PREFIX gr: <http://purl.org/goodrelations/v1#> " +
				"PREFIX dc: <http://purl.org/dc/elements/1.1/> " +
 			  	"SELECT ?subsidy ?subsidyMunicipality ?subject ?prjStatusLabel ?relatedBudgetItem (xsd:decimal(?biAmount) AS ?biAmount) " +
 			  		    "?relatedContract (xsd:decimal(?cntrAmount) AS ?cntrAmount) ?relatedSpendingItem (xsd:decimal(?siAmount) AS ?siAmount) " +
 			  	"FROM <" + QueryConfiguration.queryGraphSubsidies + "> " +
 			  	"FROM <" + QueryConfiguration.queryGraphGeo + "> " +
 			  	"WHERE { " +
 			  	"?subsidy rdf:type elod:Subsidy ; " +
 			  			 "elod:beneficiary <" + beneficiaryUri + "> ; " +
 			  			 "dc:subject ?subject ; " +
 			  			 "elod:subsidyMunicipality ?subsidyMunicipality ; " +
						 "elod:projectStatus ?projectStatus ; " +
						 "elod:hasRelatedBudgetItem ?relatedBudgetItem ; " +
						 "elod:hasRelatedContract ?relatedContract ; " +
						 "elod:hasRelatedSpendingItem ?relatedSpendingItem . " +
				"?projectStatus skos:prefLabel ?prjStatusLabel . " +
				"?relatedBudgetItem elod:price ?biUps . " +
				"?biUps gr:hasCurrencyValue ?biAmount . " +
				"?relatedContract pc:agreedPrice ?cntrUps . " +
				"?cntrUps gr:hasCurrencyValue ?cntrAmount . " +
				"?relatedSpendingItem elod:amount ?siUps . " +
				"?siUps gr:hasCurrencyValue ?siAmount . " +
				"FILTER(LANG(?prjStatusLabel) = \"el\") . " +
				"}";

		VirtuosoQueryExecution vqeSubsidies = VirtuosoQueryExecutionFactory.create(querySubsidies, graphSubsidies);
		ResultSet resultsSubsidies = vqeSubsidies.execSelect();		
		
		jsonString += "\"subsidyItems\": [";
		
		while (resultsSubsidies.hasNext()) {
			QuerySolution result = resultsSubsidies.nextSolution();
			System.out.println(result.getResource("subsidy"));
			String prjStatus = result.getLiteral("prjStatusLabel").getString();
			String biAmount = hm.roundAmountNoDecimals(result.getLiteral("biAmount"));
			String cntrAmount = hm.roundAmountNoDecimals(result.getLiteral("cntrAmount"));
			String siAmount = hm.roundAmountNoDecimals(result.getLiteral("siAmount"));
			String municipalityUri = result.getResource("subsidyMunicipality").getURI();
			String[] muniName = getMunicipalityNames(graphGeo, municipalityUri);
			
			String subject = null;
			try { //ignore blank nodes(!!!)
				subject = result.getLiteral("subject").getString();
			} catch (Exception e) {
				subject = "-";
			}
			
			jsonString += "{\"subject\": \"" + hm.cleanInvalidCharsJsonData(subject) + "\"," + 
						  "\"municipality\": \"" + muniName[0] + "\"," + "\"municipalityEng\": \"" + 
						  muniName[1] + "\","+ "\"projectStatus\": \"" + prjStatus + "\"," + 
						  "\"projectStatusEng\": \"" + hm.findEngProjectStatus(prjStatus) + "\"," + 
						  "\"budgetAmount\": \"" + biAmount + "\"," + "\"contractAmount\": \"" + 
						  cntrAmount + "\"," + "\"paymentAmount\": \"" + siAmount + "\"},";
			
			resultsFlag = true;
		}
		
		vqeSubsidies.close();
		
		if (resultsFlag) {
			jsonString = jsonString.substring(0, jsonString.length() - 1) + "],";
		} else {
			jsonString = jsonString.substring(0, jsonString.length()) + "],";
		}
		
		return jsonString;
	}
	
	/* beneficiary page kind */
	public boolean isBeneficiaryInDiavgeia(String graphName, VirtGraph graphToSearch, String beneficiaryUri, String buyerOrSeller) {
		
		String property = null;
		boolean beneficiaryInDiavgeia = false;
		
		if (buyerOrSeller.equalsIgnoreCase("Buyer")) {
			property = "buyer";
		} else {
			property = "seller";
		}
		
		String queryActor = "PREFIX elod: <http://linkedeconomy.org/ontology#> " +
 			  	"SELECT ?decision " +
 			  	"FROM <" + graphName + "> " +
 			  	"WHERE { " +
 			  	"?decision elod:" + property + " <" + beneficiaryUri + "> . " +
				"} " +
				"LIMIT 10";

		VirtuosoQueryExecution vqeActor = VirtuosoQueryExecutionFactory.create(queryActor, graphToSearch);
		ResultSet resultsActor = vqeActor.execSelect();		
		
		while (resultsActor.hasNext()) {			
			beneficiaryInDiavgeia = true;
			break;
		}
		
		vqeActor.close();
		
		return beneficiaryInDiavgeia;
	}
	
	public boolean isBeneficiaryInEproc(VirtGraph graphEproc, String beneficiaryUri, String buyerOrSeller) {
		
		String property = null;
		boolean beneficiaryInEproc = false;
		
		if (buyerOrSeller.equalsIgnoreCase("Buyer")) {
			property = "buyer";
		} else {
			property = "seller";
		}
		
		String queryActor = "PREFIX elod: <http://linkedeconomy.org/ontology#> " +
 			  	"SELECT ?decision " +
 			  	"FROM <" + QueryConfiguration.queryGraphEproc + "> " +
 			  	"WHERE { " +
 			  	"?decision elod:" + property + " <" + beneficiaryUri + "> . " +
				"} " +
				"LIMIT 10";

		VirtuosoQueryExecution vqeActor = VirtuosoQueryExecutionFactory.create(queryActor, graphEproc);
		ResultSet resultsActor = vqeActor.execSelect();		
		
		while (resultsActor.hasNext()) {			
			beneficiaryInEproc = true;
			break;
		}
		
		vqeActor.close();
		
		return beneficiaryInEproc;
	}
	
	public String setBeneficiaryPageKind(String jsonString, boolean asBuyerDiavgeia, boolean asSellerDiavgeia, 
										 boolean asBuyerEproc, boolean asSellerEproc) {
		
		String kind = null;
		
		if (asBuyerDiavgeia && asBuyerEproc) {
			kind = "buyerBoth";
		} else if (asSellerDiavgeia && asSellerEproc) {
			kind = "sellerBoth";
		} else if (asBuyerEproc && asSellerEproc) {
			kind = "sellerKhmdhs";
		} else if (asBuyerDiavgeia && asSellerDiavgeia) {
			kind = "buyerDiavgeia";
		} else if (asBuyerDiavgeia) {
			kind = "buyerDiavgeia";
		} else if (asSellerDiavgeia) {
			kind = "sellerDiavgeia";
		} else if (asBuyerEproc) {
			kind = "buyerKhmdhs";
		} else if (asSellerEproc) {
			kind = "sellerKhmdhs";
		} else {
			kind = "general";
		}
		
		jsonString += "\"pageKind\": \"" + kind + "\",";
		
		return jsonString;
	}
	
	/** Municipality details **/
	private String[] getMunicipalityNames(VirtGraph graphGeo, String municipalityUri) {
		
		String[] names = new String[2];
		
		String queryName = "PREFIX elodGeo: <http://linkedeconomy.org/geoOntology#> " +
 			  	"SELECT ?muniName " +
 			  	"FROM <" + QueryConfiguration.queryGraphGeo + "> " +
 			  	"WHERE { " +
 			  	"<" + municipalityUri + "> elodGeo:name ?muniName " +
				"}";

		VirtuosoQueryExecution vqeName = VirtuosoQueryExecutionFactory.create(queryName, graphGeo);
		ResultSet resultsName = vqeName.execSelect();		
		
		while (resultsName.hasNext()) {
			QuerySolution result = resultsName.nextSolution();
			String muniName = result.getLiteral("muniName").getString();
			if (muniName.contains("@el")) {
				names[0] = muniName.split("@")[0]; //Due to error in municipality name
			} else {
				names[1] = muniName.split("@")[0]; //Due to error in municipality name
			}
		}
		
		vqeName.close();
		
		return names;
	}
	
	/** Συνολική εικόνα **/	
	public String getTotalBeneficieries(VirtGraph graphSubsidies) {
		
		int totalBeneficiers = 0;
		
		//step 1: matched -> count distinct beneficiaries
		String queryNum = "PREFIX elod: <http://linkedeconomy.org/ontology#> " +
 			  	"SELECT (COUNT(DISTINCT ?beneficiary) AS ?beneficiaries) " +
 			  	"FROM <" + QueryConfiguration.queryGraphSubsidies + "> " +
 			  	"WHERE { " +
 			  	"?subsidy elod:beneficiary ?beneficiary . " +
 			  	"FILTER (!(regex(?beneficiary, \"tempId_\"))) . " +
				"}";

		VirtuosoQueryExecution vqeNum = VirtuosoQueryExecutionFactory.create(queryNum, graphSubsidies);
		ResultSet resultsNum = vqeNum.execSelect();		
		
		if (resultsNum.hasNext()) {
			QuerySolution result = resultsNum.nextSolution();
			totalBeneficiers = result.getLiteral("beneficiaries").getInt();
		}
		
		vqeNum.close();
		
		//step 2: unmatched -> count distinct names
		queryNum = "PREFIX gr: <http://purl.org/goodrelations/v1#> " +
				"PREFIX elod: <http://linkedeconomy.org/ontology#> " +
 			  	"SELECT (COUNT(DISTINCT ?name) AS ?beneficiaries) " +
 			  	"FROM <" + QueryConfiguration.queryGraphSubsidies + "> " +
 			  	"WHERE { " +
 			  	"?subsidy elod:beneficiary ?beneficiary . " +
 			  	"?beneficiary gr:name ?name . " +
 			  	"FILTER (regex(?beneficiary, \"tempId_\")) . " +
				"}";

		vqeNum = VirtuosoQueryExecutionFactory.create(queryNum, graphSubsidies);
		resultsNum = vqeNum.execSelect();		
		
		if (resultsNum.hasNext()) {
			QuerySolution result = resultsNum.nextSolution();
			totalBeneficiers += result.getLiteral("beneficiaries").getInt();
		}
		
		vqeNum.close();
		
		return String.valueOf(totalBeneficiers);
	}
	
	public String[] getSubsidiesTotalSums(VirtGraph graphSubsidies) {
		
		String[] sums = null;
		
		String querySums = "PREFIX gr: <http://purl.org/goodrelations/v1#> " +
				"PREFIX elod: <http://linkedeconomy.org/ontology#> " +
				"PREFIX pc: <http://purl.org/procurement/public-contracts#> " +
 			  	"SELECT (COUNT(?subsidy) AS ?subsidies) (SUM(xsd:decimal(?biAmount)) AS ?biAmount) " +
 			  		   "(SUM(xsd:decimal(?cntrAmount)) AS ?cntrAmount) (SUM(xsd:decimal(?siAmount)) AS ?siAmount) " +
 			  	"FROM <" + QueryConfiguration.queryGraphSubsidies + "> " +
 			  	"WHERE { " +
 			  	"?subsidy elod:hasRelatedBudgetItem ?budgetItem ; " +
				         "elod:hasRelatedContract ?contract ; " +
				         "elod:hasRelatedSpendingItem ?spendingItem ; " +
				         "elod:beneficiary ?beneficiary . " +
				"?budgetItem elod:price ?biUps . " +
				"?biUps gr:hasCurrencyValue ?biAmount . " +
				"?contract pc:agreedPrice ?cntrUps . " +
				"?cntrUps gr:hasCurrencyValue ?cntrAmount. " +
				"?spendingItem elod:amount ?siUps . " +
				"?siUps gr:hasCurrencyValue ?siAmount . " +
				"}";

		VirtuosoQueryExecution vqeSums = VirtuosoQueryExecutionFactory.create(querySums, graphSubsidies);
		ResultSet resultsSums = vqeSums.execSelect();		
		
		if (resultsSums.hasNext()) {
			QuerySolution result = resultsSums.nextSolution();
			String subsidies = result.getLiteral("subsidies").getString();
			String biAmount = hm.roundAmountNoDecimals(result.getLiteral("biAmount"));
			String cntrAmount = hm.roundAmountNoDecimals(result.getLiteral("cntrAmount"));
			String siAmount = hm.roundAmountNoDecimals(result.getLiteral("siAmount"));
			sums = new String[] {subsidies, biAmount, cntrAmount, siAmount};
		}
		
		vqeSums.close();
		
		return sums;
	}
	
	public String getTotalBeneficiariesAndSellers(VirtGraph graphSubsidies, VirtGraph graphDiavgeia2014, VirtGraph graphDiavgeia2015, VirtGraph graphEproc) {
		
		int totalBeneficiersAndSellers = 0;
		
		String queryNum = "PREFIX elod: <http://linkedeconomy.org/ontology#> " +
 			  	"SELECT (COUNT(DISTINCT ?beneficiary) AS ?beneficiariesAndSellers) " +
 			  	"FROM <" + QueryConfiguration.queryGraphSubsidies + "> " +
 			  	"FROM <" + QueryConfiguration.queryGraphDiavgeiaII2014 + "> " +
 			  	"FROM <" + QueryConfiguration.queryGraphDiavgeiaII2015 + "> " +
 			  	"FROM <" + QueryConfiguration.queryGraphEproc + "> " +
 			  	"WHERE { " +
 			  	"?subsidy elod:beneficiary ?beneficiary ;  " +
				         "rdf:type elod:Subsidy . " +
				"?praxi elod:seller ?beneficiary . " +
				"}";

		VirtuosoQueryExecution vqeNum = VirtuosoQueryExecutionFactory.create(queryNum, graphSubsidies);
		ResultSet resultsNum = vqeNum.execSelect();		
		
		if (resultsNum.hasNext()) {
			QuerySolution result = resultsNum.nextSolution();
			totalBeneficiersAndSellers = result.getLiteral("beneficiariesAndSellers").getInt();
		}
		
		vqeNum.close();
		
		return String.valueOf(totalBeneficiersAndSellers);
	}
	
	/** queries regarding Δικαιούχοι ΕΣΠΑ **/
	public ArrayList<Resource> getBeneficiariesStatsTables(VirtGraph graphSubsidies, boolean allBeneficiaries) {
		
		ArrayList<Resource> orgsList = new ArrayList<Resource>();
		
		String filterPart = null;
		if (allBeneficiaries) {
			filterPart = "";
		} else { //matched only
			filterPart = "FILTER (!regex(?beneficiary, \"tempId_\")) . ";
		}
		
		String queryOrgs = "PREFIX gr: <http://purl.org/goodrelations/v1#> " +
					"PREFIX elod: <http://linkedeconomy.org/ontology#> " +
					"SELECT ?beneficiary (SUM(xsd:decimal(?biAmount)) AS ?biAmount) " +
					"FROM <" + QueryConfiguration.queryGraphSubsidies + "> " +
					"WHERE { " +
					"?subsidy elod:hasRelatedBudgetItem ?budgetItem ; " +
					         "elod:hasRelatedContract ?contract ; " +
					         "elod:hasRelatedSpendingItem ?spendingItem ; " +
					         "elod:beneficiary ?beneficiary . " +
					"?budgetItem elod:price ?biUps . " +
					"?biUps gr:hasCurrencyValue ?biAmount . " +
					filterPart +
					"} " +
					"GROUP BY ?beneficiary " +
					"ORDER BY DESC (?biAmount) " +
					"LIMIT 2000";

		VirtuosoQueryExecution vqeOrgs = VirtuosoQueryExecutionFactory.create(queryOrgs, graphSubsidies);
		ResultSet resultsOrgs = vqeOrgs.execSelect();		
		
		while (resultsOrgs.hasNext()) {
			QuerySolution result = resultsOrgs.nextSolution();
			orgsList.add(result.getResource("beneficiary"));
		}
		
		vqeOrgs.close(); 
		
		return orgsList;
	}
	
	public String[] getBeneficiarySumsAndCounter(VirtGraph graphSubsidies, String beneficiaryUri) {
		
		String counter = null;
		String biAmount = null;
		String cntrAmount = null;
		String siAmount = null;
		
		String querySumCounter = "PREFIX gr: <http://purl.org/goodrelations/v1#> " +
					"PREFIX elod: <http://linkedeconomy.org/ontology#> " +
					"PREFIX pc: <http://purl.org/procurement/public-contracts#> " +
					"SELECT (COUNT(?subsidy) AS ?subsidyCounter) (SUM(xsd:decimal(?biAmount)) AS ?biAmount) " +
						   "(SUM(xsd:decimal(?cntrAmount)) AS ?cntrAmount) (SUM(xsd:decimal(?siAmount)) AS ?siAmount) " +
					"FROM <" + QueryConfiguration.queryGraphSubsidies + "> " +
					"WHERE { " +
					"?subsidy elod:hasRelatedBudgetItem ?budgetItem ; " +
					         "elod:hasRelatedContract ?contract ; " +
					         "elod:hasRelatedSpendingItem ?spendingItem ; " +
					         "elod:beneficiary <" + beneficiaryUri + "> . " +
					"?budgetItem elod:price ?biUps . " +
					"?biUps gr:hasCurrencyValue ?biAmount . " +
					"?contract pc:agreedPrice ?cntrUps . " +
					"?cntrUps gr:hasCurrencyValue ?cntrAmount. " +
					"?spendingItem elod:amount ?siUps . " +
					"?siUps gr:hasCurrencyValue ?siAmount . " +
					"}";

		VirtuosoQueryExecution vqeSumCounter = VirtuosoQueryExecutionFactory.create(querySumCounter, graphSubsidies);
		ResultSet resultsSumCounter = vqeSumCounter.execSelect();		
		
		if (resultsSumCounter.hasNext()) {
			QuerySolution result = resultsSumCounter.nextSolution();
			counter = result.getLiteral("subsidyCounter").getString();
			biAmount = hm.roundAmountNoDecimals(result.getLiteral("biAmount"));
			cntrAmount = hm.roundAmountNoDecimals(result.getLiteral("cntrAmount"));
			siAmount = hm.roundAmountNoDecimals(result.getLiteral("siAmount"));
		}
		
		vqeSumCounter.close(); 
		
		return new String[]{counter, biAmount, cntrAmount, siAmount};
	}
	
	public String getBeneficiaryMunicipalities(VirtGraph graphSubsidies, VirtGraph graphGeo, String beneficiaryUri, String lang) {
		
		String muniName = null;
		
		String querySumCounter = "PREFIX elod: <http://linkedeconomy.org/ontology#> " +
					"SELECT DISTINCT ?muniName " +
					"FROM <" + QueryConfiguration.queryGraphSubsidies + "> " +
					"FROM <" + QueryConfiguration.queryGraphGeo + "> " +
					"WHERE { " +
					"?subsidy rdf:type elod:Subsidy ;" +
 			  			 "elod:beneficiary <" + beneficiaryUri + "> ; " +
 			  			 "elod:subsidyMunicipality ?subsidyMunicipality . " +
 			  		"?subsidyMunicipality elodGeo:name ?muniName . " +
 					"FILTER regex(?muniName, \"@" + lang + "\") . " +
					"}";

		VirtuosoQueryExecution vqeSumCounter = VirtuosoQueryExecutionFactory.create(querySumCounter, graphSubsidies);
		ResultSet resultsSumCounter = vqeSumCounter.execSelect();		
		
		while (resultsSumCounter.hasNext()) {
			QuerySolution result = resultsSumCounter.nextSolution();
			muniName = result.getLiteral("muniName").getString().split("@")[0] + ", "; //Due to error in municipality name
		}
		
		vqeSumCounter.close();
		
		muniName = muniName.substring(0, muniName.length() - 2);
		
		return muniName;
	}
	
	/** Δικαιούχοι ΕΣΠΑ και Ανάδοχοι Δημοσίου **/
	public String getDiavgeiaTotalPaymentsOfBeneficiary(String graphName, VirtGraph graphToSearch, String beneficiaryUri, String year) {
		
		String totalAmount = "0";
		
		String dateFilterFrom = null;
		
		if (year.equals("2014")) {
			dateFilterFrom = year + "-06-10";
		} else {
			dateFilterFrom = year + "-01-01";
		}
		
		String queryAmount = "PREFIX elod: <http://linkedeconomy.org/ontology#> " +
					"PREFIX dcTerms: <http://purl.org/dc/terms/> " +
					"PREFIX gr: <http://purl.org/goodrelations/v1#> " +
					"SELECT (SUM(xsd:decimal(?amount)) AS ?paymentAmount) " +
					"FROM <" + graphName + "> " +
					"WHERE { " +
					"?spendingItem elod:hasExpenditureLine ?expLine ; " +
					              "dcTerms:issued ?date ; " +
					              "rdf:type elod:SpendingItem . " +
					"?expLine elod:amount ?ups ; " +
					         "elod:seller <" + beneficiaryUri + "> . " +
					"?ups gr:hasCurrencyValue ?amount . " +
					"FILTER (?date > \"" + dateFilterFrom + "T00:00:00Z\"^^xsd:dateTime) . " +
		  			"FILTER (?date < \"" + String.valueOf(Integer.valueOf(year) + 1) + "-01-01T00:00:00Z\"^^xsd:dateTime) . " +
		  			"FILTER NOT EXISTS {?spendingItem elod:hasCorrectedDecision ?correctedDecision} . " +
					"}";

		VirtuosoQueryExecution vqeAmount = VirtuosoQueryExecutionFactory.create(queryAmount, graphToSearch);		
		ResultSet resultsAmount = vqeAmount.execSelect();
		
		if (resultsAmount.hasNext()) {
			QuerySolution result = resultsAmount.nextSolution();
			totalAmount = hm.roundAmountNoDecimals(result.getLiteral("paymentAmount"));
		}
		
		vqeAmount.close();
		
		return totalAmount;
	}
	
	public String getKhmdhsTotalPaymentsOfBeneficiary(VirtGraph graphEproc, String beneficiaryUri) {
		
		String totalAmount = "0";
		
		String queryPayments = "PREFIX elod: <http://linkedeconomy.org/ontology#> " +
				"PREFIX gr: <http://purl.org/goodrelations/v1#> " +
 			  	"SELECT (SUM(xsd:decimal((1+?vat/100)*?amount)) AS ?amountReceived) " +
				"FROM <" + QueryConfiguration.queryGraphEproc + "> " +
				"WHERE { " +
				"?payment elod:hasExpenditureLine ?expLine ; " +
						 "elod:hasRelatedContract ?relatedContract . " +
				"?expLine elod:seller <" + beneficiaryUri +"> ; " +
		            	 "elod:amount ?ups . " +
		        "?ups gr:hasCurrencyValue ?amount ; " +
		        	 "elod:hasVat ?vatRes . " +
		        "?vatRes elod:vatPercentage ?vat . " +
				"FILTER NOT EXISTS {?correctedDec elod:hasCorrectedDecision ?relatedContract} . " +
				"}";

		VirtuosoQueryExecution vqePayments = VirtuosoQueryExecutionFactory.create(queryPayments, graphEproc);		
		ResultSet resultsPayments = vqePayments.execSelect();
		
		if (resultsPayments.hasNext()) {
			QuerySolution result = resultsPayments.nextSolution();
			totalAmount = hm.roundAmountNoDecimals(result.getLiteral("amountReceived"));
		}
		
		vqePayments.close();
		
		return totalAmount;
	}
	
	/** Επιδοτήσεις ΕΣΠΑ **/
	public String getTopSubsidies(String jsonString, VirtGraph graphSubsidies, VirtGraph graphOrganizations, VirtGraph graphGeo) {
		
		boolean resultsFlag = false;
		
		String querySubsidies = "PREFIX pc: <http://purl.org/procurement/public-contracts#> " +
				"PREFIX elodGeo: <http://linkedeconomy.org/geoOntology#> " +
				"PREFIX elod: <http://linkedeconomy.org/ontology#> " +
				"PREFIX gr: <http://purl.org/goodrelations/v1#> " +
				"PREFIX dc: <http://purl.org/dc/elements/1.1/> " +
 			  	"SELECT ?subsidy ?beneficiary ?subject ?muniNameEl ?muniNameEn ?prjStatusLabelEl ?prjStatusLabelEn " +
 			  		   "?relatedBudgetItem (xsd:decimal(?biAmount) AS ?biAmount) ?relatedContract (xsd:decimal(?cntrAmount) AS ?cntrAmount) " +
 			  		   "?relatedSpendingItem (xsd:decimal(?siAmount) AS ?siAmount) " +
 			  	"FROM <" + QueryConfiguration.queryGraphSubsidies + "> " +
 			  	"FROM <" + QueryConfiguration.queryGraphGeo + "> " +
 			  	"WHERE { " +
 			  	"?subsidy rdf:type elod:Subsidy ; " +
 			  			 "elod:beneficiary ?beneficiary ; " +
 			  			 "dc:subject ?subject ; " +
 			  			 "elod:subsidyMunicipality ?subsidyMunicipality ; " +
						 "elod:projectStatus ?projectStatus ; " +
						 "elod:hasRelatedBudgetItem ?relatedBudgetItem ; " +
						 "elod:hasRelatedContract ?relatedContract ; " +
						 "elod:hasRelatedSpendingItem ?relatedSpendingItem . " +
				"?projectStatus skos:prefLabel ?prjStatusLabelEl ; " +
							   "skos:prefLabel ?prjStatusLabelEn . " +
				"?relatedBudgetItem elod:price ?biUps . " +
				"?biUps gr:hasCurrencyValue ?biAmount . " +
				"?relatedContract pc:agreedPrice ?cntrUps . " +
				"?cntrUps gr:hasCurrencyValue ?cntrAmount . " +
				"?relatedSpendingItem elod:amount ?siUps . " +
				"?siUps gr:hasCurrencyValue ?siAmount . " +
				"?subsidyMunicipality elodGeo:name ?muniNameEl ; " +
									 "elodGeo:name ?muniNameEn . " +
				"FILTER(LANG(?prjStatusLabelEl) = \"el\") . " +
				"FILTER(LANG(?prjStatusLabelEn) = \"en\") . " +
				"FILTER regex(?muniNameEl, \"@el\") . " +
				"FILTER regex(?muniNameEn, \"@en\") . " +
				"} " +
				"ORDER BY DESC (?biAmount) " +
				"LIMIT 2000";

		VirtuosoQueryExecution vqeSubsidies = VirtuosoQueryExecutionFactory.create(querySubsidies, graphSubsidies);
		ResultSet resultsSubsidies = vqeSubsidies.execSelect();		
		
		while (resultsSubsidies.hasNext()) {
			QuerySolution result = resultsSubsidies.nextSolution();
			System.out.println(result.getResource("subsidy"));
			String muniNameEl = result.getLiteral("muniNameEl").getString().split("@")[0]; //Due to error in municipality name
			String muniNameEn = result.getLiteral("muniNameEn").getString().split("@")[0]; //Due to error in municipality name
			String prjStatusEl = result.getLiteral("prjStatusLabelEl").getString();
			String prjStatusEn = result.getLiteral("prjStatusLabelEn").getString();
			String biAmount = hm.roundAmountNoDecimals(result.getLiteral("biAmount"));
			String cntrAmount = hm.roundAmountNoDecimals(result.getLiteral("cntrAmount"));
			String siAmount = hm.roundAmountNoDecimals(result.getLiteral("siAmount"));
			Resource beneficiaryUri = result.getResource("beneficiary");
			
			String benName = getBeneficiaryNameInfo(graphOrganizations, graphSubsidies, beneficiaryUri.getURI());
			String translation = getBeneficiaryEnglishName(graphOrganizations, beneficiaryUri.getURI());
			if (translation == null) {
				translation = trans.transliterationGenerator(benName);
			}
			
			String subject = null;
			try { //ignore blank nodes(!!!)
				subject = result.getLiteral("subject").getString();
			} catch (Exception e) {
				subject = "-";
			}
			
			jsonString += "{\"subject\": \"" + hm.cleanInvalidCharsJsonData(subject) + "\"," + "\"beneficiary\": \"" + 
						  "<a href=\\\"" + hm.urlProfilePage(beneficiaryUri.getURI(), "Beneficiary") + "\\\" target=\\\"_blank\\\">" + 
						  benName + "</a>\"," + "\"beneficiaryEng\": \"" + "<a href=\\\"" + hm.urlProfilePage(beneficiaryUri.getURI(), "Beneficiary") + 
						  "\\\" target=\\\"_blank\\\">" + translation + "</a>\"," +"\"municipality\": \"" + muniNameEl + "\"," +
						  "\"municipalityEng\": \"" + muniNameEn + "\"," + "\"projectStatusEl\": \"" + prjStatusEl + "\"," + 
						  "\"projectStatusEng\": \"" + prjStatusEn + "\"," + "\"budgetAmount\": \"" + biAmount + "\"," + 
						  "\"contractAmount\": \"" + cntrAmount + "\"," + "\"paymentAmount\": \"" + siAmount + "\"},";
			
			resultsFlag = true;
		}
		
		vqeSubsidies.close();
		
		if (resultsFlag) {
			jsonString = jsonString.substring(0, jsonString.length() - 1);
		} else {
			jsonString = "{}";
		}
		
		return jsonString;
	}
	
	/** About **/
	public String addAbout(String jsonString, String dataDateUpdated) {
		
		jsonString += "\"about\": ";
		
		jsonString += "{\"lastUpdate\": \"" + dataDateUpdated + "\",";
		
		jsonString += "\"sources\": \"<a href=\\\"http://www.anaptyxi.gov.gr\\\">ΑΝΑΠΤΥΞΗ.gov.gr</a>";
		
		jsonString += "\"," + "\"termOfUse\": \"<a href=\\\"http://creativecommons.org/licenses/by/4.0/deed.el\\\">" + 
					  "Ελληνική άδεια Creative Commons Αναφορά Προέλευσης (CC BY v.4.0)</a>" + "\"," + 
					  "\"termOfUseEng\": \"<a href=\\\"http://creativecommons.org/licenses/by/4.0/\\\">" + 
					  "Creative Commons Attribution 4.0 (CC BY v.4.0)</a>\"}";
		
		return jsonString;
	}

}