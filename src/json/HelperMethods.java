package json;

import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

import com.hp.hpl.jena.rdf.model.Literal;

/**
 * @author G. Razis
 */
public class HelperMethods {
	
	public String roundAmountWithDecimals(Literal totalAmount) {
		
		String totalAmountStr = "";
		
		if (totalAmount != null) {
			String totalAmountParts[] = totalAmount.toString().split("http");
			String amount = totalAmountParts[0].replace("^", "");
			try {
				String[] numParts = amount.split("\\.");
				String twoDecimalsStr = "0." + numParts[1].subSequence(0, 2).toString();
				totalAmountStr = numParts[0] + "." + twoDecimalsStr.split("\\.")[1];
			} catch (Exception e) {
				System.out.println("Integer or Less than 3 decimals");
				totalAmountStr = amount;
			}
		} else {
			totalAmountStr = "0";
		}
		
		return totalAmountStr;
	}
	
	public String roundAmountNoDecimals(Literal totalAmount) {
		
		String totalAmountStr = "";
		
		if (totalAmount != null) {
			String totalAmountParts[] = totalAmount.toString().split("http");
			String amount = totalAmountParts[0].replace("^", "");
			try {
				String[] numParts = amount.split("\\.");
				totalAmountStr = numParts[0];
			} catch (Exception e) {
				totalAmountStr = amount;
			}
		} else {
			totalAmountStr = "0";
		}
		
		return totalAmountStr;
	}
	
	public String convertAmount(Literal totalAmount) {
		
		String totalAmountStr = "";
		
		if (totalAmount != null) {
			String totalAmountParts[] = totalAmount.toString().split("http");
			String amount = totalAmountParts[0].replace("^", "");
			String[] numParts = amount.split("\\.");
			int digits = numParts[0].length();
			
			if (digits == 1) {
				totalAmountStr = "0.0K";
			} else if (digits == 2) {
				totalAmountStr = "0.0K";
			} else if (digits == 3) {
				double dividor = Math.pow(10, (digits - 1));
				double amountFormatted = Math.round( (Integer.valueOf(numParts[0]) / dividor) );
				totalAmountStr = String.valueOf(amountFormatted/10) + "K";
			} else if (digits == 4) {
				double dividor = Math.pow(10, (digits - 2));
				double amountFormatted = Math.round( (Integer.valueOf(numParts[0]) / dividor) );
				totalAmountStr = String.valueOf(amountFormatted/10) + "K";
			} else if (digits == 5) {
				double dividor = Math.pow(10, (digits - 3));
				double amountFormatted = Math.round( (Integer.valueOf(numParts[0]) / dividor) );
				totalAmountStr = String.valueOf(amountFormatted/10) + "K";
			} else if (digits == 6) {
				double dividor = Math.pow(10, (digits - 1));
				double amountFormatted = Math.round( (Integer.valueOf(numParts[0]) / dividor) );
				totalAmountStr = String.valueOf(amountFormatted/10) + "M";
			} else if (digits == 7) {
				double dividor = Math.pow(10, (digits - 2));
				double amountFormatted = Math.round( (Integer.valueOf(numParts[0]) / dividor) );
				totalAmountStr = String.valueOf(amountFormatted/10) + "M";
			} else if (digits == 8) {
				double dividor = Math.pow(10, (digits - 3));
				double amountFormatted = Math.round( (Integer.valueOf(numParts[0]) / dividor) );
				totalAmountStr = String.valueOf(amountFormatted/10) + "M";
			} else if (digits == 9) {
				double dividor = Math.pow(10, (digits - 1));
				double amountFormatted = Math.round( (Integer.valueOf(numParts[0]) / dividor) );
				totalAmountStr = String.valueOf(amountFormatted/10) + "B";
			} else if (digits == 10) {
				double dividor = Math.pow(10, (digits - 2));
				double amountFormatted = Math.round( (Long.valueOf(numParts[0]) / dividor) );
				totalAmountStr = String.valueOf(amountFormatted/10) + "B";
			} else if (digits == 11) { //greater than MAX_INT
				double dividor = Math.pow(10, (digits - 2));
				double amountFormatted = (Long.valueOf(numParts[0]) / dividor);
				totalAmountStr = String.valueOf(amountFormatted).substring(0, 4) + "B";
			} else if (digits == 12) { //greater than MAX_INT
				double dividor = Math.pow(10, (digits - 1));
				double amountFormatted = Long.valueOf(numParts[0]) / dividor;
				double amountRounded = Math.round(Float.valueOf(String.valueOf(amountFormatted).substring(0, 3)));
				totalAmountStr = String.valueOf(amountRounded/10) + "T";
			} else if (digits == 13) { //greater than MAX_INT
				double dividor = Math.pow(10, (digits - 2));
				double amountFormatted = Long.valueOf(numParts[0]) / dividor;
				double amountRounded = Math.round(Float.valueOf(String.valueOf(amountFormatted).substring(0, 3)));
				totalAmountStr = String.valueOf(amountRounded/10) + "T";
			}
		} else {
			totalAmountStr = "0.0K";
		}
		
		return totalAmountStr;
	}
	
	public void writeJsonFile(String jsonString, String folderName, String filename) {
		
		String[] filenameParts = new String[]{"", ""};
		PrintWriter writer = null;
		
		if (filename.contains("Organization/")) {
			filenameParts = filename.split("Organization/"); ////http://linkedeconomy.org/resource/Organization/xxx
		} else if (filename.contains("OrganizationalUnit/")) {
			filenameParts = filename.split("OrganizationalUnit/"); //http://linkedeconomy.org/resource/OrganizationalUnit/xxx
		} else  if (filename.contains("Person/")) {
			filenameParts = filename.split("Person/"); //http://linkedeconomy.org/resource/Person/xxx
		} else { //used by other cases of JSON
			filenameParts[1] = filename;
		}
		
		filenameParts[1] = cleanInvalidCharsFilename(filenameParts[1]);
		
		try {
			if (filenameParts[1].length() > 199) {
				filenameParts[1] = filenameParts[1].substring(0, 200);
			}
			writer = new PrintWriter(QueryConfiguration.jsonFilepath + folderName + "/" + filenameParts[1] + ".json");
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		
		writer.println(jsonString);	
		writer.close();
		
	}
	
	/**
     * Keep the date from the provided dateTime Literal 
     * and transform it in the DD-MM-YYYY format.
     * 
     * @param Literal the dateTime Literal
     * @return String the date in the DD-MM-YYYY format
     */
	public String getFormattedDateFromDateLiteral(Literal lastUpdateDate) {
		
		String date = lastUpdateDate.getString().split("T")[0];
		
		String dateElements[] = date.split("-");
		
		date = dateElements[2] + "-" + dateElements[1] + "-" + dateElements[0];
		
		return date;
	}
	
	/**
     * Creates the URL of the profile page of the provided Agent.
     * 
     * @param String the URI of the Agent
     * @param String whether the Agent is a Buyer a Seller or a Hybrid Organization
     * @return String the URL of the profile page of the provided Agent
     */
	public String urlProfilePage(String uri, String buyerOrSeller) {
		
		String url = "";
		String entity = "";
		String[] uriParts = new String[]{"", ""};
		
		if (uri.contains("Person")) {
			entity = "person";
		} else {
			entity = "organization";
		}
		
		entity += "-beneficiary";
		
		if (uri.contains("Organization/")) {
			uriParts = uri.split("Organization/"); //http://linkedeconomy.org/resource/Organization/AFM or AFM/ID (099674100, 997604027/55143)
		} else if (uri.contains("OrganizationalUnit/")) {
			uriParts = uri.split("OrganizationalUnit/"); //http://linkedeconomy.org/resource/OrganizationalUnit/UnitId (100009134)
		} else if (uri.contains("Person/")) {
			uriParts = uri.split("Person/"); //http://linkedeconomy.org/resource/Person/AFM
		} else {
			uriParts = uri.split("linkedeconomy.org/");
		}
		
		try {
			if (uriParts[1].length() > 199) {
				uriParts[1] = uriParts[1].substring(0, 200);
			}
			uriParts[1] = cleanInvalidCharsFilename(uriParts[1]);
			url = entity + "?=afm=" + uriParts[1];
		} catch (Exception e) {
			url = entity + "?=afm=" + "-";
		}
		
		return url;
	}
	
	/**
     * Handle the invalid characters of the VatId which is used for the 
     * JSON filename. The characters have to be removed by the URL too 
     * (it has to be the same as the JSON filename).
     * 
     * @param String a string to be cleaned
     * @return String the cleaned representation of the input string
     */
	public String cleanInvalidCharsFilename(String aString) {
		
		String cleanedString = aString.replace(":", "_");
		cleanedString = cleanedString.replace("/", "_");
		cleanedString = cleanedString.replace("\\", "");
		cleanedString = cleanedString.replace("*", "");
		cleanedString = cleanedString.replace("?", "");
		cleanedString = cleanedString.replace("\"", "");
		cleanedString = cleanedString.replace("<", "");
		cleanedString = cleanedString.replace(">", "");
		cleanedString = cleanedString.replace("|", "");
		
		return cleanedString;
	}
	
	/**
     * Handle the invalid characters of provided string.
     * 
     * @param String a string to be cleaned
     * @return String the cleaned representation of the input string
     */
	public String cleanInvalidCharsJsonData(String aString) {
		
		String cleanedString = aString.replace("\"", "'");
		cleanedString = cleanedString.replace("\\", "");
		
		return cleanedString;
	}
	
	/**
     * Convert the string representation of the validVatId response 
     * of the graph to a boolean value.
     * 
     * @param String validVatId response of the graph
     * @return String the boolean value of the validVatId
     */
	public boolean validVatIdToBoolean(String validVatId) {
		
		boolean flag = false;
		
		if (validVatId.equalsIgnoreCase("1")) {
			flag = true;
		}
		
		return flag;
	}
	
	public void writeToFile(String fileName, String data) {
		
		try {
		    PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(fileName + ".txt", true)));
		    out.println(data);
		    out.close();
		} catch (IOException e) {
		    e.printStackTrace();
		}
		
	}
	
	/**
     * Keep the date from the provided dateTime String 
     * and transform it in the DD-MM-YYYY format.
     * 
     * @param String the dateTime String
     * @return String the date in the DD-MM-YYYY format
     */
	public String getFormattedDateFromDateString(String lastUpdateDate) {
		
		String dateElements[] = lastUpdateDate.split("-");
		
		String date = dateElements[2] + "-" + dateElements[1] + "-" + dateElements[0];
		
		return date;
	}
	
	/**
     * Find the Vat Id from the URI.
     * 
     * @param String the URI
     * @return String the Vat Id
     */
	public String getVatIdFromUri(String uri) {
		
		String vatId = null;
		
		int count = uri.length() - uri.replace("/", "").length();
				
		if ( (count == 6) || (count == 5) ) { //Supervised Organization and normal case respectively
			vatId = uri.split("/")[5];
		}
		
		return vatId;
	}
	
	/**
     * Find the validity of the Agent.
     * 
     * @param String the Agent's URI
     * @return boolean the validity of the Agent
     */
	public boolean checkAgentValidity(String uri) {
		
		boolean validity = true;
		
		if ( (uri.contains("_NotSponsor")) || (uri.contains("_EmptyVatId")) || (uri.contains("_NotPerson")) ) {
			validity = false;
		}
		
		return validity;
	}
	
	/**
     * Find the corresponding English name of the provided status of the project.
     * 
     * @param String the status of the project in Greek
     * @return String the corresponding English name 
     * of the provided status of the project
     */
	public String findEngProjectStatus(String status) {
		
		String statusEng = null;
		
		if (status.equalsIgnoreCase("Συμβασιοποιημένο")) {
			statusEng = "Contracted";
		} else {
			statusEng = "Completed";
		}
		
		return statusEng;
	}
	
	/**
     * Change the format of the provided name from 
     * "ΧΧΧ ΥΥΥ πατρ ΖΖΖ" to "ΥΥΥ ΖΖΖ ΧΧΧ".
     * 
     * @param String the name as found in the Subsidies file
     * @return String the changed format of the name
     */
	public String beneficiaryWithFatherName(String name) {
		
		String[] nameParts = null;
		String formattedName = "";//ΜΙΧΑΛΟΠΟΥΛΟΥ ΜΑΡΙΑ πατρ. ΗΡΑΚΛΗΣ
		
		if (name.contains("πατρ.")) {
			//Split the name
			nameParts = name.split(" ");
			//the format is known, so...
			formattedName += nameParts[1] + " " + nameParts[3] + " " + nameParts[0];
		} else {
			formattedName = name;
		}
		
		return formattedName;
	}

}